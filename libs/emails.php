<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'libs/PHPMailer/src/Exception.php';
require 'libs/PHPMailer/src/PHPMailer.php';
require 'libs/PHPMailer/src/SMTP.php';
// require 'libs/PHPMailer-master/PHPMailerAutoload.php';

function sendEmail($email, $issue,$msghmlt, $issuing)
{
    
    $mail = new PHPMailer(true);
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                       // Habilita la salida de depuración detallada
        $mail->isSMTP();                                            //Enviar usando SMTP
        $mail->Host       = 'sxb1plzcpnl453511.prod.sxb1.secureserver.net';                     //Configure el servidor SMTP para enviar
        $mail->SMTPAuth   = true;                                   //Habilitar la autenticación SMTP
        $mail->Username   = 'notificaciones@businesfactory.co';         //Nombre de usuario SMTP
        $mail->Password   = 'abcd.1234';                            //Contraseña SMTP
        // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;      //Habilite el cifrado TLS; Se recomienda `PHPMailer :: ENCRYPTION_SMTPS`
        $mail->SMTPSecure = 'ssl';                                  //Habilite el cifrado TLS; Se recomienda `PHPMailer :: ENCRYPTION_SMTPS`
        $mail->Port       = 465;                                    //Puerto TCP para conectarse, use 465 para `PHPMailer :: ENCRYPTION_SMTPS` arriba
    
        //Recipients
        $mail->setFrom('notificaciones@businesfactory.co', 'Business Factory');
        $mail->addAddress($email, $issuing);                        // Agregar una destinataria

        //Content
        $mail->isHTML(true);                                        // Establecer el formato de correo electrónico en HTML
        $mail->CharSet = 'UTF-8';
        $mail->Subject = $issue;                                    //Asunto 
        $mail->Body    = $msghmlt;                                  //Mensaje
    
        $mail->send();
        return true;
    } catch (Exception $e) {
        die($e->getMessage());
    }
    
}

