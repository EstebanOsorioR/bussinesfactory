<?php
require_once 'libs/IntegracionTheFactory/Modelo.php';
include_once "libs/IntegracionTheFactory/webservice_receptor.php";

function generarFacturaElectronica($comprobante,  $secuncial, $tokens, $datacliente, $items, $totales, $otros, $fechahora)
{
   
    try {
        error_reporting(E_ERROR);
        $WebService = new WebService();
        $options = array('exceptions' => true, 'trace' => true, 'location'=> WSDL);
        // $params;

        $TokenEnterprise = $tokens["emisor_token_empresa"];// //SE DEBE SETEAR ESTE VALOR (SUMINSTRADO POR TFHKA)
        $TokenAutorizacion = $tokens["emisor_token_password"];// //SE DEBE SETEAR ESTE VALOR (SUMINSTRADO POR TFHKA)
        $enviarAdjunto = false;

        $factura = new FacturaGeneral();
        $factura->cliente = new Cliente();
        $factura->cantidadDecimales = $comprobante[4];
        $factura->cliente->actividadEconomicaCIIU = $datacliente["actividad_codigo"];// (No obligatorio)
        //Datos del Cliente Completo
        $destinatarios = new Destinatario();	
            $destinatarios->canalDeEntrega = "0";
        
            $correodestinatario = new strings();	 
                $correodestinatario->string =  $datacliente["cliente_email"];
        
            $destinatarios->email = $correodestinatario;
            $destinatarios->nitProveedorReceptor = "1";
            $destinatarios->telefono =  $datacliente["cliente_telefono"];	
        
        $factura->cliente->destinatario[0] = $destinatarios;
        
        $tributos1 = new Tributos();	
            $tributos1->codigoImpuesto = $datacliente["tributofiscal_codigo"];
            
        $extensible1 = new Extensibles();
            $extensible1->controlInterno1 = "";
            $extensible1->controlInterno2 = "";
            $extensible1->nombre = "";
            $extensible1->valor = "";
            
            $tributos1->extras[0] = $extensible1;
            
        $factura->cliente->detallesTributarios[0] = $tributos1;
        
        $DireccionCliente = new Direccion();	
            $DireccionCliente->aCuidadoDe = "";
            $DireccionCliente->aLaAtencionDe = "";
            $DireccionCliente->bloque = "";
            $DireccionCliente->buzon = "";
            $DireccionCliente->calle = "";
            $DireccionCliente->calleAdicional = "";
            $DireccionCliente->ciudad = $datacliente["ciudad_nombre"];
            $DireccionCliente->codigoDepartamento = $datacliente["departamento_codigo"];
            $DireccionCliente->correccionHusoHorario = "";
            $DireccionCliente->departamento = $datacliente["departamento_nombre"];
            $DireccionCliente->departamentoOrg = "";
            $DireccionCliente->direccion = $datacliente["cliente_direccion"];
            $DireccionCliente->habitacion = "";
            $DireccionCliente->distrito = "";
            $DireccionCliente->lenguaje = $datacliente["cliente_id_lenguaje"];
            $DireccionCliente->municipio = $datacliente["ciudad_codigo"];
            $DireccionCliente->nombreEdificio = "";
            $DireccionCliente->numeroParcela = "";
            $DireccionCliente->pais = $datacliente["pais_codigo"];
            $DireccionCliente->piso = "";
            $DireccionCliente->region = "";
            $DireccionCliente->subDivision = "";
            $DireccionCliente->ubicacion = "";
            $DireccionCliente->zonaPostal = $datacliente["cliente_codigo_postal"];	
    
        $factura->cliente->direccionCliente = $DireccionCliente;
        
        $DireccionFiscal = new Direccion();	
            $DireccionFiscal->aCuidadoDe = "";
            $DireccionFiscal->aLaAtencionDe = "";
            $DireccionFiscal->bloque = "";
            $DireccionFiscal->buzon = "";
            $DireccionFiscal->calle = "";
            $DireccionFiscal->calleAdicional = "";
            $DireccionFiscal->ciudad = $datacliente["ciudad_nombre"];
            $DireccionFiscal->codigoDepartamento = $datacliente["departamento_codigo"];
            $DireccionFiscal->correccionHusoHorario = "";
            $DireccionFiscal->departamento = $datacliente["departamento_nombre"];
            $DireccionFiscal->departamentoOrg = "";
            $DireccionFiscal->direccion = $datacliente["cliente_direccion"];
            $DireccionFiscal->habitacion = "";
            $DireccionFiscal->distrito = "";
            $DireccionFiscal->lenguaje = $datacliente["cliente_id_lenguaje"];
            $DireccionFiscal->municipio =  $datacliente["ciudad_codigo"];
            $DireccionFiscal->nombreEdificio = "";
            $DireccionFiscal->numeroParcela = "";
            $DireccionFiscal->pais =  $datacliente["pais_codigo"];
            $DireccionFiscal->piso = "";
            $DireccionFiscal->region = "";
            $DireccionFiscal->subDivision = "";
            $DireccionFiscal->ubicacion = "";
            $DireccionFiscal->zonaPostal =  $datacliente["cliente_codigo_postal"];	
        
        $factura->cliente->direccionFiscal = $DireccionFiscal;
        
        $factura->cliente->email = $datacliente["cliente_email"];
        
        
        $InfoLegalCliente = New InformacionLegalCliente;
            $InfoLegalCliente->codigoEstablecimiento = "0001";
            $InfoLegalCliente->nombreRegistroRUT = "PRINCIPAL";
            $InfoLegalCliente->numeroIdentificacion = $datacliente["cliente_numero_identificacion"];
            $InfoLegalCliente->numeroIdentificacionDV = $datacliente["cliente_digito_verificacion"];
            $InfoLegalCliente->tipoIdentificacion = $datacliente["tipoiden_codigo"];	
        
        $factura->cliente->informacionLegalCliente = $InfoLegalCliente;
        
        
        $factura->cliente->nombreRazonSocial  = $datacliente["cliente_razon_social"];
        $factura->cliente->notificar = "SI";
        $factura->cliente->numeroDocumento = $datacliente["cliente_numero_identificacion"];
        $factura->cliente->numeroIdentificacionDV = $datacliente["cliente_digito_verificacion"];
        
        $obligacionesCliente = new Obligaciones();
            $obligacionesCliente->obligaciones =  $datacliente["responsfiscal_codigo"];
            $obligacionesCliente->regimen =  $datacliente["regfiscal_codigo"];
        
        $factura->cliente->responsabilidadesRut[0] = $obligacionesCliente;
        
        $factura->cliente->tipoIdentificacion = $datacliente["tipoiden_codigo"];
        $factura->cliente->tipoPersona = $datacliente["tipoper_codigo"];
        //FIN cliente
        
        //Consecutivo factura Ejm 1
        // $factura->consecutivoDocumento = $consecutivo;
        $factura->consecutivoDocumento = $secuncial["secuencial_consecutivo"];

        /**
        * Capturar el detalle de la factura
        */
        
        //Producto #1
        foreach ($items as $key => $value) {
            // var_dump($value[0]);exit;
            $factDetalle = new FacturaDetalle();
            $factDetalle->cantidadPorEmpaque = "1"; //1
            $factDetalle->cantidadReal = "1";//1.00
            $factDetalle->cantidadRealUnidadMedida = "WSD"; // $fdetfact->codigo; || Se debe dejar asi
            $factDetalle->cantidadUnidades =   $value[1];//1.00 ||cantidad
            $factDetalle->codigoProducto = $value[0]; //||Codigo
            $factDetalle->descripcion =  $value[3];//Campo en blanco no requeridos || Descripcion producto
            $factDetalle->descripcionTecnica = "";//Campo en blanco no requeridos
            $factDetalle->estandarCodigo = ""; //999
            $factDetalle->estandarCodigoProducto = ""; //PHKA80
            
            $impdet = new FacturaImpuestos;
                $impdet->baseImponibleTOTALImp =  $value[6];//100.00 || precio antes de iva
                $impdet->codigoTOTALImp = "01";//01
                $impdet->controlInterno = "";
                $impdet->porcentajeTOTALImp =  $value[6];//19.00  ||Porcentaje IVA
                $impdet->unidadMedida = "";
                $impdet->unidadMedidaTributo = "";
                $impdet->valorTOTALImp =  $value[7];//19.00 ||IVA precio
                $impdet->valorTributoUnidad = "";
                
            $factDetalle->impuestosDetalles[0] = $impdet;
            
            
            $impTot = new ImpuestosTotales;
                $impTot->codigoTOTALImp = "01";//01
                $impTot->montoTotal =  $value[7];//19.00 ||IVA precio otra vez
            
            $factDetalle->impuestosTotales[0] = $impTot;
            
            $factDetalle->marca = "";//HKA
            $factDetalle->muestraGratis = "0";
            $factDetalle->precioTotal =  $value[9];//119.00 ||Monta total por item + iva
            $factDetalle->precioTotalSinImpuestos =  $value[5];//100.00 || Monta total por item
            $factDetalle->precioVentaUnitario =  $value[4];//100.00 || precio unitario
            $factDetalle->secuencia = $key;//1
            $factDetalle->unidadMedida = $value[2];//KMG	
            $factura->detalleDeFactura [$key] = $factDetalle;	
        }
      
    
           //Se registra el primer item en el objeto factura
            
            // $factDetalle1 = new FacturaDetalle();
            // $factDetalle1->cantidadPorEmpaque = "1";
            // $factDetalle1->cantidadReal = "1.00";
            // $factDetalle1->cantidadRealUnidadMedida = "WSD"; // $fdetfact->codigo;
            // $factDetalle1->cantidadUnidades = "1.00";
            // $factDetalle1->codigoProducto = "P000003";
            // $factDetalle1->descripcion = "Impresora SRP-812";//Campo en blanco no requeridos
            // $factDetalle1->descripcionTecnica = "Impresora térmica de punto de venta, ideal para puntos de venta con alto rendimiento";//Campo en blanco no requeridos
            // $factDetalle1->estandarCodigo = "999";
            // $factDetalle1->estandarCodigoProducto = "SRP-812";
            
            // $impdet1 = new FacturaImpuestos;
            //     $impdet1->baseImponibleTOTALImp = "200.00";
            //     $impdet1->codigoTOTALImp = "01";
            //     $impdet1->controlInterno = "";
            //     $impdet1->porcentajeTOTALImp = "19.00";
            //     $impdet1->unidadMedida = "";
            //     $impdet1->unidadMedidaTributo = "";
            //     $impdet1->valorTOTALImp = "38.00";
            //     $impdet1->valorTributoUnidad = "";
                
            // $factDetalle1->impuestosDetalles[0] = $impdet1;
            
            
            // $impTot1 = new ImpuestosTotales;
            //     $impTot1->codigoTOTALImp = "01";
            //     $impTot1->montoTotal = "38.00";
            
            // $factDetalle1->impuestosTotales[0] = $impTot1;
            
            // $factDetalle1->marca = "HKA";
            // $factDetalle1->muestraGratis = "0";
            // $factDetalle1->precioTotal = "238.00";
            // $factDetalle1->precioTotalSinImpuestos = "200.00";
            // $factDetalle1->precioVentaUnitario = "200.00";
            // $factDetalle1->secuencia = "2";
            // $factDetalle1->unidadMedida = "WSD";	
    
            // $factura->detalleDeFactura [1] = $factDetalle1; // segundo item
    
            $factura->fechaEmision = "2021-05-24 00:00:00";
            // $factura->fechaEmision = $fechahora;
          
        //fin de detalle
        
        
        //IMPUESTOS GENERALES1  
        $objImpGen = new FacturaImpuestos;
                $objImpGen->baseImponibleTOTALImp = "100000";//300.00 || Subtotal global
                $objImpGen->codigoTOTALImp = "01";//01 || Tipo Impuesto
                $objImpGen->controlInterno = "";
                $objImpGen->porcentajeTOTALImp = "19";//19.00 || Porcentaje IVA
                $objImpGen->unidadMedida = "";
                $objImpGen->unidadMedidaTributo = "";
                $objImpGen->valorTOTALImp = "19000";//57.00 || Precio IVA
                $objImpGen->valorTributoUnidad = "0";//0.00 
       
           $factura->impuestosGenerales[0] = $objImpGen;
        
        $impTot2 = new ImpuestosTotales;
                $impTot2->codigoTOTALImp = "01";//01 || Tipo Impuesto
                $impTot2->montoTotal =  $totales[2];//57.00 || Precio IVA
                
        $factura->impuestosTotales[0] = $impTot2;
        
        $pagos = new MediosDePago();
            $pagos->medioPago =  $otros[1];
            $pagos->metodoDePago =  $otros[0];
            $pagos->numeroDeReferencia = $otros[2];	
    
        $factura->mediosDePago[0] = $pagos;
        $factura->moneda = "COP";
        $factura->redondeoAplicado = "0.00"	;//0.00
        $factura->rangoNumeracion = $secuncial["secuencial_prefijo"]."-".$secuncial["secuencial_rango_inicial"]; // //SE DEBE SETEAR ESTE VALOR (SUMINSTRADO POR TFHKA EN PRUEBAS, POR LA DIAN EN PRODUCCION)
        
        $factura->tipoOperacion = $comprobante[5];//
        $factura->totalBaseImponible =  $totales[0];//$factura->impuestosGenerales["baseImponibleTOTALImp"];|| Subtotal global
        $factura->totalBrutoConImpuesto =  $totales[3];//357 || Total global
        $factura->totalMonto = $totales[3];//357.00 || Total global otra vez
        $factura->totalProductos = count($items);// || Nro. cantidad productos
        $factura->totalSinImpuestos= $totales[0];//300.00 || Subtotal global
    
        $factura->tipoDocumento=$comprobante[0];     //Tipo documento
    
     
    
        if ($enviarAdjunto == "TRUE"){
    
            $adjuntos="1";
    
        }else{
    
            $adjuntos="0";
            }
    
         
         $params = array(
             'tokenEmpresa' =>  $TokenEnterprise,
             'tokenPassword' =>$TokenAutorizacion,
             'factura' => $factura ,
             'adjuntos' => $adjuntos);
         //Enviar Objeto Factura
         $resultado = $WebService->enviar(WSDL,$options,$params);
         //capturar codigo de respuesta del WS del Autofact para dar respuesta al usuario
         
        //  echo "<h1>Resultado de la Emisión</h1></br>";
         
        if($resultado["codigo"]==200){

                // print_r("Código: " .$resultado["codigo"] ."</br>Mensaje:  " .$resultado["mensaje"] ."</br>Consecutivo:  " .$resultado["consecutivoDocumento"] ."</br>CUFE:  " .$resultado["cufe"] ."</br>Fecha de Respuesta:  " .$resultado["fechaRespuesta"] ."</br>Hash:  " .$resultado["hash"] ."</br>Reglas de validación DIAN:  " .$resultado["reglasValidacionDIAN"] ."</br>Resultado:  " .$resultado["resultado"] ."</br>Tipo de CUFE:  " .$resultado["tipoCufe"] ."</br>Mensaje Validación:  " );
    
                // $max = sizeof($resultado["mensajesValidacion"]->string);
                //  for($i = 0; $i < $max;$i++){
    
                //      print_r("</br>" .$resultado["mensajesValidacion"]->string[$i]  );
                //  }

                 $max = sizeof($resultado["mensajesValidacion"]->string);
                 for($i = 0; $i < $max;$i++){
    
                    $mensajesValidacion = $resultado["mensajesValidacion"]->string[$i];
                 }

                 return array('codigo' => $resultado["codigo"], 'mensaje' =>  $resultado["mensaje"] , 'consecutivo' =>  $resultado["consecutivoDocumento"], 'cufe'  => $resultado["cufe"], 'fechaRespuesta'  => $resultado["fechaRespuesta"], 'hash'  => $resultado["hash"], 'reglasValidacionDIAN'  => $resultado["reglasValidacionDIAN"], 'Resultado'  =>  $resultado["resultado"] , 'Tipo de CUFE'  => $resultado["tipoCufe"] ,"mensajesValidacion"  =>  $mensajesValidacion);
        
        // ENVIO DE ADJUNTOS
    
            // if ($enviarAdjunto == "TRUE"){
    
            //         $handle = fopen($_FILES['archivo']["tmp_name"],"r");
            //         $conten = fread($handle,filesize($_FILES['archivo']["tmp_name"]));
                    
    
            //         $nombreformato = explode(".", $_FILES['archivo']['name']);
            //         $tm = sizeof($nombreformato);
                    
    
            //         $Adjunto = new adjunto();
            //         $Adjunto->archivo= $conten;
    
            //         $correo = new strings();
            //         $correo = $_POST["correo"];
    
            //           $Adjunto->email[0] = $correo;
            //           $Adjunto->enviar = "1";
            //          $Adjunto->formato = $nombreformato[$tm-1];
            //           $Adjunto->nombre= $nombreformato[0];
            //           $Adjunto->numeroDocumento = $resultado["consecutivoDocumento"];
            //           $Adjunto->tipo = "2";
    
            //           $params = new CargarAdjuntos();
            //              $params->tokenEmpresa =  $TokenEnterprise;
            //              $params->tokenPassword = $TokenAutorizacion;
            //              $params->adjunto = $Adjunto;
    
            //         $options = array('exceptions' => true, 'trace' => true);
            //           $resultado = $WebService->CargarAdjuntos(WSANEXO,$options,$params);
    
            //           echo "<h2>Resultado del Envío de Adjuntos</h2></br>";
    
            //           print_r("Código: " .$resultado["codigo"] ."</br>Mensaje:  " .$resultado["mensaje"] ."</br>Resultado:  " .$resultado["resultado"]);
    
            //     }
    
        }else{
                // $max = sizeof($resultado["mensajesValidacion"]->string);
    
                // print_r("Código: " .$resultado["codigo"] ."</br>Mensaje:  " .$resultado["mensaje"] ."</br>Fecha de Respuesta:  " .$resultado["fechaRespuesta"] ."</br>Mensaje Validación:  " );
                //  for($i = 0; $i < $max;$i++){
    
                //      print_r("</br>" .$resultado["mensajesValidacion"]->string[$i]  );
                //  }
                 //."</br>Resultado:  " .$resultado["resultado"] );

                 $max = sizeof($resultado["mensajesValidacion"]->string);
                 for($i = 0; $i < $max;$i++){
    
                    $mensajesValidacion = $resultado["mensajesValidacion"]->string[$i];
                 }

                 return array('codigo' => $resultado["codigo"], 'mensaje' =>  $resultado["mensaje"] , 'fechaRespuesta' =>  $resultado["fechaRespuesta"], "mensajesValidacion"  =>  $mensajesValidacion);

            
        }
        
        exit();
    } catch (Exception $e) {
        die($e->getMessage());
    }
    
}

function genetarPDF($tokens,$numfactura){
    try {
        error_reporting(E_ERROR);
        $WebService = new WebService();
        $options = array('exceptions' => true, 'trace' => true, 'location'=> WSDL);
        $params = array(
            'tokenEmpresa' =>  $tokens["emisor_token_empresa"],
            'tokenPassword' =>$tokens["emisor_token_password"],
            'documento' => $numfactura);
        //Descargar Objeto Factura
        $resultado = $WebService->Descargas(WSDL,$options,$params,"pdf");
             
        if($resultado["codigo"]==200){

            $decoded = base64_decode($resultado["documento"]);
            $file = $numfactura.'.pdf';
            file_put_contents($file, $decoded);
            
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($file).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                readfile($file);
                exit;
            };

    }else{
           
             $max = sizeof($resultado["mensajesValidacion"]->string);
             for($i = 0; $i < $max;$i++){
                $mensajesValidacion = $resultado["mensajesValidacion"]->string[$i];
             }
             return array('codigo' => $resultado["codigo"], 'mensaje' =>  $resultado["mensaje"] , 'fechaRespuesta' =>  $resultado["fechaRespuesta"], "mensajesValidacion"  =>  $mensajesValidacion);
    }

    exit();
    } catch (Exception $e) {
        die($e->getMessage());
    }
}



function genetarXML($tokens,$numfactura){
    try {
        error_reporting(E_ERROR);
        $WebService = new WebService();
        $options = array('exceptions' => true, 'trace' => true, 'location'=> WSDL);
        $params = array(
            'tokenEmpresa' =>  $tokens["emisor_token_empresa"],
            'tokenPassword' =>$tokens["emisor_token_password"],
            'documento' => $numfactura);
        //Descargar Objeto Factura
        $resultado = $WebService->Descargas(WSDL,$options,$params,"xml");
             
        if($resultado["codigo"]==200){

            $decoded = base64_decode($resultado["documento"]);
            $file = $numfactura.'.xml';
            file_put_contents($file, $decoded);
            
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($file).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                readfile($file);
                exit;
            };

    }else{
           
             $max = sizeof($resultado["mensajesValidacion"]->string);
             for($i = 0; $i < $max;$i++){
                $mensajesValidacion = $resultado["mensajesValidacion"]->string[$i];
             }
             return array('codigo' => $resultado["codigo"], 'mensaje' =>  $resultado["mensaje"] , 'fechaRespuesta' =>  $resultado["fechaRespuesta"], "mensajesValidacion"  =>  $mensajesValidacion);
    }

    exit();
    } catch (Exception $e) {
        die($e->getMessage());
    }
}