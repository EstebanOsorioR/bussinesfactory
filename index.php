<?php
require_once 'config.php';
require_once 'model/conn.model.php';
require_once 'controller/home.controller.php';
// require_once 'controller/myaccount.controller.php';

if (isset($_REQUEST["c"])) {
    $controller = strtolower($_REQUEST["c"]);
    $action     = isset($_REQUEST["a"]) ? $_REQUEST["a"] : "index";
    if (file_exists('controller/' . $controller . '.controller.php')) {
        require_once "controller/$controller.controller.php";
        $controller = ucwords($controller) . 'Controller';
        $controller = new $controller;
        if (method_exists($controller, $action)) {
            call_user_func(array($controller, $action));
        } else {
            $controller = "home";
            require_once "controller/$controller.controller.php";
            $controller = ucwords($controller) . 'Controller';
            $controller = new $controller;
            $controller->error404();
        }
    } else {
        $controller = "home";
        require_once "controller/$controller.controller.php";
        $controller = ucwords($controller) . 'Controller';
        $controller = new $controller;
        $controller->error404();
    }
} else {
    $controller = "home";
    require_once "controller/$controller.controller.php";
    $controller = ucwords($controller) . 'Controller';
    $controller = new $controller;
    $controller->login();
}
