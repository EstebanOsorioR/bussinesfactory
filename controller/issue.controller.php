<?php
require_once 'model/issue.model.php';
require_once 'controller/home.controller.php';
require_once 'controller/catalog.controller.php';
require_once 'libs/facturacion_electronica.php';




class IssueController
{
    private $model;
    private $home;
    private $catalog;
    private $fechahora;

    public function __CONSTRUCT()
    {
        $this->model = new IssueModel();
        $this->home = new HomeController();
        $this->catalog = new CatalogController();
        date_default_timezone_set('America/Bogota');
        $this->fechahora = date("Y-m-d H:i:s");
    }

    // Vista lista de productos
    public function voucher()
    {
        $title = "| Emitar comprobante";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        require_once "views/modules/mod_issue/voucher.php";
        require_once "views/include/footer.php";
    }

    // Vista lista de productos
    public function listvoucher()
    {
        $title = "| Emitar comprobante";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        require_once "views/modules/mod_issue/list_voucher.php";
        require_once "views/include/footer.php";
    }

    //Select para tipo de documentos
    public function selectTypeDocument()
    {
        $const = $this->model->listTypeDocument();
        foreach ($const as $key => $value) {
            echo '<option value="' . $value["tipo_documento_codigo"] . '">' . $value["tipo_documento_nombre"] . '</option>';
        }
    }
    
    //Select para eñ secencual
    public function selectSequential()
    {
        if (isset($_POST["dato"])) {
            $documento = $_POST["dato"];
            if (!empty($documento)) {
                // var_dump($documento);exit();
                $const = $this->model->listSequential($documento);
                // echo "<pre>"; var_dump($const[0]["secuencial_id"]);exit();
                foreach ($const as $key => $value) {
                    echo '<option value="' . $value["secuencial_id"] . '">' . $value["secuencial_prefijo"] . ' ' . $value["secuencial_rango_desde"] . '-' . $value["secuencial_rango_hasta"] . '</option>';
        
                }
            }
        }
    }

    //Select para tipo de documentos
    public function selectTypeOperation()
    {
        $const = $this->model->listTypeOperation();
        foreach ($const as $key => $value) {
            echo '<option value="' . $value["tipoope_emision_codigo"] . '">' . $value["tipoope_emision_nombre"] . '</option>';
        }
    }

    //Select para metodo de pago
    public function selectPaymentMethod()
    {
        $const = $this->model->listPaymentMethod();
        foreach ($const as $key => $value) {
            echo '<option value="' . $value["metodo_pago_id"] . '">' . $value["metodo_pago_nombre"] . '</option>';
        }
    }

    //Select para medio de pago
    public function selectPaymentMeans()
    {
        $const = $this->model->listPaymentMeans();
        foreach ($const as $key => $value) {
            echo '<option value="' . $value["medio_pago_codigo"] . '">' . $value["medio_pago_nombre"] . '</option>';
        }
    }

    //Select cliente
    public function selectClient()
    {
        $const = $this->model->listClient();
        foreach ($const as $key => $value) {
            echo '<option value="' . $value["cliente_id"] . '">' . $value["cliente_razon_social"] . '</option>';
        }
    }

    //para mostrar cliente
    public function seeClient()
    {
        if (isset($_POST["dato"])) {
            $client = $_POST["dato"];
            if (!empty($client)) {
                // var_dump($documento);exit();
                $const = $this->model->seeClients($client);
                // echo "<pre>"; var_dump($const[0]["secuencial_id"]);exit();
                foreach ($const as $key => $value) {
                    echo '  <div class="col-md-4 col-sm-12 col-xs-12">
                                <label>Número de Identificación</label>
                                <input type="text" class="form-control" value = "'. $value["cliente_numero_identificacion"]."-". $value["cliente_digito_verificacion"].'" readonly="readonly" required="required" />
                            </div>
                            
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <label>Email</label>
                                <input type="text" class="form-control" value = "'.$value["cliente_email"].'" readonly="readonly" required="required" />
                            </div>';

                }
            }
        }
    }

    //Select conceptos (productos)
    public function selectConcept()
    {
        $const = $this->model->listConcept();
        foreach ($const as $key => $value) {
            echo '<option value="' . $value["producto_id"] . '">' . $value["producto_codigo"] .' - '. $value["producto_descripcion"] . '</option>';
        }
    }

    //Select para tipo de documentos
    public function selectTypeDiscount()
    {
        $const = $this->model->listTypeDiscount();
        foreach ($const as $key => $value) {
            echo '<option value="' . $value["tipo_descuento_codigo"] . '">' . $value["tipo_descuento_nombre"] . '</option>';
        }
    }


    //para mostrar cliente
    public function seeConcept()
    {
        if (isset($_POST["dato"])) {
            $concepto = $_POST["dato"];
            if (!empty($concepto)) {
                // var_dump($documento);exit();
                $const = $this->model->infoConcept($concepto);
                // echo "<pre>"; var_dump(is_array($const));exit();
                if (is_array($const[0])) {
                    $datos = [];
                    foreach ($const as $row => $value) {
                        $array           = [];
                        $array["code"]   = $value["producto_codigo"];
                        $array["description"]   = $value["producto_descripcion"];
                        $array["codeunit"]   = $value["producto_id_unidad_medida"];
                        $array["percent"]   = $value["porimp_nombre_entero"];
                        $array["price"]   = $value["producto_precio_unitario"];
                        $array["unit"] = $value["unimed_nombre"];
                        $array["tax"] = $value["producto_id_tipo_impuesto"];
                        $array["taxtype"] = $value["producto_id_porc_impuesto"];
                        $array["producttype"] = $value["tipproducto_nombre"];                    
                        $datos[]         = $array;
                    }
                    echo json_encode($datos[0],JSON_UNESCAPED_UNICODE);
                }
                
            }
        }
    }

    
    // Guardar o editar productos
    public function generateDocument()
    {
        $comprobante = filter_input(INPUT_POST, 'frmvoucherdata', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        $cliente = filter_input(INPUT_POST, 'frmclient', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        $data = filter_input(INPUT_POST, 'frmitems', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        $ivas = filter_input(INPUT_POST, 'frmivas', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        $totales = filter_input(INPUT_POST, 'frmtotales', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        $otros = filter_input(INPUT_POST, 'frmothers', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        //  echo "<pre>"; var_dump($totales);exit();
        if (isset($comprobante)&&isset($cliente)&&isset($data)&&isset($ivas)&&isset($totales)&&isset($otros)) {
            
            session_start();
            $secuncial =  $this->model->listSecuncial( $comprobante[2]);
            $tokens =  $this->model->listTokens();
            $datacliente = $this->model->listAllClients($cliente);
            $items = array_chunk($data,10);
            $ivas = array_chunk($ivas,2);
        
            // echo "<pre>"; var_dump($otros); exit();
            $result = generarFacturaElectronica($comprobante, $secuncial, $tokens, $datacliente, $items,$totales, $otros,$this->fechahora);
            //  echo "<pre>"; var_dump($result); exit();

            if ($result["codigo"] == 200) {
                $data =array_chunk($data,10);
                $items = json_encode(array_chunk($data,10),JSON_UNESCAPED_UNICODE);
                $result1 = $this->model->saveDocument($comprobante,$cliente,$items,$totales,$otros,$result["consecutivo"]);
                // var_dump($result1["answer"]); exit();
                if ($result1["answer"] == "save") {
                    echo json_encode($result,JSON_UNESCAPED_UNICODE);
                }

            }else{
                echo json_encode($result,JSON_UNESCAPED_UNICODE);
            }

        }
    }

     // Tabla de documentos electronicos
     public function seeDucuments()
     {
         if (isset($_POST["start"],
             $_POST["length"],
             $_POST["order"][0]["column"],
             $_POST["order"][0]["dir"],
             $_POST["search"]["value"],
             $_POST["draw"])) {
             // Parametro de inicio
             $start = (int) $_POST["start"];
             // Parametro Final
             $length = (int) $_POST["length"];
             // Parametro Ordenar
             $orderColumn = $_POST["order"][0]["column"];
             $orderDir    = $_POST["order"][0]["dir"];
             // Parametro de busqueda
             $search = $_POST["search"]["value"];
             //Consulta
             $const = $this->model->listDucuments($start, $length, $search, $orderColumn, $orderDir);
             $datos = [];
             foreach ($const[0] as $row => $value) {
                 $array           = [];
                 $array["document"]   = $value["tipo_documento_nombre"];
                 $array["date"]   = $value["comprobante_fecha_emision"];
                 $array["voucher"] = $value["comprobante_no_documento"];
                 $array["clinet"] = $value["cliente_razon_social"];
                 $array["nit"] = $value["cliente_numero_identificacion"] . "-" . $value["cliente_digito_verificacion"];
                 $array["total"] = $value["comprobante_monto_total"];
                 $array["actions"] = '<div class="btn-group">
                                         <a class= "btn a_pdf" id="a_pdf" href="'.SERVERURL.'issue/readerPDF/'. $value["comprobante_no_documento"].'"><i class="fa fa-file-pdf-o"></i></a>
                                         <a class= "btn a_xml" id="a_xml" href="'.SERVERURL.'issue/readerXML/'. $value["comprobante_no_documento"].'"><i class="fa fa-file-code-o"></i></a>
                                     </div>';
                $array["email"]   = '<a class= "btn" id="a_correo" data-delete='.$value["comprobante_no_documento"].'><i class="fa fa-envelope"></i></a>';            
                 $datos[]         = $array;
             }
             // var_dump($const[2]);exit;
             $json_data = array(
                 "draw"            => intval($_POST["draw"]),
                 "recordsTotal"    => intval($const[1]),
                 "recordsFiltered" => intval($const[2][0]),
                 "data"            => $datos,
             );
             echo json_encode($json_data);
         }
     }

     //Mostar la facueta PDF
     public function readerPDF()
     {
        if (isset($_GET["key"])) {//Si hay una variable get
            // $data  = $_GET["id"];
            $data = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_STRING);
            if (!empty($data)) {// Si es numerica

                session_start();
                $tokens =  $this->model->listTokens();
                $numfactura = $data;
                    // var_dump($pdf);
                $result = genetarPDF($tokens,$numfactura);
                
            }
        }
     }


    //Mostar la facueta PDF
    public function readerXML()
    {
    if (isset($_GET["key"])) {//Si hay una variable get
        // $data  = $_GET["id"];
        $data = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_STRING);
        if (!empty($data)) {// Si es numerica

            session_start();
            $tokens =  $this->model->listTokens();
            $numfactura = $data;
                // var_dump($pdf);
            $result = genetarXML($tokens,$numfactura);
            
        }
    }
    }
  
  
  

}
