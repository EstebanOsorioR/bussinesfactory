<?php
require_once 'model/myaccount.model.php';
require_once 'controller/home.controller.php';

class MyAccountController
{
    private $model;
    private $home;

    public function __CONSTRUCT()
    {
        $this->model = new MyAccountModel();
        $this->home = new HomeController();
    }


    public function checkStatusWiew(){
        $result = $this->model->seeStatusAlert();
        return $result;
    }

    // Vista cambiar contraseña contraseña 
    public function frmpassword()
    {                   
        $title = "| Cambio de contraseña";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        require_once "views/modules/mod_myaccount/frm_password.php";
        require_once "views/include/footer.php";
    }


    //Vista mi cuenta
    public function account(){
        $title = "| Mi Cuenta";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        $const = $this->model->infoAccount();
        // echo "<pre>"; var_dump($const);exit();
    //    var_dump($const[0]["tipoper_nombre"]);
        require_once "views/modules/mod_myaccount/account.php";
        require_once "views/include/footer.php";
    }

    //Vista mi cuenta formulario
    public function frmaccount(){
        $title = "| Mi Cuenta";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        $const = $this->model->seeAccount();
        require_once "views/modules/mod_myaccount/frm_myaccount.php";
        require_once "views/include/footer.php";
    }

    public function frmsequential(){
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        if (isset($_GET["key"])) {//Si hay una variable get
            // $data  = $_GET["id"];
            $data = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_NUMBER_INT);
            if (is_numeric($data)) {// Si es numerica
                $result = $this->model->infoSequential($data);
                if($result[0]){// Si $result tiene información
                    $const = $result[0];
                }else{
                    $this->home->error404body();
                    exit();
                }
            }else{
                $const = "";
            }
        }
        require_once "views/modules/mod_myaccount/frm_sequential.php";
        require_once "views/include/footer.php";
    }

    // Select para Tipo de Ambiente 
    public function selectTypeEnvironment($data="")
    {
        $const = $this->model->listTypeEnvironment();
        foreach ($const as $key => $value) {
            if ($data == $value["ambiente_id"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["ambiente_id"] . '">' . $value["ambiente_nombre"] . '</option>';
        }
    }

        // Select para Tipo de Ambiente 
        public function selectTypeDocument($data="")
        {
            $const = $this->model->listTypeDocument();
            foreach ($const as $key => $value) {
                if ($data == $value["tipo_documento_id"]) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option '.$sel.' value="' . $value["tipo_documento_id"] . '">' . $value["tipo_documento_nombre"] . '</option>';
            }
        }

        

    // Select para Tipo de Operación 
    public function selectTypeOperation($data="")
    {
        $const = $this->model->listTypeOperation();
        foreach ($const as $key => $value) {
            if ($data == $value["tipo_operacion_id"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["tipo_operacion_id"] . '">' . $value["tipo_operacion_nombre"] . '</option>';
        }
    }
   
    
    // Select para Modalidad de secuancial
    public function selectModality($data="")
    {
        $const = $this->model->listModality();
        foreach ($const as $key => $value) {
            if ($data == $value["modalidad_id"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["modalidad_id"] . '">' . $value["modalidad_nombre"] . '</option>';
         }
    }


    //Funcion Alerta para cambiar la contaseña por primera vez
    public function checkStatusAlert(){
        $result = $this->model->seeStatusAlert();
        if ($result == "3") {
            echo json_encode(array('answer' => 'alertpass'));
        }else if($result == "4"){
            echo json_encode(array('answer' => 'alertaccount'));
        }
    }

    //Funcion para cambiar la contaseña
    public function changePass()
    {
        $data = filter_input(INPUT_POST, 'frmpassword', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        if ($data[1] == $data[2]) { //Si estan las contaseñas estan iguales tanto Nueva contraseña y Confirmar nueva contraseña
            if ($data[0] != $data[1]) {//La contraseña actual es diferente de la contraseña nueva
                // var_dump($data[0]);exit();
                $result = $this->model->verifyPassword($data[0]);
                // var_dump($result);exit();
                if ($result == "agree") {
                    $result = $this->model->saveNewPass($data[1]);
                    if ($result == "success") {//Si actualizo
                        echo json_encode(array('answer' => 'success'));
                    }else if ($result == "success_edit"){
                        echo json_encode(array('answer' => 'success_edit'));
                    }else{
                        echo json_encode(array('answer' => 'error'));
                    }
                }else if($result == "no_agree"){
                    echo json_encode(array('answer' => 'no_agree'));
               
                }else{//Por si hay un error externo
                    echo json_encode(array('answer' => 'error'));
                }
   
            }else{
                echo json_encode(array('answer' => 'same'));
            }
        }else{//Si no estan igual las contaseñas nuevas
            echo json_encode(array('answer' => 'samenew'));
        }
        // var_dump($data);exit();
    }

    // Select para Régimen de contribuyente
    public function selectTaxpayerRegime($data="")
    {
        $const = $this->model->listTaxpayerRegime();
        foreach ($const as $key => $value) {
            if ($data == $value["tipoper_id"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["tipoper_id"] . '">' . $value["tipoper_nombre"] . '</option>';
        }
    }

    // Select para Tipo de régimen
    public function selectRegimeType($data="")
    {
        $const = $this->model->listRegimeType();
        foreach ($const as $key => $value) {
            if ($data == $value["regfiscal_id"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["regfiscal_id"] . '">' . $value["regfiscal_nombre"] . '</option>';
        }
    }

    // Select para Tipo de identificación
    public function selectIdentificationType($data="")
    {
        $const = $this->model->listIdentificationType();
        foreach ($const as $key => $value) {
            if ($data == $value["tipoiden_id"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["tipoiden_id"] . '">' . $value["tipoiden_nombre"] . '</option>';
        }
    }
    
    // Select para actividad económica 
    public function selectEconomicActivity($data="")
    {
        $const = $this->model->listEconomicActivity();
        foreach ($const as $key => $value) {
            if ($data == $value["actividad_id"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["actividad_id"] . '">' . $value["actividad_codigo"]."-". $value["actividad_nombre"] . '</option>';
        }
    }

    // Select para responsabilidades 
    public function selectResponsibility($data="")
    {
        $const = $this->model->listResponsibility();
        foreach ($const as $key => $value) {
            if ($data == $value["responsfiscal_id"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["responsfiscal_id"] . '">' . $value["responsfiscal_nombre"] . '</option>';
        }
    }


    // Select para Impustos DIAN
    public function selectTaxesDIAN($data="")
    {
        $const = $this->model->listTaxesDIAN();
        foreach ($const as $key => $value) {
            if ($data == $value["tributofiscal_id"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["tributofiscal_id"] . '">' .  $value["tributofiscal_codigo"] ."-". $value["tributofiscal_descripcion"] . '</option>';
        }
    }

    // Select para departamento 
    public function selectCountry($data="")
    {
        $const = $this->model->listCountry();
        foreach ($const as $key => $value) {
            if ($data == $value["pais_id"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["pais_id"] . '">' . ucfirst(strtolower($value["pais_nombre"])) . '</option>';
        }
    }
    // Select para departamento 
    public function selectDepartment($data="")
    {
        $const = $this->model->listDepartment();
        foreach ($const as $key => $value) {
            if ($data == $value["departamento_id"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["departamento_id"] . '">' . ucfirst(strtolower($value["departamento_nombre"])) . '</option>';
        }
    }

    // Select para ciudad
    public function selectCity($data="")
    {
        if (isset($_POST["dato"])) {
            $departament = $_POST["dato"];
            if (!empty($departament)) {
                // var_dump($departament);exit();
                $const = $this->model->listCity($departament);
                // var_dump($const[1]);exit();
                foreach ($const[0] as $key => $value) {
                    if ($const[1] == $value["ciudad_id"]) {
                        $sel = "selected";
                    } else {
                        $sel = "";
                    }
                    echo ('<option '.$sel.' value="' . $value["ciudad_id"] . '">' . ucfirst(strtolower($value["ciudad_nombre"])) . '</option>');
                }
            }
           
            // var_dump( $departament );exit();
        }

    }

    // Para editar mi cuenta
    public function saveEditAccount()
    {
        $taxpayer = filter_input(INPUT_POST, 'frmtaxpayer', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        $address = filter_input(INPUT_POST, 'frmaddress', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        $additional = filter_input(INPUT_POST, 'frmadditional', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        $contact = filter_input(INPUT_POST, 'frmcontact', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        // var_dump($additional);exit();
        $result = $this->model->editAccount($taxpayer,$address,$additional,$contact);
        echo json_encode(array('answer' => $result));
    }

    // para guardar y editar secuenciales
    public function saveEditSequential()
    {
        $data = filter_input(INPUT_POST, 'frmsequential', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        // var_dump($additional);exit();
        $result = $this->model->saveEditSequentials($data);
        echo json_encode($result);
    }

    // Vista lista de unidades de medida
    public function wiewsequential()
    {
        $title = "| Detalle de producto";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        if (isset($_GET["key"])) {//Si hay una variable get
            // $data  = $_GET["id"];
            $data = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_NUMBER_INT);
            if (is_numeric($data)) {// Si es numerica
                $result = $this->model->infoDetailSequential($data);
                if($result[0]){// Si $result tiene información
                    $data = $result[0];
                }else{
                    $this->home->error404body();
                    exit();
                }
            }else{
                $data = "";
            }
        }
        require_once "views/modules/mod_myaccount/wiew_sequential.php";
        require_once "views/include/footer.php";
    }

    // Tabla de Secuenciales
    public function seeSequential()
    {
        if (isset($_POST["start"],
            $_POST["length"],
            $_POST["order"][0]["column"],
            $_POST["order"][0]["dir"],
            $_POST["search"]["value"],
            $_POST["draw"])) {
            // Parametro de inicio
            $start = (int) $_POST["start"];
            // Parametro Final
            $length = (int) $_POST["length"];
            // Parametro Ordenar
            $orderColumn = $_POST["order"][0]["column"];
            $orderDir    = $_POST["order"][0]["dir"];
            // Parametro de busqueda
            $search = $_POST["search"]["value"];
            //Consulta
            $const = $this->model->listSequential($start, $length, $search, $orderColumn, $orderDir);
            $datos = [];
            // echo "<pre>"; var_dump($const[0][0]["secuencial_consecutivo"]);exit();
            foreach ($const[0] as $row => $value) {
                $array           = [];
                if ($value["secuencial_rango_inicial"] == $value["secuencial_consecutivo"] ) {
                    $inputs = '  <div class="btn-group">
                                    <a class= "btn" id="a_detalle" href="'.SERVERURL.'myaccount/wiewsequential/'. $value["secuencial_id"].'"><i class="fa fa-search"></i></a>
                                    <a class= "btn" id="a_editar" href="'.SERVERURL.'myaccount/frmsequential/'. $value["secuencial_id"].'"><i class="fa fa-pencil"></i></a>
                                    <a class= "btn" id="a_remover" data-delete='.$value["secuencial_id"].'><i class="fa fa-ban"></i></a>
                                </div>';
                }else{
                    $inputs = '  <div class="btn-group">
                                    <a class= "btn" id="a_detalle" href="'.SERVERURL.'myaccount/wiewsequential/'. $value["secuencial_id"].'"><i class="fa fa-search"></i></a>
                                    <a class= "btn" id="a_remover" data-delete='.$value["secuencial_id"].'><i class="fa fa-ban"></i></a>
                                </div>';
                }
                $array["document"]   = $value["tipo_documento_nombre"];
                $array["establishment"] = $value["establecimiento_nombre"];
                $array["prefix"] = $value["secuencial_prefijo"];
                $array["startdate"] = $value["secuencial_fecha_inicio"];
                $array["enddate"] = $value["secuencial_fecha_fin"];
                $array["active"] = ($value["secuencial_activo"] == "1" ? ' <span class="label label-primary">Activo</span>' : '<span class="label label-danger">Desactivo</span>');
                $array["environment"] = $value["ambiente_nombre"];
                $array["actions"] = $inputs;
                $datos[]         = $array;
            }
            // var_dump($const[2]);exit;
            $json_data = array(
                "draw"            => intval($_POST["draw"]),
                "recordsTotal"    => intval($const[1]),
                "recordsFiltered" => intval($const[2][0]),
                "data"            => $datos,
            );
            echo json_encode($json_data);
        }
    }

    //Inacticar secuencuial 
    public function inactivateSequential()
    {
        $data = filter_input(INPUT_POST, 'data', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        // echo  "<pre>"; var_dump($data);exit();
        if (in_array('', $data) == false) {
            $result = $this->model->inactivateSequentials($data);
            echo json_encode($result);
        } else {
            $answer = array('answer' => 'forced');
            echo json_encode($answer);
        }
    }






}
