<?php
require_once 'model/catalog.model.php';
require_once 'controller/home.controller.php';
require_once 'controller/myaccount.controller.php';


class CatalogController
{
    private $model;
    private $home;

    public function __CONSTRUCT()
    {
        $this->model = new CatalogModel();
        $this->home = new HomeController();
    }


    // Vista lista de productos
    public function listproducts()
    {
        $title = "| Productos";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        require_once "views/modules/mod_catalog/list_products.php";
        require_once "views/include/footer.php";
    }

    // Vista de formulario guardar y editar de productos
    public function frmproducts()
    {
  
    //    var_dump($data);exit;
        $title = "| Productos";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        if (isset($_GET["key"])) {//Si hay una variable get
            // $data  = $_GET["id"];
            $data = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_NUMBER_INT);
            if (is_numeric($data)) {// Si es numerica
                $result = $this->model->infoProduct($data);
                // var_dump($result );exit();
                if($result[0]){// Si $result tiene información
                    $data = $result[0];
                }else{
                    $this->home->error404body();
                    exit();
                }
            }else{
                $data = "";
            }
        }
        require_once "views/modules/mod_catalog/frm_products.php";
        require_once "views/include/footer.php";
    }

    // Vista lista de unidades de medida
    public function wiewproducts()
    {
        $title = "| Detalle de producto";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        if (isset($_GET["key"])) {//Si hay una variable get
            // $data  = $_GET["id"];
            $data = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_NUMBER_INT);
            if (is_numeric($data)) {// Si es numerica
                $result = $this->model->infoDetailProduct($data);
                if($result[0]){// Si $result tiene información
                    $data = $result[0];
                }else{
                    $this->home->error404body();
                    exit();
                }
            }else{
                $data = "";
            }
        }
        require_once "views/modules/mod_catalog/wiew_products.php";
        require_once "views/include/footer.php";
    }

    // Vista Cliente
    public function listclients()
    {
        // $this->home->checkStatus();
        $title = "| Clientes";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        require_once "views/modules/mod_catalog/list_client.php";
        require_once "views/include/footer.php";
    }

    public function frmclient()
    {
        // $this->home->checkStatus();
        $title = "| Clientes";
        $this->home->checkStatus();     
        require_once "views/include/headar.php";
        if (isset($_GET["key"])) {//Si hay una variable get
            // $data  = $_GET["id"];
            $data = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_NUMBER_INT);
            if (is_numeric($data)) {// Si es numerica
                $result = $this->model->infoClient($data);
                // var_dump();exit();
                if(is_array($result)){// Si $result tiene información
                    $const = $result;
                }else{
                    $this->home->error404body();
                    exit();
                }
            }else{
                $const = "";
            }
        }
        require_once "views/modules/mod_catalog/frm_client.php";
        require_once "views/include/footer.php";
    }    

    // Tabla de cliente
    public function seeClients()
    {
        if (isset($_POST["start"],
            $_POST["length"],
            $_POST["order"][0]["column"],
            $_POST["order"][0]["dir"],
            $_POST["search"]["value"],
            $_POST["draw"])) {
            // Parametro de inicio
            $start = (int) $_POST["start"];
            // Parametro Final
            $length = (int) $_POST["length"];
            // Parametro Ordenar
            $orderColumn = $_POST["order"][0]["column"];
            $orderDir    = $_POST["order"][0]["dir"];
            // Parametro de busqueda
            $search = $_POST["search"]["value"];
            //Consulta
            $const = $this->model->listClients($start, $length, $search, $orderColumn, $orderDir);
            $datos = [];
            foreach ($const[0] as $row => $value) {
                $array           = [];
                $array["checkbox"]   = '<input class="chk_agregar" type="checkbox" value="'.$value["cliente_id"].'">';
                $array["id"]   = $value["cliente_id"];
                $array["typeperson"]   = $value["tipoper_nombre"];
                $array["name"] = $value["tipoiden_nombre"];
                $array["identification"] = $value["cliente_numero_identificacion"]."-". $value["cliente_digito_verificacion"];
                $array["business"] = $value["cliente_razon_social"];
                $array["email"] = $value["cliente_email"];
                $array["actions"] = '<div class="btn-group">
                                        <a class= "btn" id="a_detalle" href="'.SERVERURL.'catalog/wiewclients/'. $value["cliente_id"].'"><i class="fa fa-search"></i></a>
                                        <a class= "btn" id="a_editar" href="'.SERVERURL.'catalog/frmclient/'. $value["cliente_id"].'"><i class="fa fa-pencil"></i></a>
                                        <a class= "btn" id="a_remover" data-delete='.$value["cliente_id"].'><i class="fa fa-trash"></i></a>
                                    </div>';
                $datos[]         = $array;
            }
            // var_dump($const[2]);exit;
            $json_data = array(
                "draw"            => intval($_POST["draw"]),
                "recordsTotal"    => intval($const[1]),
                "recordsFiltered" => intval($const[2][0]),
                "data"            => $datos,
            );
            echo json_encode($json_data);
        }
    }

    // Vista lista de unidades de medida
    public function wiewclients()
    {
        $title = "| Detalle de producto";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        if (isset($_GET["key"])) {//Si hay una variable get
            // $data  = $_GET["id"];
            $data = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_NUMBER_INT);
            if (is_numeric($data)) {// Si es numerica
                $result = $this->model->infoDetailClient($data);
                if($result[0]){// Si $result tiene información
                    $data = $result[0];
                }else{
                    $this->home->error404body();
                    exit();
                }
            }else{
                $data = "";
            }
        }
        require_once "views/modules/mod_catalog/wiew_clients.php";
        require_once "views/include/footer.php";
    }

    //Eliminar cliente(s) masivamente
    public function deleteMassivelyClients()
    {
        $data = filter_input(INPUT_POST, 'data', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        // echo  "<pre>"; var_dump($data);exit();
        if (in_array('', $data) == false) {
            $result = $this->model->deleteMassivelyClients($data);
            echo json_encode($result);
        } else {
            $answer = array('answer' => 'forced');
            echo json_encode($answer);
        }
    }
        

    // Guardar o editar Cliente
    public function saveEditClient()
    {
    $data = filter_input(INPUT_POST, 'frmclient', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
    $taxpayer = filter_input(INPUT_POST, 'frmtaxpayer', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
    $additional = filter_input(INPUT_POST, 'frmadditional', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
    $address = filter_input(INPUT_POST, 'frmaddress', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
    // var_dump($data);exit();
    if (in_array('', $data) == false) {
        $result = $this->model->saveEditClients($data,$taxpayer,$additional,$address);
        echo json_encode($result);
    } else {
        $answer = array('answer' => 'forced');
        echo json_encode($answer);
    }
    }

    // Vista lista de unidades de medida
    public function listmeasureunit()
    {
        $title = "| Unidades de medida";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        require_once "views/modules/mod_catalog/list_measureunit.php";
        require_once "views/include/footer.php";
    }

    // Vista de formulario de unidades de medida
    public function frmmeasureunit()
    {
        $title = "| Unidades de medida";
        $this->home->checkStatus();
        require_once "views/include/headar.php";
        require_once "views/modules/mod_catalog/frm_measureunit.php";
        require_once "views/include/footer.php";
    }

    // Input select tipos de productos
    public function selectTypeProduct($data="")
    {
        $const = $this->model->listTypeProduct();
        foreach ($const as $key => $value) {
            if ($data == $value["tipproducto_codigo"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["tipproducto_codigo"] . '">' . $value["tipproducto_nombre"] . '</option>';
        }
    }

    // Input select unidad de medida
    public function selectUnit($data="")
    {
        $const = $this->model->listUnit();
        foreach ($const as $key => $value) {
            if ($data == $value["unimed_codigo"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["unimed_codigo"] . '">' . $value["unimed_nombre"] . '</option>';
        }
    }

    // Input select impuestos
    public function selectTax($data="")
    {
        $const = $this->model->listTax();
        foreach ($const as $key => $value) {
            if ($data == $value["impuesto_codigo"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["impuesto_codigo"] . '">' . $value["impuesto_nombre"] . '</option>';
        }
    }

    // Input select tipos de impuestos
    public function selectTypeTax($data="")
    {
        $const = $this->model->listTypeTax();
        foreach ($const as $key => $value) {
            if ($data == $value["porimp_codigo"]) {
                $sel = "selected";
            } else {
                $sel = "";
            }
            echo '<option '.$sel.' value="' . $value["porimp_codigo"] . '">' . $value["porimp_nombre"] . '</option>';
        }
    }

    // Tabla de unidad de medida 
    public function seeMeasureunit()
    {
        if (isset($_POST["start"],
            $_POST["length"],
            $_POST["order"][0]["column"],
            $_POST["order"][0]["dir"],
            $_POST["search"]["value"],
            $_POST["draw"])) {
            // Parametro de inicio
            $start = (int) $_POST["start"];
            // Parametro Final
            $length = (int) $_POST["length"];
            // Parametro Ordenar
            $orderColumn = $_POST["order"][0]["column"];
            $orderDir    = $_POST["order"][0]["dir"];
            // Parametro de busqueda
            $search = $_POST["search"]["value"];
            //Consulta
            $const = $this->model->listMeasureunit($start, $length, $search, $orderColumn, $orderDir);
            $datos = [];
            foreach ($const[0] as $row => $value) {
                $array           = [];
                $array["checkbox"]   = '<input class="chk_agregar" type="checkbox" value="'.$value["unimed_codigo"].'">';
                $array["unit"]   = $value["unimed_codigo"];
                $array["name"] = $value["unimed_nombre"];
                $datos[]         = $array;
            }
            // var_dump($const[2]);exit;
            $json_data = array(
                "draw"            => intval($_POST["draw"]),
                "recordsTotal"    => intval($const[1]),
                "recordsFiltered" => intval($const[2][0]),
                "data"            => $datos,
            );
            echo json_encode($json_data);
        }
    }

    //Editar o eliminar los unidades de medidas del emisor
    public function editMeasureunit()
    {
        $data = filter_input(INPUT_POST, 'data', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        // echo  "<pre>"; var_dump($data);exit();
        if (in_array('', $data) == false) {
            $result = $this->model->editDeleteMeasureunit($data);
            echo json_encode($result);
        } else {
            $answer = array('answer' => 'forced');
            echo json_encode($answer);
        }
    }

    // Tabla de unidad de medida Dian
    public function seeMeasureunitDian()
    {
        if (isset($_POST["start"],
            $_POST["length"],
            $_POST["order"][0]["column"],
            $_POST["order"][0]["dir"],
            $_POST["search"]["value"],
            $_POST["draw"])) {
            // Parametro de inicio
            $start = (int) $_POST["start"];
            // Parametro Final
            $length = (int) $_POST["length"];
            // Parametro Ordenar
            $orderColumn = $_POST["order"][0]["column"];
            $orderDir    = $_POST["order"][0]["dir"];
            // Parametro de busqueda
            $search = $_POST["search"]["value"];
            //Consulta
            $const = $this->model->listMeasureunitDian($start, $length, $search, $orderColumn, $orderDir);
            $datos = [];
            foreach ($const[0] as $row => $value) {
                $array           = [];
                $array["checkbox"]   = '<input class="chk_agregar" type="checkbox" value="'.$value["unimed_codigo"].'">';
                $array["unit"]   = $value["unimed_codigo"];
                $array["name"] = $value["unimed_nombre"];
                $datos[]         = $array;
            }
            // var_dump($const[2]);exit;
            $json_data = array(
                "draw"            => intval($_POST["draw"]),
                "recordsTotal"    => intval($const[1]),
                "recordsFiltered" => intval($const[2][0]),
                "data"            => $datos,
            );
            echo json_encode($json_data);
        }
    }

    //Guardar los unidades de medidas seleccionadas por el emisor
    public function saveMeasureunit()
    {
        $data = filter_input(INPUT_POST, 'data', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        // echo  "<pre>"; var_dump($separado_por_comas);exit();
        if (in_array('', $data) == false) {
            $data = implode(",", $data);
            $result = $this->model->saveMeasureunit($data);
            echo json_encode($result);
        } else {
            $answer = array('answer' => 'forced');
            echo json_encode($answer);
        }
    }
    // Tabla de productos
    public function seeProducts()
    {
        if (isset($_POST["start"],
            $_POST["length"],
            $_POST["order"][0]["column"],
            $_POST["order"][0]["dir"],
            $_POST["search"]["value"],
            $_POST["draw"])) {
            // Parametro de inicio
            $start = (int) $_POST["start"];
            // Parametro Final
            $length = (int) $_POST["length"];
            // Parametro Ordenar
            $orderColumn = $_POST["order"][0]["column"];
            $orderDir    = $_POST["order"][0]["dir"];
            // Parametro de busqueda
            $search = $_POST["search"]["value"];
            //Consulta
            $const = $this->model->listProducts($start, $length, $search, $orderColumn, $orderDir);
            $datos = [];
            foreach ($const[0] as $row => $value) {
                $array           = [];
                $array["checkbox"]   = '<input class="chk_agregar" type="checkbox" value="'.$value["producto_id"].'">';
                $array["id"]   = $value["producto_id"];
                $array["code"]   = $value["producto_codigo"];
                $array["description"] = $value["producto_descripcion"];
                $array["unit"] = $value["unimed_nombre"];
                $array["price"] = $value["producto_precio_unitario"];
                $array["tax"] = $value["impuesto_nombre"];
                $array["typetax"] = $value["porimp_nombre"];
                $array["typeproduct"] = $value["tipproducto_nombre"];
                $array["actions"] = '<div class="btn-group">
                                        <a class= "btn" id="a_detalle" href="'.SERVERURL.'catalog/wiewproducts/'. $value["producto_id"].'"><i class="fa fa-search"></i></a>
                                        <a class= "btn" id="a_editar" href="'.SERVERURL.'catalog/frmproducts/'. $value["producto_id"].'"><i class="fa fa-pencil"></i></a>
                                        <a class= "btn" id="a_remover" data-delete='.$value["producto_id"].'><i class="fa fa-trash"></i></a>
                                    </div>';
                $datos[]         = $array;
            }
            // var_dump($const[2]);exit;
            $json_data = array(
                "draw"            => intval($_POST["draw"]),
                "recordsTotal"    => intval($const[1]),
                "recordsFiltered" => intval($const[2][0]),
                "data"            => $datos,
            );
            echo json_encode($json_data);
        }
    }

    //Eliminar producto(s) masivamente
    public function deleteMassivelyProducts()
    {
        $data = filter_input(INPUT_POST, 'data', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        // echo  "<pre>"; var_dump($data);exit();
        if (in_array('', $data) == false) {
            $result = $this->model->deleteMassivelyProducts($data);
            echo json_encode($result);
        } else {
            $answer = array('answer' => 'forced');
            echo json_encode($answer);
        }
    }
    

    // Guardar o editar productos
    public function saveEditProduct()
    {
    $data = filter_input(INPUT_POST, 'frmproducts', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);

    if (in_array('', $data) == false) {
        $result = $this->model->saveEditProducts($data);
        echo json_encode($result);
    } else {
        $answer = array('answer' => 'forced');
        echo json_encode($answer);
    }
    }

    // Select para ciudad
    public function selectCity($data="")
    {
        if (isset($_POST["dato"])) {
            $departament = $_POST["dato"];
            if (!empty($departament)) {
                // var_dump($departament);exit();
                $const = $this->model->listCity($departament);
                // var_dump($const[1]);exit();
                foreach ($const[0] as $key => $value) {
                    if ($const[1] == $value["ciudad_id"]) {
                        $sel = "selected";
                    } else {
                        $sel = "";
                    }
                    echo ('<option '.$sel.' value="' . $value["ciudad_id"] . '">' . ucfirst(strtolower($value["ciudad_nombre"])) . '</option>');
                }
            }
            
            // var_dump( $departament );exit();
        }

    }

    
}
