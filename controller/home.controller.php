<?php
require_once 'model/home.model.php';
require_once 'libs/recaptchalib.php';
require_once 'libs/emails.php';


class HomeController
{
    private $model;

    public function __CONSTRUCT()
    {
        $this->model = new HomeModel();
    }

    // Vista inicio de seccion 
    public function login()
    {           
        require_once "views/include/headarbegin.php";
        require_once "views/modules/mod_home/login.php";
        require_once "views/include/footerbegin.php";
    }

    public function checkStatus(){
        $result = $this->model->seeStatus();
      
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
            $url = "https://"; 
          }else{
            $url = "http://"; 
          }
          $url_actual = $url . $_SERVER['HTTP_HOST'] .  $_SERVER['REQUEST_URI'];
        //   var_dump( SERVERURL . "myaccount/frmpassword");exit();

        if ($result == "3" && SERVERURL . "myaccount/frmpassword" != $url_actual) {
         
            header("location:" . SERVERURL . "myaccount/frmpassword");

            // exit();
        }else if($result == "4" && SERVERURL . "myaccount/frmaccount" != $url_actual){
            header("location:" . SERVERURL . "myaccount/frmaccount");
            // exit();
        }
    }
   

    public function index()
    {
        
        $title = "| Inicio";
        $this->checkStatus();
        require_once "views/include/headar.php";
        require_once "views/modules/mod_home/main.php";
        require_once "views/include/footer.php";
    }

    public function error404()
    {
        require_once "views/modules/mod_home/error404.php";
    }

    public function error404body()
    {
        require_once "views/modules/mod_home/error404body.php";
    }



    // Vista restablecer contraseña 
    public function restorePassword()
    {   
        //  var_dump($_GET["key"]);exit();
        $title = "| Restablecer contraseña";
        if (isset($_GET["key"])) {//Si hay una variable get
            $data = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_STRING);
            $data = explode("_", $data);
            $result = $this->model->restorePassword(base64_decode($data[0]."="), $data[1]);
            // var_dump($result);exit();
            if( $result == true){
                require_once "views/include/headarbegin.php";
                require_once "views/modules/mod_home/restore.php";
                require_once "views/include/footerbegin.php";
            }else{
                $this->error404();exit();
            }
        }else{
            $this->error404();exit();
        }
        
       

    }

    public function confirmAccount()
    {
        $title = "| Confirmación email";
        if (isset($_GET["key"])) {//Si hay una variable get
            $data = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_STRING);
            $result = $this->model->activeIssuing(base64_decode($data."="));
            // var_dump($result);exit();
            if($result == "confirmed"){
                require_once "views/modules/mod_home/get_swal.php";
                echo '<script type="text/javascript">
                const url = window.location;
                let baseurl = "",
                    answer = "";
                if (url.hostname == "localhost") {
                    baseurl = url.origin + "/" + url.pathname.split("/")[1] + "/";
                } else {
                    baseurl = url.origin + "/";
                }
                swal({
                    title: "El usuario",
                    text: "Ya fue confirmado satisfactoriamente.",
                    type: "success",
                    confirmButtonColor: "#3c8dbc",
                    showConfirmButton: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                        location.href = baseurl + "home/login";
                    }
                }
                );
                </script>';
            }else if($result == 'success_edit'){
                require_once "views/modules/mod_home/get_swal.php";
                echo '<script type="text/javascript">
                const url = window.location;
                let baseurl = "",
                    answer = "";
                if (url.hostname == "localhost") {
                    baseurl = url.origin + "/" + url.pathname.split("/")[1] + "/";
                } else {
                    baseurl = url.origin + "/";
                }
                swal({
                    title: "Su cuenta ha sido confirmada correctamente",
                    text: "Debe iniciar sesión para activar el usuario.",
                    type: "success",
                    confirmButtonColor: "#3c8dbc",
                    showConfirmButton: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                        location.href = baseurl + "home/login";
                    }
                }
                );
                </script>';
            }else if($result == "previously"){
                echo '<script type="text/javascript">swal({title: "OOPS!!",text: "Error 500 - Error de servidor interno.",type: "success",confirmButtonColor: "#3c8dbc",showConfirmButton: true,});</script>';
                require_once "views/modules/mod_home/get_swal.php";
                echo '<script type="text/javascript">
                const url = window.location;
                let baseurl = "",
                    answer = "";
                if (url.hostname == "localhost") {
                    baseurl = url.origin + "/" + url.pathname.split("/")[1] + "/";
                } else {
                    baseurl = url.origin + "/";
                }
                swal({
                    title: "El usuario",
                    text: "Ya fue confirmado satisfactoriamente..",
                    type: "info",
                    confirmButtonColor: "#3c8dbc",
                    showConfirmButton: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                        location.href = baseurl + "home/login";
                    }
                }
                );
                </script>';
            }else if($result == false){
                $this->error404();exit();
            }else{
                // echo'<script type="text/javascript">alert("Su cuenta ha sido confirmada correctamente, debe iniciar sesión para activar el usuario."); location.href = baseurl + "home/index"; </script>';
                echo '<script type="text/javascript">swal({title: "OOPS!!",text: "Error 500 - Error de servidor interno.",type: "success",confirmButtonColor: "#3c8dbc",showConfirmButton: true,});</script>';
                require_once "views/modules/mod_home/get_swal.php";
                echo '<script type="text/javascript">
                const url = window.location;
                let baseurl = "",
                    answer = "";
                if (url.hostname == "localhost") {
                    baseurl = url.origin + "/" + url.pathname.split("/")[1] + "/";
                } else {
                    baseurl = url.origin + "/";
                }
                swal({
                    title: "OOPS!!",
                    text: "Error 500 - Error de servidor interno.",
                    type: "error",
                    confirmButtonColor: "#3c8dbc",
                    showConfirmButton: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                        location.href = baseurl + "home/login";
                    }
                }
                );
                </script>';
            }
        }
    }

    //Recuperacion de contraseña desde el login
    public function restoreEmailLogin()
    {   
        if ($_SERVER['HTTP_HOST'] == "localhost") {
            $secretKey = '6Lcxq3MaAAAAAE5FQY57G1waaSHW2uKlEwJyx8JZ'; // secretKeyLocal
        }else if($_SERVER['HTTP_HOST'] == "businesfactory.co"){
            $secretKey = '6LfEoXMaAAAAAGQ51G0-0udThuPO9-WH5_fKpcKb'; // secretKeyWeb
        }

		$response = false;
        if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['g-recaptcha-response'])){
            $objRecaptcha = new ReCaptcha($secretKey);
            //igualo el $response->success a 1, para la entrega del dia 17 , asi siempre pasa el if.
            $response = $objRecaptcha->verifyResponse($_SERVER['REMOTE_ADDR'], $_POST['g-recaptcha-response']);
            if ($response == true) {
                $data = filter_input(INPUT_POST, 'frmrestoreemail', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
                $result = $this->model->restoreEmail($data);
                // var_dump($result);exit();
                if (is_array($result)) {
                    $token = base64_encode($result[1]);
                    // var_dump($token);exit();
                    $token = str_replace ( "=", '', $token);
                    // var_dump($token);exit();
                    $issue = "Su enlace de restablecimiento de contraseña";
                    if ($_SERVER['HTTP_HOST'] == "localhost") {
                        $url = 'localhost/bussinesfactory'; 
                    }elseif($_SERVER['HTTP_HOST'] == "businesfactory.co"){
                        $url = 'businesfactory.co'; 
                    }
                    //Preparamos el mensaje a enviar
                    $msghmlt = '<br>
                    <p>Estimado(a) cliente, ha solicitado restablecer su contraseña en el sistema de Facturación Electrónica.</p>
                    <p>Para continuar por favor haga click sobre el botón.</p>
                    <a style = " 
                    box-shadow: -5px -1px 14px -6px #276873;
                    background-color:#187394;
                    border-radius:8px;
                    display:inline-block;
                    cursor:pointer;
                    color:#ffffff;
                    font-family:Arial;
                    font-size:13px;
                    font-weight:bold;
                    padding:13px 32px;
                    text-decoration:none;
                    text-shadow:0px 1px 0px #3d768a;"href="http://'.$url.'/home/restorePassword/'. $token .'_'.$result[0].'" target="_blank">Restablecer contrasela</a>
                    <p><b>Business Factory</b> - Tel. +574-311.11.11 - <a style="color: #179dba; text-decoration: underline; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;" href="http://businesfactory.co/" target="_blank">businesfactory.co</a> </p>
                    ';
                    //enviamos correo al emisor para restablecer contaseña
                    $send = sendEmail($result[1], $issue, $msghmlt, "");
                    if ( $send == true) {//Si se envio el correo
                        echo json_encode(array('answer' =>  'send'));
                    }else{
                        echo json_encode(array('answer' =>  'error'));
                    }
                }else{
                    echo json_encode(array('answer' =>  $result));
                }
            }else{
                $answer = array('answer' => 'error');
                echo json_encode($answer);
            }
        }else{
            $answer = array('answer' => 'emptycaptcha');
            echo json_encode($answer);
        }
    }

    public function loginIssuing()
    {   
        if ($_SERVER['HTTP_HOST'] == "localhost") {
            $secretKey = '6Lcxq3MaAAAAAE5FQY57G1waaSHW2uKlEwJyx8JZ'; // secretKeyLocal
        }else if($_SERVER['HTTP_HOST'] == "businesfactory.co"){
            $secretKey = '6LfEoXMaAAAAAGQ51G0-0udThuPO9-WH5_fKpcKb'; // secretKeyWeb
        }

		$response = false;
        if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['g-recaptcha-response'])){
            $objRecaptcha = new ReCaptcha($secretKey);
            //igualo el $response->success a 1, para la entrega del dia 17 , asi siempre pasa el if.
            $response = $objRecaptcha->verifyResponse($_SERVER['REMOTE_ADDR'], $_POST['g-recaptcha-response']);
            if ($response == true) {
                $data = filter_input(INPUT_POST, 'frmlogin', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
                $result = $this->model->loginIssuing($data);
                echo json_encode($result);
            }else{
                $answer = array('answer' => 'error');
                echo json_encode($answer);
            }
        }else{
            $answer = array('answer' => 'emptycaptcha');
            echo json_encode($answer);
        }
    }

    public function registerIssuing()
    {  
 
        // var_dump($_POST["frmregister"]);exit();
        if ($_SERVER['HTTP_HOST'] == "localhost") {
            $secretKey = '6Lcxq3MaAAAAAE5FQY57G1waaSHW2uKlEwJyx8JZ'; // secretKeyLocal
        }elseif($_SERVER['HTTP_HOST'] == "businesfactory.co"){
            $secretKey = '6LfEoXMaAAAAAGQ51G0-0udThuPO9-WH5_fKpcKb'; // secretKeyWeb
        }
        
        $response = false;
        // var_dump($response);exit();
        if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['g-recaptcha-response'])){
            $objRecaptcha = new ReCaptcha($secretKey);
            
            //igualo el $response->success a 1, para la entrega del dia 17 , asi siempre pasa el if.
            $response = $objRecaptcha->verifyResponse($_SERVER['REMOTE_ADDR'], $_POST['g-recaptcha-response']);
            if ($response == true) {
                $data = filter_input(INPUT_POST, 'frmregister', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
                $result = $this->model->registerIssuing($data);
                // echo json_encode($result);
                if (is_array($result)) {// si hubo registo me y me retorna un array
                    //Preparamos correo para enviar al emisor como asunto y mensaje
                    $token = base64_encode($data[1]);
                    // var_dump($token);exit();
                    $token = str_replace ( "=", '', $token);
                    // var_dump($token);exit();
                    $issue = "Activa tu cuenta de Facturación Electrónica con Bussiness Factory";
                    if ($_SERVER['HTTP_HOST'] == "localhost") {
                        $url = 'localhost/bussinesfactory'; 
                    }elseif($_SERVER['HTTP_HOST'] == "businesfactory.co"){
                        $url = 'businesfactory.co'; 
                    }
                    //Preparamos el mensaje a enviar
                    $msghmlt = ' <div>
                        <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                                <tr>
                                    <td>
                                        <table style="width: 100%; max-width: 600px;" border="0" cellspacing="0" cellpadding="0" align="center">
                                            <tbody>
                                                <tr>
                                                    <td style="font-size: 1px; line-height: 1px;" colspan="1" height="45px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="center" width="128" height="25">
                                                        <div>
                                                            <img style="margin: 0px 12px 0px 0px; display: block; border: none; outline: none; text-decoration: none; text-align: center; font-size: 20px;"
                                                                title="Business Factory" src="https://1.bp.blogspot.com/-fgwn4pJt8mY/YJoOEy6a55I/AAAAAAAAF5I/RXc9j1_cUhU252TvWx_sj_SWjZ90wh-2ACLcBGAsYHQ/s1954/Logo%2BOriginal%2BBusiness%2BFactory.png" alt="Business Factory" width="200" height="60" align="center" border="0"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 1px; line-height: 1px;" colspan="1" height="30px">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 100%; max-width: 600px;" border="0" cellspacing="0" cellpadding="0" align="center">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table border="0" cellspacing="0" cellpadding="0" align="center">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="font-size: 1px; line-height: 1px;" colspan="3" height="1">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 1px;">&nbsp;</td>
                                                                    <td>
                                                                        <table style="border: 1px solid #bfc0cd; border-collapse: separate; border-radius: 3px; width: 100%; max-width: 560px;" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="font-size: 1px; line-height: 1px;" colspan="3" height="30px">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 45px; display: block;">&nbsp;</td>
                                                                                    <td>
                                                                                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #484a6f; font-weight: normal; text-align: center; line-height: 22px;"align="center" width="100%">
                                                                                                        <p>Bienvenido(a) <b>'.$data[0].'. Ha sido registrado en el sistema de Facturación Electrónica Colombia.</p>
                                                                                                        <p>Su contraseña es: <b>'.$result[0].'</b></p>
                                                                                                        <p>Para confirmar su registro por favor haga click en el boton.</p>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="font-size: 1px; line-height: 1px;" colspan="1" height="30px">&nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="center" valign="middle" width="100%">
                                                                                                        <div>
                                                                                                            <table border="0" cellspacing="0" cellpadding="0" align="center">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style="display: block; padding: 2px; line-height: 20px;" align="center" height="40">
                                                                                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                                                                                <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td>
                                                                                                                                            <a href="http://'.$url.'/home/confirmAccount/'. $token .'" target="_blank">Confirmar cuenta</a> 
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td style="width: 45px; display: block;">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="font-size: 1px; line-height: 1px;" colspan="3" height="30px">&nbsp;</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 1px;">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-size: 1px; line-height: 1px;" colspan="3" height="1">&nbsp;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 1px; line-height: 1px;" colspan="1" height="15px">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 100%; max-width: 600px;" border="0" cellspacing="0" cellpadding="0" align="center">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%; max-width: 560px;" border="0" cellspacing="0" cellpadding="0" align="center">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width: 15px; display: block;">&nbsp;</td>
                                                                    <td style="font-family: \'Helvetica Neue\', Arial, sans-serif; font-size: 10px; color: #b4babd; line-height: 18px; text-align: center;" align="center">
                                                                        Has recibido este correo electrónico porque has solicitado una cuenta en
                                                                        <a style="color: #179dba; text-decoration: underline; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;" href="http://businesfactory.co/" target="_blank">businesfactory.co</a> con esta dirección de correo electrónico.<br/>
                                                                        Si no tenías la intención de hacerlo, puedes ignorar este correo electrónico: la cuenta todavía no se ha creado.<br/> Business Factory - Tel. +574-311.11.11
                                                                    </td>
                                                                    <td style="width: 15px; display: block;">&nbsp;</td>
                                                                </tr>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>';
                    //enviamos correo al emisor para acticar la cuenta 
                    $send = sendEmail($data[1], $issue, $msghmlt, $data[0]);
                    if ( $send == true) {//Si se envio el correo
                        echo json_encode(array('answer' =>  'send'));
                    }else{
                        echo json_encode(array('answer' =>  'error'));
                    }
                
                }else{
                    echo json_encode(array('answer' =>  $result));
                }
                
            }else{
                $answer = array('answer' => 'error');
                echo json_encode($answer);
            }
        }else{
            $answer = array('answer' => 'emptycaptcha');
            echo json_encode($answer);
        }
    }

    //Restablecer contaseña
    public function restorePass()
    {
        if ($_SERVER['HTTP_HOST'] == "localhost") {
            $secretKey = '6Lcxq3MaAAAAAE5FQY57G1waaSHW2uKlEwJyx8JZ'; // secretKeyLocal
        }else if($_SERVER['HTTP_HOST'] == "businesfactory.co"){
            $secretKey = '6LfEoXMaAAAAAGQ51G0-0udThuPO9-WH5_fKpcKb'; // secretKeyWeb
        }

		$response = false;
        if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['g-recaptcha-response'])){
            $objRecaptcha = new ReCaptcha($secretKey);
            //igualo el $response->success a 1, para la entrega del dia 17 , asi siempre pasa el if.
            $response = $objRecaptcha->verifyResponse($_SERVER['REMOTE_ADDR'], $_POST['g-recaptcha-response']);
            if ($response == true) {
                $data = filter_input(INPUT_POST, 'frmpass', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
                if ($data[1] == $data[2]) {
                    $result = $this->model->restorePass($data);
                    echo json_encode($result);
                }else{//Si no estan igual las contaseñas
                    echo json_encode(array('answer' => 'same'));
                }
            }else{
                $answer = array('answer' => 'error');
                echo json_encode($answer);
            }
        }else{
            $answer = array('answer' => 'emptycaptcha');
            echo json_encode($answer);
        }
       
    }

    public function seeDataIssuing()
    {
        $const = $this->model->seeIssuing();
            echo '  <h2>'.$const[0].'</h2>
            <span>NIT:'.$const[1].'-'.$const[2].'</span>';
    }


    public function logout()
    {
        require_once 'logout.php';
    }

}
