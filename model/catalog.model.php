<?php
require_once 'model/conn.model.php';

class CatalogModel{

   private $pdo;
   private $fechahora;

   public function __CONSTRUCT(){
     try{
       $this->pdo = DataBase::connect();
       date_default_timezone_set('America/Bogota');
       $this->fechahora = date("Y-m-d H:i:s");
     }catch(Exception $e){
       die($e->getMessage());
     }
   }
  // Listar tipos de productos
   public function listTypeProduct()
   {
       try {
           $sql   = "SELECT * FROM tbl_tipo_producto ORDER BY tipproducto_nombre ASC";
           $query = $this->pdo->prepare($sql);
           $const = $query->execute();
           $const = $query->fetchALL(PDO::FETCH_BOTH);
           return $const;
       } catch (\Exception $e) {
           die($e->getMessage());
       }
   }

       // Listar unidades de medidas
       public function listUnit()
       {
           try {
            // session_start();
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT * FROM tbl_unidades_medida WHERE emisor_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetch(); 
            $data = explode(",", $const["unimed_codigos"]);
            foreach ($data as $key => $value) {
                $array  = [];
                $array  = "'$value'"; 
                $datos[] = $array;
            }
            $datos = implode(",",$datos);

            $sql   = "SELECT * FROM tbl_unidades_medida_dian WHERE unimed_codigo IN (".$datos.")  ORDER BY unimed_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
           } catch (\Exception $e) {
               die($e->getMessage());
           }
       }

    // Listar impuestos
    public function listTax()
    {
        try {
            $sql   = "SELECT * FROM tbl_tipo_impuesto ORDER BY impuesto_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

   // Listar tipos de impuestos
   public function listTypeTax()
   {
       try {
           $sql   = "SELECT * FROM tbl_porcentaje_impuesto ORDER BY porimp_nombre ASC";
           $query = $this->pdo->prepare($sql);
           $const = $query->execute();
           $const = $query->fetchALL(PDO::FETCH_BOTH);
           return $const;
       } catch (\Exception $e) {
           die($e->getMessage());
       }
   }


   // Listar unidades de medida 
   public function listMeasureunit($start, $length, $search, $orderColumn, $orderDir)
   {
       try {
            session_start();
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT * FROM tbl_unidades_medida WHERE emisor_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetch();

            if ($const != false) {//Si consigue datos en el consulta anterior            
                $data = explode(",", $const["unimed_codigos"]);
                foreach ($data as $key => $value) {
                    $array  = [];
                    $array  = "'$value'"; 
                    $datos[] = $array;
                }
                $datos = implode(",",$datos);
                
                $orderColumns = $orderColumn + 1;
                $searchs      = "";
                if ($search) {
                    $searchs = " AND (  unimed_codigo LIKE '%" . $search . "%'
                    OR unimed_nombre LIKE '%" . $search . "%' )";                             
                    
                }
                $sql = "SELECT COUNT(1) FROM tbl_unidades_medida_dian WHERE unimed_codigo IN (".$datos.") ";
                $query = $this->pdo->prepare($sql);
                $count = $query->execute();
                $count = $query->fetch();

                $sql = "SELECT  * FROM  tbl_unidades_medida_dian WHERE unimed_codigo  IN ($datos) $searchs
                ORDER BY  $orderColumns $orderDir
                LIMIT $start, $length";
                $query = $this->pdo->prepare($sql);
                $const = $query->execute();
                $const = $query->fetchALL(PDO::FETCH_BOTH);
                $rows  = $query->rowCount();


                $datos = array($const, $rows, $count);
                return $datos;
                $this->disconnect;
            }else{
                $const = [];
                $rows = 0;
                $count = "0";
                $datos = array($const, $rows, $count);
                return $datos;
            }
       } catch (\Exception $e) {
           die($e->getMessage());
       }
   }

       //Consultar Clnite
       public function infoClient($data)
       {
           try {
            //    session_start();
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT * FROM tbl_clientes WHERE cliente_id = ? AND cliente_id_emisor = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($data,$id));
            $const = $query->fetch();
            return $const;
           } 
           catch (Exception $e) {
               die($e->getMessage());
           }
       }

   // Listar unidades de medida Dian
   public function listMeasureunitDian($start, $length, $search, $orderColumn, $orderDir)
   {
       try {
            session_start();
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT * FROM tbl_unidades_medida WHERE emisor_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetch();
            if ($const == false) {
                $where = "";
            }else{
                $data = explode(",", $const["unimed_codigos"]);
                foreach ($data as $key => $value) {
                    $array  = [];
                    $array  = "'$value'"; 
                    $datos[] = $array;
                }
                $datos = implode(",",$datos);
                $where = "WHERE unimed_codigo NOT IN (".$datos.")";
            }
            $orderColumns = $orderColumn + 1;
            $searchs      = "";
            if ($search) {
                if ($where == "") {
                    $searchs = " WHERE (  unimed_codigo LIKE '%" . $search . "%'
                    OR unimed_nombre LIKE '%" . $search . "%' )";
                }else{
                    $searchs = " AND (  unimed_codigo LIKE '%" . $search . "%'
                    OR unimed_nombre LIKE '%" . $search . "%' )";
                }
                
            }
            $sql = "SELECT COUNT(1)     FROM tbl_unidades_medida_dian $where";
            $query = $this->pdo->prepare($sql);
            $count = $query->execute();
            $count = $query->fetch();

            $sql = "SELECT  * FROM  tbl_unidades_medida_dian $where $searchs
            ORDER BY  $orderColumns $orderDir
            LIMIT $start, $length";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            $rows  = $query->rowCount();

            //  echo "<pre>"; var_dump($sql);exit();

            $datos = array($const, $rows, $count);
            return $datos;
            $this->disconnect;
       } catch (\Exception $e) {
           die($e->getMessage());
       }
   }

   // Editar o eliminar Unidades de medida del emisor
   public function editDeleteMeasureunit($data){
    try {
        session_start();
        $id = $_SESSION["emisor_id"];
    
        $sql   = "SELECT * FROM tbl_unidades_medida WHERE emisor_id = ?";
        $query = $this->pdo->prepare($sql);
        $const = $query->execute(array($id));
        $const = $query->fetch();
        if ($const != false) {
            $const = explode(",", $const["unimed_codigos"]);    
            $result_array = array_diff($const,$data);//Elimina todo lo que venga de la variabla $data a $const
            if (!empty($result_array)) {//Si la variable $result_array tiene datos
                $result_array = implode(",", $result_array);
                $sql   = "UPDATE tbl_unidades_medida SET unimed_codigos = ? WHERE emisor_id = ?";
                $query = $this->pdo->prepare($sql);
                $result = $query->execute(array($result_array,$id));
                if ($result == true) {
                    return  array('answer' => 'success_save');
                }else{
                  return  array('answer' => 'error');
                }
                }else{ //Si la variable $result_array no tiene datos
                    $sql   = "DELETE FROM tbl_unidades_medida WHERE emisor_id = ?";
                    $query = $this->pdo->prepare($sql);
                    $result = $query->execute(array($id));
                    if ($result == true) {
                        return  array('answer' => 'success_save');
                    }else{
                      return  array('answer' => 'error');
                    }
                }
        }else{
            return  array('answer' => 'error');
        }
    } catch (\Exception $e) {
        die($e->getMessage());
    }
   }

   // Guardar Unidades de medida
   public function saveMeasureunit($data){
    try {
        session_start();
        $id = $_SESSION["emisor_id"];
    
        $sql   = "SELECT * FROM tbl_unidades_medida WHERE emisor_id = ?";
        $query = $this->pdo->prepare($sql);
        $const = $query->execute(array($id));
        $const = $query->fetch();
        // echo "<pre>"; var_dump($const);exit();     
        if ($const == false) { //Si no tiene ningun registro de unidad de medida    
            $sql    = "INSERT INTO tbl_unidades_medida (emisor_id,unimed_codigos) VALUES (?,?)";
            $query  = $this->pdo->prepare($sql);
            $result = $query->execute(array($id,$data));
            if ($result == true) {
                return  array('answer' => 'success_save');
            }                
        }else { //Si tiene registros unidad de medida agresadas anteriormente
              $data = $const["unimed_codigos"].",".$data;  
              $sql    = "UPDATE tbl_unidades_medida SET unimed_codigos = ? WHERE emisor_id = ?";
              $query  = $this->pdo->prepare($sql);
              $result = $query->execute(array($data,$id));
              if ($result == true) {
                  return  array('answer' => 'success_save');
              }else{
                return  array('answer' => 'error');
              }
              
        }
        
    } catch (\Exception $e) {
        die($e->getMessage());
    }
   }

      // Listar productos
      public function listProducts($start, $length, $search, $orderColumn, $orderDir)
      {
          try {
              session_start();
              $id = $_SESSION["emisor_id"];
              $orderColumns = $orderColumn + 1;
              $searchs      = "";
              if ($search) {
                  $searchs = " (  producto_codigo LIKE '%" . $search . "%'
                                  OR producto_descripcion LIKE '%" . $search . "%'
                                  OR unimed_nombre LIKE '%" . $search . "%'
                                  OR producto_precio_unitario LIKE '%" . $search . "%'
                                  OR impuesto_nombre LIKE '%" . $search . "%'
                                  OR tipproducto_nombre LIKE '%" . $search . "%' ) AND";
              }
              $sql = "SELECT COUNT(1) FROM tbl_producto 
              WHERE  producto_estado_activo = 1 AND producto_id_emisor = $id";
              $query = $this->pdo->prepare($sql);
              $count = $query->execute();
              $count = $query->fetch();
   
           //    var_dump($sql);exit();
   
              $sql = "SELECT producto_id,
              producto_codigo, 
              producto_descripcion, 
              unimed_nombre, 
              producto_precio_unitario, 
              producto_id_emisor,
              impuesto_nombre,
              porimp_nombre,
              tipproducto_nombre
              FROM tbl_producto 
              INNER JOIN tbl_unidades_medida_dian ON producto_id_unidad_medida = unimed_codigo
              INNER JOIN  tbl_tipo_impuesto ON producto_id_tipo_impuesto = impuesto_codigo
              INNER JOIN  tbl_porcentaje_impuesto ON producto_id_porc_impuesto = porimp_codigo
              INNER JOIN tbl_tipo_producto ON producto_id_tipo_producto = tipproducto_codigo
              WHERE $searchs producto_estado_activo = 1 AND producto_id_emisor = $id
              ORDER BY  $orderColumns $orderDir
              LIMIT $start, $length";
              $query = $this->pdo->prepare($sql);
              $const = $query->execute();
              $const = $query->fetchALL(PDO::FETCH_BOTH);
              $rows  = $query->rowCount();
   
           //    var_dump($sql);exit();
   
              $datos = array($const, $rows, $count);
              return $datos;
              $this->disconnect;
          } catch (\Exception $e) {
              die($e->getMessage());
          }
      }

    //Eliminar producto(s) masivamente
    public function deleteMassivelyProducts($data){
        try {
            $const = false;
            session_start();
            $id = $_SESSION["emisor_id"];
            //Genera n veces (?) de lo que venga en el array $data para ser utilizado  IN 
            $values = count($data);
            $criteria = sprintf("?%s", str_repeat(",?", ($values ? $values-1 : 0)));
            if ($const == false) {
                $sql   = "DELETE FROM tbl_producto WHERE producto_id_emisor = ? AND producto_id IN ($criteria)";
                $query = $this->pdo->prepare($sql);
                array_unshift($data,$id);//Preparar los valores 
                $result = $query->execute($data);
                if ($result == true) {
                    return  array('answer' => true);
                }else{
                    return  array('answer' => false);
                }
            }else{
                $active = 0;
                $sql   = "UPDATE tbl_producto SET producto_estado_activo = ? WHERE producto_id_emisor = ? AND producto_id IN ($criteria)";
                $query = $this->pdo->prepare($sql);
                array_unshift($data,$active,$id);//Preparar los valores 
                $result = $query->execute($data);
                if ($result == true) {
                    return  array('answer' => true);
                }else{
                    return  array('answer' => false);
                }
                
                $this->disconnect;
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    //Consultar productos
    public function infoProduct($data)
    {
        try {
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT  * FROM tbl_producto WHERE producto_id = ? AND producto_id_emisor = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($data, $id));
            $const = $query->fetch();
            // echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando
            // echo "<pre>";  var_dump($result);exit();
            // var_dump($const);exit();
            return array($const);
            $this->disconnect;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    //Consultar productos con relacion
    public function infoDetailProduct($data)
    {
        try {
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT producto_id,
                            producto_codigo, 
                            producto_descripcion, 
                            unimed_nombre, 
                            producto_precio_unitario, 
                            producto_id_emisor,
                            impuesto_nombre,
                            porimp_nombre,
                            tipproducto_nombre
                            FROM tbl_producto 
                            INNER JOIN tbl_unidades_medida_dian ON producto_id_unidad_medida = unimed_codigo
                            INNER JOIN  tbl_tipo_impuesto ON producto_id_tipo_impuesto = impuesto_codigo
                            INNER JOIN  tbl_porcentaje_impuesto ON producto_id_porc_impuesto = porimp_codigo
                            INNER JOIN tbl_tipo_producto ON producto_id_tipo_producto = tipproducto_codigo
                            WHERE producto_id = ? AND producto_id_emisor = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($data,$id));
            $const = $query->fetch();
            return array($const);
            $this->disconnect;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

   // Guardar o editar productos
   public function saveEditProducts($data){
    try {
        session_start();
        $id = $_SESSION["emisor_id"];
        if ($data[0] == "0") {// Si es para guardar un producto
            $sql   = "SELECT producto_codigo FROM tbl_producto WHERE producto_codigo = ? AND producto_id_emisor = ? AND producto_estado_activo <> 0";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($data[2],$id));
            $const = $query->fetch();
            if ($const == false) { // Si el producto no existe y esta inactivo
                $active = "1";     
                $sql    = "INSERT INTO tbl_producto (producto_id_tipo_producto, producto_codigo, producto_descripcion, producto_id_unidad_medida, producto_precio_unitario, producto_id_emisor, producto_estado_activo, producto_id_tipo_impuesto, producto_id_porc_impuesto) VALUES (?,?,?,?,?,?,?,?,?)";
                $query  = $this->pdo->prepare($sql);                
                $result = $query->execute(array($data[1], $data[2], $data[3],$data[4],$data[5],$id,$active,$data[6],$data[7]));
                // echo "<pre>";  var_dump($data);exit();
                if ($result == true) {
                    return  array('answer' => 'success_save');
                }                
            }else if ($const[0] == $data[2]) {
                return array('answer' => 'exists');
            }else{
                return array('answer' => 'error');
            }
        }else if ($data[0] == "1"){ //Si es para editar un producto
            $sql   = "SELECT producto_codigo FROM tbl_producto WHERE  producto_id_emisor = ? AND producto_estado_activo <> 0";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetchAll();
            //    echo "<pre>";  var_dump($const);exit();
            if(in_array($data[2], array_column($const, 'producto_codigo'))){ //Si existe algun o algunos codigos
                $sql   = "SELECT producto_id, producto_codigo FROM tbl_producto WHERE  producto_codigo = ? AND producto_id_emisor = ? AND producto_estado_activo <> 0";
                $query = $this->pdo->prepare($sql);
                $const = $query->execute(array($data[2],$id));
                $const = $query->fetchAll();
                $rows = $query->rowCount();

                if ($rows > 1) {//Si existe mas de un codigo
                    return array('answer' => 'exists');
                }else{
                    // echo "<pre>";  var_dump($data[8]);exit();
                    foreach ($const as $key => $value) {
                        if ($value["producto_id"] == $data[8]) {// Si encontro el id que pertenece para poder actualizar
                            $sql    = "UPDATE tbl_producto SET producto_id_tipo_producto = ?, producto_codigo = ?, producto_descripcion = ?, producto_id_unidad_medida = ?, producto_precio_unitario = ?, producto_id_tipo_impuesto = ?, producto_id_porc_impuesto = ?, producto_fechamodify = ? WHERE producto_id = ?";
                            $query  = $this->pdo->prepare($sql);                
                            $result = $query->execute(array($data[1],$data[2], $data[3],$data[4],$data[5],$data[6],$data[7],$this->fechahora,$data[8]));
                            if ($result == true) {
                                return  array('answer' => 'success_edit');
                            }else{
                                return  array('answer' => 'error');
                            }             

                        }else{
                            return array('answer' => 'exists');
                        }
                    }
                }
            }else{//Si no hay ningun codigo
                $sql    = "UPDATE tbl_producto SET producto_id_tipo_producto = ?, producto_codigo = ?, producto_descripcion = ?, producto_id_unidad_medida = ?, producto_precio_unitario = ?, producto_id_tipo_impuesto = ?, producto_id_porc_impuesto = ?, producto_fechamodify = ? WHERE producto_id = ?";
                $query  = $this->pdo->prepare($sql);                
                $result = $query->execute(array($data[1],$data[2], $data[3],$data[4],$data[5],$data[6],$data[7],$this->fechahora,$data[8]));
                if ($result == true) {
                    return  array('answer' => 'success_edit');
                }else{
                    return  array('answer' => 'error');
                }             
            }
        }
    } catch (\Exception $e) {
        die($e->getMessage());
    }
   }

    // Guardar o editar clientes
    public function saveEditClients($data,$taxpayer,$additional,$address)
    {
        try {
            if (!empty($taxpayer)&&!empty($address)&&!empty($additional)&&!empty($data)) {
                session_start();
                $id = $_SESSION["emisor_id"];
                // var_dump("df");exit();
                if ($data[0] == "0") {// Si es para guardar un cliente
                    $sql   = "SELECT cliente_numero_identificacion FROM tbl_clientes WHERE cliente_numero_identificacion = ? AND cliente_id_emisor = ? AND cliente_estado_activo <> 0";
                    $query = $this->pdo->prepare($sql);
                    $const = $query->execute(array($taxpayer[4],$id));
                    $const = $query->fetch();
                    //  var_dump($const);exit();
                    if ($const == false) { // Si el cliente no existe y esta inactivo
                        $active = "1";     
                        $sql    = " INSERT INTO tbl_clientes (cliente_id_tipo_persona, cliente_razon_social, cliente_nombre_comercial, cliente_tipoiden_id, cliente_numero_identificacion, cliente_digito_verificacion, cliente_id_regimen_fiscal, cliente_email, cliente_telefono, cliente_nombre_contacto,
                                    cliente_actividad_economica, cliente_id_respons_fiscal, cliente_id_detalle_tributario,
                                    cliente_id_pais, cliente_id_departamento, cliente_id_ciudad,cliente_direccion, cliente_codigo_postal,
                                    cliente_id_lenguaje, cliente_estado_activo, cliente_id_emisor,cliente_fechacreate
                                    ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                        $query  = $this->pdo->prepare($sql);                
                        $result = $query->execute(  array(
                                                        $taxpayer[0], $taxpayer[1], $taxpayer[2], $taxpayer[3], $taxpayer[4], $taxpayer[5], $taxpayer[6], $taxpayer[7], $taxpayer[8], $taxpayer[9],
                                                        $additional[0], $additional[1], $additional[2],
                                                        $address[0], $address[1], $address[2], $address[3], $address[4],
                                                        "es", $active, $id, $this->fechahora
                                                    ));
                        // echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando
                        // echo "<pre>"; var_dump($query->errorInfo());exit();//para ver el errores pdo
                        // echo "<pre>";  var_dump($result);exit();
                        if ($result == true) {
                            return  array('answer' => 'success_save');
                        }                
                    }else if ($const[0] == $taxpayer[4]) {
                        return array('answer' => 'exists');
                    }else{
                        return array('answer' => 'error');
                    }
                }else if ($data[0] == "1"){ //Si es para editar un producto
                    $sql   = "SELECT cliente_numero_identificacion FROM tbl_clientes WHERE  cliente_id_emisor = ? AND cliente_estado_activo <> 0";
                    $query = $this->pdo->prepare($sql);
                    $const = $query->execute(array($id));
                    $const = $query->fetchAll();
                    //    echo "<pre>";  var_dump($const);exit();
                    if(in_array($taxpayer[4], array_column($const, 'cliente_numero_identificacion'))){ //Si existe algun o algunas identidades
                        $sql   = "SELECT cliente_id, cliente_numero_identificacion FROM tbl_clientes WHERE  cliente_numero_identificacion = ? AND cliente_id_emisor = ? AND cliente_estado_activo <> 0";
                        $query = $this->pdo->prepare($sql);
                        $const = $query->execute(array($taxpayer[4],$id));
                        $const = $query->fetchAll();
                        $rows = $query->rowCount();
                        // echo "<pre>";  var_dump($const);exit();
                        if ($rows > 1) {//Si existe mas de un codigo
                            return array('answer' => 'exists');
                        }else{
                            // echo "<pre>";  var_dump($const);exit();
                            foreach ($const as $key => $value) {
                                if ($value["cliente_id"] == $data[1]) {// Si encontro el id que pertenece para poder actualizar
                                    $sql    = "UPDATE tbl_clientes SET cliente_id_tipo_persona = ?, cliente_razon_social = ?, cliente_nombre_comercial = ?, cliente_tipoiden_id = ?, cliente_numero_identificacion = ?, cliente_digito_verificacion = ?, cliente_id_regimen_fiscal = ?, cliente_email = ?, cliente_telefono = ?, cliente_nombre_contacto = ?,
                                    cliente_actividad_economica = ?, cliente_id_respons_fiscal = ?, cliente_id_detalle_tributario = ?,
                                    cliente_id_pais = ?, cliente_id_departamento = ?, cliente_id_ciudad = ?, cliente_direccion = ?, cliente_codigo_postal = ?,
                                    cliente_id_lenguaje = ?, cliente_fechamodify = ? WHERE cliente_id = ?";
                                    $query  = $this->pdo->prepare($sql);                
                                    $result = $query->execute(array($taxpayer[0], $taxpayer[1], $taxpayer[2], $taxpayer[3], $taxpayer[4], $taxpayer[5], $taxpayer[6], $taxpayer[7], $taxpayer[8], $taxpayer[9],
                                                                    $additional[0], $additional[1], $additional[2],
                                                                    $address[0], $address[1], $address[2], $address[3], $address[4],
                                                                    "es", $this->fechahora,$data[1]));
                                    // echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando
                                    if ($result == true) {
                                        return  array('answer' => 'success_edit');
                                    }else{
                                        return  array('answer' => 'error');
                                    }             
        
                                }else{
                                    return array('answer' => 'exists');
                                }
                            }
                        }
                    }else{//Si no hay ningun identidicación
                        $sql    = "UPDATE tbl_clientes SET cliente_id_tipo_persona = ?, cliente_razon_social = ?, cliente_nombre_comercial = ?, cliente_tipoiden_id = ?, cliente_numero_identificacion = ?, cliente_digito_verificacion = ?, cliente_id_regimen_fiscal = ?, cliente_email = ?, cliente_telefono = ?, cliente_nombre_contacto = ?,
                        cliente_actividad_economica = ?, cliente_id_respons_fiscal = ?, cliente_id_detalle_tributario = ?,
                        cliente_id_pais = ?, cliente_id_departamento = ?, cliente_id_ciudad = ?, cliente_direccion = ?, cliente_codigo_postal = ?,
                        cliente_id_lenguaje = ?, cliente_fechamodify = ? WHERE cliente_id = ?";
                        $query  = $this->pdo->prepare($sql);                
                        $result = $query->execute(array($taxpayer[0], $taxpayer[1], $taxpayer[2], $taxpayer[3], $taxpayer[4], $taxpayer[5], $taxpayer[6], $taxpayer[7], $taxpayer[8], $taxpayer[9],
                                                        $additional[0], $additional[1], $additional[2],
                                                        $address[0], $address[1], $address[2], $address[3], $address[4],
                                                        "es", $this->fechahora,$data[1]));
                        // echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando
                        if ($result == true) {
                            return  array('answer' => 'success_edit');
                        }else{
                            return  array('answer' => 'error');
                        }              
                    }
                }
            }
           
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }


    // Listar para ciudades
    public function listCity($departament)
    {
        try {
            $sql   = "SELECT departamento_codigo FROM tbl_departamento WHERE departamento_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($departament));
            $const = $query->fetch(PDO::FETCH_BOTH);
            if ($const) {
                $sql   = "SELECT * FROM tbl_ciudad WHERE ciudad_id_departamento = ? ORDER BY ciudad_nombre ASC";
                $query = $this->pdo->prepare($sql);
                $const = $query->execute(array($const["departamento_codigo"]));
                $const = $query->fetchALL(PDO::FETCH_BOTH);
                if ($const) {
                    $city = $const;
                    session_start();
                    $id = $_SESSION["emisor_id"];
                    $sql   = "SELECT cliente_id_ciudad FROM tbl_clientes WHERE cliente_id_emisor = ?";
                    $query = $this->pdo->prepare($sql);
                    $const1 = $query->execute(array($id));
                    $const1 = $query->fetch(PDO::FETCH_BOTH);
                    if ($const1) {
                        return array($city,$const1["cliente_id_ciudad"]);
                    }else{
                        return array($city,"");
                    }
                }
                
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar clientes
    public function listClients($start, $length, $search, $orderColumn, $orderDir)
    {
        try {
            session_start();
            $id = $_SESSION["emisor_id"];
            $orderColumns = $orderColumn + 1;
            $searchs      = "";
            if ($search) {
                $searchs = " (  t.tipoper_nombre LIKE '%" . $search . "%'
                                OR i.tipoiden_nombre LIKE '%" . $search . "%'
                                OR c.cliente_numero_identificacion LIKE '%" . $search . "%'
                                OR c.producto_precio_unitario LIKE '%" . $search . "%'
                                OR c.cliente_razon_social LIKE '%" . $search . "%'
                                OR c.cliente_email LIKE '%" . $search . "%' ) AND";
            }
            $sql = "SELECT COUNT(1) FROM tbl_clientes 
            WHERE  cliente_estado_activo = 1 AND cliente_id_emisor = ?";
            $query = $this->pdo->prepare($sql);
            $count = $query->execute(array($id));
            $count = $query->fetch();
 
            // var_dump($query->debugDumpParams());exit();
 
            $sql = "SELECT 	c.cliente_id,
            t.tipoper_nombre, 
            i.tipoiden_nombre, 
            c.cliente_numero_identificacion,
            c.cliente_digito_verificacion,
            c.cliente_razon_social, 
            c.cliente_email
            FROM tbl_clientes AS c 
            INNER JOIN tbl_tipo_persona AS t ON t.tipoper_id = c.cliente_id_tipo_persona
            INNER JOIN tbl_tipo_identificacion AS i ON i.tipoiden_id = c.cliente_tipoiden_id
            WHERE $searchs  c.cliente_estado_activo = 1 AND c.cliente_id_emisor = ?
            ORDER BY  $orderColumns $orderDir
            LIMIT $start, $length";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            $rows  = $query->rowCount();
            // echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando
            // echo "<pre>"; var_dump($query->errorInfo());exit();//para ver el errores pdo
            // var_dump($const);exit();
 
            $datos = array($const, $rows, $count);
            return $datos;
            $this->disconnect;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    //Eliminar cliente(s) masivamente
    public function deleteMassivelyClients($data){
        try {
            $const = false;
            session_start();
            $id = $_SESSION["emisor_id"];
            //Genera n veces (?) de lo que venga en el array $data para ser utilizado  IN 
            $values = count($data);
            $criteria = sprintf("?%s", str_repeat(",?", ($values ? $values-1 : 0)));
            if ($const == false) {
                $sql   = "DELETE FROM tbl_clientes WHERE cliente_id_emisor = ? AND cliente_id IN ($criteria)";
                $query = $this->pdo->prepare($sql);
                array_unshift($data,$id);//Preparar los valores 
                $result = $query->execute($data);
                if ($result == true) {
                    return  array('answer' => true);
                }else{
                    return  array('answer' => false);
                }
            }else{
                $active = 0;
                $sql   = "UPDATE tbl_clientes SET cliente_estado_activo = ? WHERE cliente_id_emisor = ? AND cliente_id IN ($criteria)";
                $query = $this->pdo->prepare($sql);
                array_unshift($data,$active,$id);//Preparar los valores 
                $result = $query->execute($data);
                if ($result == true) {
                    return  array('answer' => true);
                }else{
                    return  array('answer' => false);
                }
                
                $this->disconnect;
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    //Consultar cliente con relacion
    public function infoDetailClient($data)
    {
        try {
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT 	c.cliente_id,
            t.tipoper_nombre, 
            c.cliente_razon_social, 
            c.cliente_nombre_comercial, 
            i.tipoiden_nombre, 
            c.cliente_numero_identificacion,
            c.cliente_digito_verificacion,
            r.regfiscal_nombre,
            c.cliente_email,
            c.cliente_telefono,
            c.cliente_nombre_contacto,
            tf.tributofiscal_codigo,
            tf.tributofiscal_nombre,
            rf.responsfiscal_nombre,
            a.actividad_codigo,
            a.actividad_nombre,
            p.pais_nombre,
            d.departamento_nombre,
            cc.ciudad_nombre,
            c.cliente_direccion,
            c.cliente_codigo_postal
            FROM tbl_clientes AS c 
            INNER JOIN tbl_tipo_persona AS t ON t.tipoper_id = c.cliente_id_tipo_persona
            INNER JOIN tbl_tipo_identificacion AS i ON i.tipoiden_id = c.cliente_tipoiden_id
            INNER JOIN tbl_regimen_fiscal AS r ON r.regfiscal_id = c.cliente_id_regimen_fiscal
            INNER JOIN tbl_tributo_fiscal AS tf ON tf.tributofiscal_id = c.cliente_id_detalle_tributario
			INNER JOIN tbl_responsabilidad_fiscal AS rf ON rf.responsfiscal_id = c.cliente_id_respons_fiscal
            INNER JOIN tbl_actividad_economica AS a ON a.actividad_id = c.cliente_actividad_economica
            INNER JOIN tbl_pais AS p ON p.pais_id = c.cliente_id_pais
            INNER JOIN tbl_departamento AS d ON d.departamento_id = c.cliente_id_departamento
            INNER JOIN tbl_ciudad AS cc ON cc.ciudad_id = c.cliente_id_ciudad
            WHERE  c.cliente_estado_activo = 1 AND c.cliente_id_emisor = ? AND cliente_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id,$data));
            $const = $query->fetch();
            return array($const);
            $this->disconnect;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

}
