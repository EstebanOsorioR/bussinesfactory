<?php
require_once 'model/conn.model.php';

class HomeModel
{
    private $pdo;
    private $fechahora;
    public function __CONSTRUCT()
    {
        try {
            $this->pdo = DataBase::connect();
            date_default_timezone_set('America/Bogota');
            $this->fechahora = date("Y-m-d H:i:s");
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    //Funcionalidad de inicio de sección  un emisor 
    public function loginIssuing($data)
    {
        try {
            //Buscar el emisor por correo
            $sql   = "SELECT emisor_id, emisor_email, emisor_pass, emisor_razon_social, emisor_numero_identificacion, emisor_digito_verificacion, emisor_estado_activo FROM tbl_emisores WHERE emisor_email =  ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array(strtolower($data[0])));
            $const = $query->fetch();
            // var_dump($const);exit();

            if ($const == true) {//Si encontro el emisor 
                // Condicional para validar contrasena si es igual
                if (password_verify($data[1], $const[2])) {
                    if ($const[6] == 1 ||$const[6] == 3||$const[6] == 4) {//Si el emisor esta activo o cambiar contrasañse o llenar datos fiscales
                        session_start(); // Se inician variables para iniciar session
                        $_SESSION["validar"]   = true;
                        $_SESSION["emisor_id"] = $const[0];
                        $_SESSION["emisor_razon_social"] = $const[3];
                        $_SESSION["emisor_numero_identificacion"]    = $const[4];
                        $_SESSION["emisor_digito_verificacion"]  = $const[5];
                        $_SESSION["emisor_estado_activo"]  = $const[6];
                        return array('answer' => 'success_auth');
                        // if ($const[6] == 1) {       //Si el emisor esta activo
                        //     return array('answer' => 'success_auth');
                        // }
                        // else if ($const[6] == 3) { //Cambio de contraseña
                        //     return array('answer' => 'success_pass');
                        // }else if($const[6] == 4){   //Llenar datos fiscales
                        //     return array('answer' => 'success_tax');
                        // }
                    } else if($const[6] == 2){//El usuario no a confirmado la contraseña
                        return array('answer' => 'confirm' );
                    }else {//Si el emisor no esta activo
                        return array('answer' => 'error_active' );
                    }
                } else {//Si la contraseña no coincide
                    return array('answer' => 'error_auth');
                }
            } else if ($const == false){//Si no encontro el emisor 
                return array('answer' => 'error_auth');
            } else{//Por si hay un error externo
                return array('answer' => 'error');
            }
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

    //Verifica el estado
    public function seeStatus()
    {
        try {
            session_start();
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT emisor_estado_activo FROM tbl_emisores WHERE emisor_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetch();
            return $const["emisor_estado_activo"];
        } 
        catch (Exception $e) {
            die($e->getMessage());
        }
    }
        //Funcionalidad de inicio de sección  un emisor 
        public function activeIssuing($data)
        {
            try {
                
                $sql   = "SELECT emisor_id, emisor_email, emisor_estado_activo FROM tbl_emisores WHERE emisor_email = ?";
                $query = $this->pdo->prepare($sql);
                $const = $query->execute(array($data));
                $const = $query->fetch();
                // echo "<pre>";var_dump($const);exit();
                if ($const == false) {//Si el correo desencriptado no existe o no existe ningun correo
                    return false;
                }else if(is_array($const)){// Si el correo existe
                    if ($const['emisor_estado_activo'] == 1) {//El correo ya fue confirmado anteriormente
                        return 'confirmed';
                        // echo "<pre>";var_dump($const[2]);exit();
                    }else if($const['emisor_estado_activo'] == 2){//El emisor no ha confirmado cuando por lo que aquí se le activara la cuenta 
                        // echo "<pre>";var_dump($const[2]);exit();
                        $sql    = "UPDATE tbl_emisores SET emisor_estado_activo = ?, emisor_fechamodify = ? WHERE emisor_id = ?";
                        $query  = $this->pdo->prepare($sql);
                        $result = $query->execute(array(3, $this->fechahora, $const['emisor_id']));
                        if ($result == true) {
                            return 'success_edit';
                        }else{  //Por si hay un error externo
                            return 'error';
                        }  
                    }else if($const['emisor_estado_activo'] == 3){ //Si el emisor ya fue confirmado
                        return 'previously';
                    }else{ //Por si hay un error externo

                    }    
                }else{ //Por si hay un error externo
                    return 'error';
                }    
            }
            catch (Exception $e) {
                die($e->getMessage());
            }
        }

    public function registerIssuing($data)
    {
        try {
            //Buscar el emisor por correo
            $sql   = "SELECT emisor_email, emisor_estado_activo FROM tbl_emisores WHERE emisor_email = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array(strtolower($data[1])));
            $const = $query->fetch();
            // var_dump($const);exit();
            if ($const == false) {// Si el emisor no existe
                //Generar un contraseña aleatoria.
                $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890@#!%&";//Se define una cadena de caractares.
                $longitudCadena=strlen($cadena);//Obtenemos la longitud de la cadena de caracteres
                $pass = "";//Definimos la variable que va a contener la contraseña
                $longitudPass=8;//Se define la longitud de la contraseña, puedes poner la longitud que necesites
                
                for($i=1 ; $i<=$longitudPass ; $i++){ //Creamos la contraseña recorriendo la cadena tantas veces como hayamos indicado
                    $pos=rand(0,$longitudCadena-1);//Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
                    $pass .= substr($cadena,$pos,1); //Vamos formando la contraseña con cada carácter aleatorio.
                }
                $pass_encrypted = password_hash($pass, PASSWORD_DEFAULT);//Encriptamos la contraseña 
                // var_dump($token);exit();
                $sql    = "INSERT INTO tbl_emisores (emisor_email, emisor_razon_social, emisor_pass, emisor_estado_activo, emisor_fechacreate) VALUES (?,?,?,?,?)";
                $query  = $this->pdo->prepare($sql);                
                $result = $query->execute(array(strtolower($data[1]), $data[0], $pass_encrypted, 2,$this->fechahora));
                if ($result = true) {
                   return array($pass);
                }else{//Por si hay un error externo
                    return 'error';
                }
                // var_dump($pass);exit();
            }else if($const[1] == 2){
                return 'previous_register';//Se a detectado un registro previo
            }else if($const[1] == 0 || $const[1] == 1 || $const[1] == 3 || $const[1] == 4 || $const[1] == 5 ){
                return 'exists';//El correo electrónico ya ha sido registrado
            }else{//Por si hay un error externo
                return 'error';
            }
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

    //Buscar un emisor para enviar  correo de recuperacion de contraseña 
    public function restoreEmail($data)
    {
        try {
            $sql   = "SELECT emisor_id, emisor_email FROM tbl_emisores WHERE emisor_email = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array(strtolower($data[0])));
            $const = $query->fetch();
            if ($const) {// Si entro a un emisor 
                $token = bin2hex(random_bytes(24));
                $sql    = "UPDATE tbl_emisores SET emisor_token = ?, emisor_fechamodify = ? WHERE emisor_id = ?";
                $query  = $this->pdo->prepare($sql);
                $result = $query->execute(array($token, $this->fechahora, $const['emisor_id']));
                if ($result = true) {
                    return array($token, $const["emisor_email"]);
                 }else{//Por si hay un error externo
                     return 'error';
                 }
            }else{// Si no entro a ningin emisor
                return "no_exists";
            }
        } 
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function restorePassword($email, $token)
    {
        try {
          
            $sql   = "SELECT emisor_token FROM tbl_emisores WHERE emisor_email = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array(strtolower($email)));
            $const = $query->fetch();
            // var_dump($const);exit();
            if ($const) {// Si hay un emisor con ese correo
               if($const[0]== $token){//Si el token get es igual al token db
                return true;
               }else{
                return false;
               }
            }else{// Si no entro a ningin emisor
                return false;
            }
        } 
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

    //Actulizar la nueva contaseña
    public function restorePass($data)
    {
        try {
          
            $sql   = "SELECT emisor_id, emisor_email FROM tbl_emisores WHERE emisor_email = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array(strtolower($data[0])));
            $const = $query->fetch();
            // var_dump($const);exit();
            if ($const == true && $const["emisor_email"] == $data[0]) {//Si existe el correo
                $pass_encrypted = password_hash($data[1], PASSWORD_DEFAULT);
                $sql    = "UPDATE tbl_emisores SET emisor_pass = ?, emisor_fechamodify = ? WHERE emisor_id = ?";
                $query  = $this->pdo->prepare($sql);
                $result = $query->execute(array($pass_encrypted, $this->fechahora, $const['emisor_id']));
                if ($result = true) {//Si actulizo la nueva contraseña
                    return array('answer' => 'success');
                }else{//Por si hay un error externo
                    return array('answer' => 'error');
                }
            }else{//El correo no existe
                return array('answer' => 'error');
            }
        } 
        catch (Exception $e) {
            die($e->getMessage());
        }
    }   


    public function seeIssuing()
    {
        try {
            // session_start();
            $id = $_SESSION["emisor_id"];
            
            $sql   = "SELECT emisor_razon_social, emisor_numero_identificacion, emisor_digito_verificacion FROM tbl_emisores WHERE emisor_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetch();
            $data = array($const["emisor_razon_social"],$const["emisor_numero_identificacion"],$const["emisor_digito_verificacion"]);
            return $data;
           
        } 
        catch (Exception $e) {
            die($e->getMessage());
        }
    }   
    
    
    
}