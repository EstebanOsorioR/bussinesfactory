<?php
require_once 'model/conn.model.php';
require_once 'controller/home.controller.php';

class MyAccountModel
{
    private $pdo;
    private $fechahora;
    public function __CONSTRUCT()
    {
        try {
            $this->pdo = DataBase::connect();
            date_default_timezone_set('America/Bogota');
            $this->fechahora = date("Y-m-d H:i:s");
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

    
    //Verificar contaseña
    public function verifyPassword($data)
    {
        try {
            // session_start();
            // $id = $_SESSION["emisor_id"];
            $id = "69";
            //  var_dump($id);exit();
            $sql   = "SELECT emisor_pass FROM tbl_emisores WHERE emisor_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetch();
            if (!empty($const)) {//si retorna datos
                if (password_verify($data, $const["emisor_pass"])) {//Verifica la contraseña
                    return "agree";
                }else{// Si no coinciden la contraseña
                    return "no_agree";
                }
            }else{//por si hay un error externo
                return "error";
            }
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

    //Guardar contaseña Nueva
    public function saveNewPass($data)
    {
        try {
            // session_start();
            $id = $_SESSION["emisor_id"];
            $estado = $_SESSION["emisor_estado_activo"];
            //  var_dump($id);exit();
            $pass_encrypted = password_hash($data, PASSWORD_DEFAULT);
            $sql    = "UPDATE tbl_emisores SET emisor_pass = ?, emisor_estado_activo = ?, emisor_fechamodify = ? WHERE emisor_id = ?";
            $query  = $this->pdo->prepare($sql);
            $result = $query->execute(array($pass_encrypted, 4, $this->fechahora, $id));
            if ($result == true) {
               
                if($estado == 3){
                    return 'success_edit';
                }else{
                    return 'success';
                }
                
            }else{  //Por si hay un error externo
                return 'error';
            }  
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

    //verifica el estado
    public function seeStatusAlert()
    {
        try {
            session_start();
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT emisor_estado_activo FROM tbl_emisores WHERE emisor_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetch();
            return $const["emisor_estado_activo"];
        } 
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

    //Consultar Cuenta
    public function seeAccount()
    {
        try {
        //   session_start();
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT * FROM tbl_emisores WHERE emisor_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetch();
            return $const;
        } 
        catch (Exception $e) {
            die($e->getMessage());
        }
    }


    // Listar para Tipo de Ambiente 
    public function listTypeEnvironment()
    {
        try {
            $sql   = "SELECT * FROM tbl_ambiente ORDER BY ambiente_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }
    

    // Listar para Tipo de Docunento 
    public function listTypeDocument()
    {
        try {
            $sql   = "SELECT * FROM tbl_tipo_documento ORDER BY tipo_documento_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }
        
    

      // Listar para Tipo de Operación
      public function listTypeOperation()
      {
          try {
              $sql   = "SELECT * FROM tbl_tipo_operacion ORDER BY tipo_operacion_nombre ASC";
              $query = $this->pdo->prepare($sql);
              $const = $query->execute();
              $const = $query->fetchALL(PDO::FETCH_BOTH);
              return $const;
          } catch (\Exception $e) {
              die($e->getMessage());
          }
      }


           // Listar para  Modalidad de secuancial
           public function listModality()
           {
               try {
                   $sql   = "SELECT * FROM tbl_modalidad_secuencial ORDER BY modalidad_nombre ASC";
                   $query = $this->pdo->prepare($sql);
                   $const = $query->execute();
                   $const = $query->fetchALL(PDO::FETCH_BOTH);
                   return $const;
               } catch (\Exception $e) {
                   die($e->getMessage());
               }
           }

    // Listar Régimen de contribuyente
    public function listTaxpayerRegime()
    {
        try {
            $sql   = "SELECT * FROM tbl_tipo_persona ORDER BY tipoper_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar Tipo de régimen
    public function listRegimeType()
    {
        try {
            $sql   = "SELECT * FROM tbl_regimen_fiscal ORDER BY regfiscal_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar Tipo de identificación
    public function listIdentificationType()
    {
        try {
            $sql   = "SELECT * FROM tbl_tipo_identificacion ORDER BY tipoiden_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar Actividades Econimicas
    public function listEconomicActivity()
    {
        try {
            $sql   = "SELECT * FROM tbl_actividad_economica ORDER BY actividad_codigo ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar Resposabilidades
    public function listResponsibility()
    {
        try {
            $sql   = "SELECT * FROM tbl_responsabilidad_fiscal ORDER BY responsfiscal_codigo ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar para Impustos DIAN
    public function listTaxesDIAN()
    {
        try {
            $sql   = "SELECT * FROM tbl_tributo_fiscal ORDER BY tributofiscal_codigo ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar para departamentos
    public function listCountry()
    {
        try {
            $sql   = "SELECT * FROM tbl_pais ORDER BY pais_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar para departamentos
    public function listDepartment()
    {
        try {
            $sql   = "SELECT * FROM tbl_departamento ORDER BY departamento_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar para ciudades
    public function listCity($departament)
    {
        try {
            $sql   = "SELECT departamento_codigo FROM tbl_departamento WHERE departamento_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($departament));
            $const = $query->fetch(PDO::FETCH_BOTH);
            if ($const) {
                $sql   = "SELECT * FROM tbl_ciudad WHERE ciudad_id_departamento = ? ORDER BY ciudad_nombre ASC";
                $query = $this->pdo->prepare($sql);
                $const = $query->execute(array($const["departamento_codigo"]));
                $const = $query->fetchALL(PDO::FETCH_BOTH);
                if ($const) {
                    $city = $const;
                    session_start();
                    $id = $_SESSION["emisor_id"];
                    $sql   = "SELECT emisor_id_ciudad FROM tbl_emisores WHERE emisor_id = ?";
                    $query = $this->pdo->prepare($sql);
                    $const1 = $query->execute(array($id));
                    $const1 = $query->fetch(PDO::FETCH_BOTH);
                    if ($const1) {
                        return array($city,$const1["emisor_id_ciudad"]);
                    }else{
                        return array($city,"");
                    }
                }
                
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    //Consultar productos con relacion
    public function infoAccount()
    {
        try {
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT 	e.emisor_id,
            t.tipoper_nombre, 
            r.regfiscal_nombre,
            i.tipoiden_nombre, 
            e.emisor_numero_identificacion,
            e.emisor_digito_verificacion,
            e.emisor_razon_social, 
            e.emisor_nombre_comercial,
            e.emisor_token_empresa,
            e.emisor_token_password,         
            tf.tributofiscal_codigo,
            tf.tributofiscal_nombre,
            rf.responsfiscal_codigo,
            rf.responsfiscal_nombre,
            a.actividad_codigo,
            a.actividad_nombre,
            p.pais_nombre,
            d.departamento_nombre,
            c.ciudad_nombre,
            e.emisor_direccion,
            e.emisor_codigo_postal,
            e.emisor_telefono,
            e.emisor_email_recepcion,
            e.emisor_nota
            FROM tbl_emisores AS e 
            INNER JOIN tbl_tipo_persona AS t ON t.tipoper_id = e.emisor_id_tipo_persona
            INNER JOIN tbl_tipo_identificacion AS i ON i.tipoiden_id = e.emisor_tipoiden_id
            INNER JOIN tbl_regimen_fiscal AS r ON r.regfiscal_id = e.emisor_id_regimen_fiscal
            INNER JOIN tbl_tributo_fiscal AS tf ON tf.tributofiscal_id = e.emisor_id_detalle_tributario
			INNER JOIN tbl_responsabilidad_fiscal AS rf ON rf.responsfiscal_id = e.emisor_id_respons_fiscal
            INNER JOIN tbl_actividad_economica AS a ON a.actividad_id = e.emisor_actividad_economica
            INNER JOIN tbl_pais AS p ON p.pais_id = e.emisor_id_pais
            INNER JOIN tbl_departamento AS d ON d.departamento_id = e.emisor_id_departamento
            INNER JOIN tbl_ciudad AS c ON c.ciudad_id = e.emisor_id_ciudad
            WHERE e.emisor_id =  ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetch();
            return array($const);
            $this->disconnect;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    

    // Guardar cuenta
    public function editAccount($taxpayer,$address,$additional,$contact)
    {
        try {
            if (!empty($taxpayer)&&!empty($address)&&!empty($additional)&&!empty($contact)) {
            session_start();
            $id = $_SESSION["emisor_id"];
            // var_dump($address);exit();
            $sql    = " UPDATE tbl_emisores SET emisor_id_tipo_persona = ?, emisor_id_regimen_fiscal = ?, emisor_tipoiden_id = ?, emisor_numero_identificacion = ? ,  emisor_digito_verificacion = ?, emisor_razon_social = ?, emisor_nombre_comercial = ?, 
                        emisor_id_pais = ?, emisor_id_departamento = ?, emisor_id_ciudad = ?, emisor_direccion = ? , emisor_codigo_postal = ?,
                        emisor_actividad_economica = ?, emisor_id_respons_fiscal = ?, emisor_id_detalle_tributario = ?,
                        emisor_telefono = ?, emisor_email_recepcion = ?, emisor_nota = ? , emisor_fechamodify = ?, emisor_estado_activo = ? 
                        WHERE emisor_id = ?";
            $query  = $this->pdo->prepare($sql);
            $result = $query->execute   (array($taxpayer[0], $taxpayer[1], $taxpayer[2] , $taxpayer[3], $taxpayer[4], $taxpayer[5], $taxpayer[6],
                                                $address[0], $address[1], $address[2], $address[3], $address[4],
                                                $additional[0], $additional[1], $additional[2],
                                                $contact[0], $contact[1], $contact[2], $this->fechahora,1,
                                                $id));
            // echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando
            // echo "<pre>"; var_dump($query->errorInfo());exit();//para ver el errores pdo
            //  echo "<pre>"; var_dump($result);exit();
            if ($result == true) {//Si actualizo a cuenta
                return 'success_edit';
            }else{// Por si no actualiza a cuentas
                return 'error';
            }
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }
    
    //Consultar secuenciales
    public function infoSequential($data)
    {
        try {
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT  * FROM tbl_secuenciales WHERE secuencial_id = ? AND secuencial_id_emisor = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($data, $id));
            $const = $query->fetch();
            // echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando
            // echo "<pre>";  var_dump($result);exit();
            // var_dump($const);exit();
            return array($const);
            $this->disconnect;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    
   // Guardar o editar secuenciales
   public function saveEditSequentials($data){
    try {
        session_start();
        $id = $_SESSION["emisor_id"];
        // Fortmatear las fechas
        $fechainicio = new DateTime($data[11]);
        $fechainicio =date_format($fechainicio, 'Y-m-d H:i:s');
        $fechafin = new DateTime($data[12]);
        $fechafin = date_format($fechafin, 'Y-m-d H:i:s');
        // var_dump($data);exit();
        if ($data[0] == "0") {// Si es para guardar un producto
            $sql   = "SELECT secuencial_numero_resolucion FROM tbl_secuenciales WHERE secuencial_numero_resolucion = ? AND secuencial_id_emisor = ? AND secuencial_activo <> 0";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($data[2],$id));
            $const = $query->fetch();
            if ($const == false) { // Si el producto no existe y esta inactivo
                $sql    = "INSERT INTO tbl_secuenciales (secuencial_id_ambiente, secuencial_id_tip_documento, secuencial_id_tip_operacion, secuencial_id_modalidad, secuencial_id_establecimiento, secuencial_numero_resolucion, secuencial_prefijo, secuencial_rango_desde, secuencial_rango_hasta,secuencial_rango_inicial, secuencial_consecutivo, secuencial_fecha_inicio, secuencial_fecha_fin, secuencial_clavedian, secuencial_activo, secuencial_fechacreate, secuencial_id_emisor) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $query  = $this->pdo->prepare($sql);                
                $result = $query->execute(array($data[1], $data[2], $data[3], $data[4], $data[5], $data[6], $data[7], $data[8], $data[9], $data[10], $data[10], $fechainicio, $fechafin, $data[13], 1, $this->fechahora,$id));
                // echo "<pre>";  var_dump($result);exit();
                if ($result == true) {
                    return  array('answer' => 'success_save');
                }                
            }else if ($const[0] == $data[2]) {
                return array('answer' => 'exists');
            }else{
                return array('answer' => 'error');
            }
        }else if ($data[0] == "1"){ //Si es para editar un producto
            $sql   = "SELECT secuencial_numero_resolucion FROM tbl_secuenciales WHERE  secuencial_id_emisor = ? AND secuencial_activo <> 0";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetchAll();
            // echo "<pre>";  var_dump($data[6]);exit();
            if(in_array($data[6], array_column($const, 'secuencial_numero_resolucion'))){ //Si existe algun o algunos codigos
                // echo "<pre>";  var_dump("hola");exit();
                $sql   = "SELECT secuencial_id, secuencial_numero_resolucion FROM tbl_secuenciales WHERE  secuencial_numero_resolucion = ? AND secuencial_id_emisor = ? AND secuencial_activo <> 0";
                $query = $this->pdo->prepare($sql);
                $const = $query->execute(array($data[6],$id));
                $const = $query->fetchAll();
                $rows = $query->rowCount();

                if ($rows > 1) {//Si existe mas de un codigo
                    return array('answer' => 'exists');
                }else{
                    // echo "<pre>";  var_dump($data[8]);exit();
                    foreach ($const as $key => $value) {
                        if ($value["secuencial_id"] == $data[14]) {// Si encontro el id que pertenece para poder actualizar
                            $sql    = "UPDATE tbl_secuenciales SET secuencial_id_ambiente = ?, secuencial_id_tip_documento = ?, secuencial_id_tip_operacion = ?, secuencial_id_modalidad = ?, secuencial_id_establecimiento = ?, secuencial_numero_resolucion = ?, secuencial_prefijo = ?, secuencial_rango_desde = ?, secuencial_rango_hasta = ?,secuencial_rango_inicial = ?, secuencial_consecutivo = ?, secuencial_fecha_inicio = ?, secuencial_fecha_fin = ?, secuencial_clavedian  = ?, secuencial_fechamodify  = ? WHERE secuencial_id = ?";
                            $query  = $this->pdo->prepare($sql);                
                            $result = $query->execute(array($data[1], $data[2], $data[3], $data[4], $data[5], $data[6], $data[7], $data[8], $data[9], $data[10], $data[10], $fechainicio, $fechafin, $data[13], $this->fechahora, $data[14]));
                            if ($result == true) {
                                return  array('answer' => 'success_edit');
                            }else{
                                return  array('answer' => 'error');
                            }             

                        }else{
                            return array('answer' => 'exists');
                        }
                    }
                }
            }else{//Si no hay ningun un numero de secuancial
                    //    echo "<pre>";  var_dump($data[6]);exit();
                $sql    = "UPDATE tbl_secuenciales SET secuencial_id_ambiente = ?, secuencial_id_tip_documento = ?, secuencial_id_tip_operacion = ?, secuencial_id_modalidad = ?, secuencial_id_establecimiento = ?, secuencial_numero_resolucion = ?, secuencial_prefijo = ?, secuencial_rango_desde = ?, secuencial_rango_hasta = ?,secuencial_rango_inicial = ?, secuencial_consecutivo = ?, secuencial_fecha_inicio = ?, secuencial_fecha_fin = ?, secuencial_clavedian  = ?, secuencial_fechamodify  = ? WHERE secuencial_id = ?";
                $query  = $this->pdo->prepare($sql);                
                $result = $query->execute(array($data[1], $data[2], $data[3], $data[4], $data[5], $data[6], $data[7], $data[8], $data[9], $data[10], $data[10], $fechainicio, $fechafin, $data[13], $this->fechahora, $data[14]));
                echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando
                if ($result == true) {
                    return  array('answer' => 'success_edit');
                }else{
                    return  array('answer' => 'error');
                }             
            }
        }
    } catch (\Exception $e) {
        die($e->getMessage());
    }
   }

     // Listar productos
     public function listSequential($start, $length, $search, $orderColumn, $orderDir)
     {
         try {
             session_start();
             $id = $_SESSION["emisor_id"];
             $orderColumns = $orderColumn + 1;
             $searchs      = "";
             if ($search) {
                 $searchs = " (  tipo_documento_nombre LIKE '%" . $search . "%'
                                 OR establecimiento_nombre LIKE '%" . $search . "%'
                                 OR secuencial_prefijo LIKE '%" . $search . "%'
                                 OR secuencial_fecha_inicio LIKE '%" . $search . "%'
                                 OR secuencial_fecha_fin LIKE '%" . $search . "%'
                                 OR ambiente_nombre LIKE '%" . $search . "%' ) AND";
             }
             $sql = "SELECT COUNT(1) FROM tbl_secuenciales 
             WHERE  secuencial_id_emisor = $id";
             $query = $this->pdo->prepare($sql);
             $count = $query->execute();
             $count = $query->fetch();
  
            //  var_dump($count);exit();
  
             $sql = "SELECT s.secuencial_id,
             t.tipo_documento_nombre, 
             e.establecimiento_nombre,
             s.secuencial_prefijo,
             DATE_FORMAT(s.secuencial_fecha_inicio,'%d-%m-%Y') AS secuencial_fecha_inicio,
             DATE_FORMAT(s.secuencial_fecha_fin,'%d-%m-%Y') AS secuencial_fecha_fin,
             s.secuencial_rango_inicial,
             s.secuencial_consecutivo,
             s.secuencial_activo,
             a.ambiente_nombre
             FROM tbl_secuenciales AS s 
             INNER JOIN tbl_tipo_documento AS t ON t.tipo_documento_id = s.secuencial_id_tip_documento
             INNER JOIN tbl_establecimientos AS e ON e.establecimiento_id = s.secuencial_id_establecimiento
             INNER JOIN tbl_ambiente AS a ON a.ambiente_id = s.secuencial_id_ambiente	
             WHERE $searchs s.secuencial_id_emisor = $id
             ORDER BY  $orderColumns $orderDir
             LIMIT $start, $length";
             $query = $this->pdo->prepare($sql);
             $const = $query->execute();
             $const = $query->fetchALL(PDO::FETCH_BOTH);
            //  echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando
             $rows  = $query->rowCount();
  
            //  var_dump($const);exit();
  
             $datos = array($const, $rows, $count);
             return $datos;
             $this->disconnect;
         } catch (\Exception $e) {
             die($e->getMessage());
         }
     }

     //Consultar secuencial con relacion
    public function infoDetailSequential($data)
    {
        try {
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT s.secuencial_id,
            t.tipo_documento_nombre, 
            s.secuencial_numero_resolucion,
            s.secuencial_prefijo,
            s.secuencial_clavedian,
            s.secuencial_rango_desde,
            s.secuencial_rango_hasta,
            s.secuencial_rango_inicial,
            DATE_FORMAT(s.secuencial_fecha_inicio,'%d-%m-%Y') AS secuencial_fecha_inicio,
            DATE_FORMAT(s.secuencial_fecha_fin,'%d-%m-%Y') AS secuencial_fecha_fin,
            m.modalidad_nombre,
            e.establecimiento_nombre,      
            a.ambiente_nombre
            FROM tbl_secuenciales AS s 
            INNER JOIN tbl_tipo_documento AS t ON t.tipo_documento_id = s.secuencial_id_tip_documento
            INNER JOIN tbl_establecimientos AS e ON e.establecimiento_id = s.secuencial_id_establecimiento
            INNER JOIN tbl_ambiente AS a ON a.ambiente_id = s.secuencial_id_ambiente	
            INNER JOIN tbl_modalidad_secuencial AS m ON m.modalidad_id = s.secuencial_id_modalidad
            WHERE s.secuencial_id_emisor = ? AND s.secuencial_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id,$data));
            $const = $query->fetch();
            return array($const);
            $this->disconnect;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    //Inacticar secuencuial 
    public function inactivateSequentials($data){
        try {
            $const = false;
            session_start();
            $id = $_SESSION["emisor_id"];
            //Genera n veces (?) de lo que venga en el array $data para ser utilizado  IN 
            $values = count($data);
            $criteria = sprintf("?%s", str_repeat(",?", ($values ? $values-1 : 0)));
            $active = 0;
            $sql   = "UPDATE tbl_secuenciales SET secuencial_activo = ? WHERE secuencial_id_emisor = ? AND secuencial_id IN ($criteria)";
            $query = $this->pdo->prepare($sql);
            array_unshift($data,$active,$id);//Preparar los valores 
            $result = $query->execute($data);
            if ($result == true) {
                return  array('answer' => true);
            }else{
                return  array('answer' => false);
            }
            $this->disconnect;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    
}