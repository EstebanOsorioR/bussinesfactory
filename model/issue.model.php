<?php
require_once 'model/conn.model.php';

class IssueModel
{
    private $pdo;
    private $fechahora;
    public function __CONSTRUCT()
    {
        try {
            $this->pdo = DataBase::connect();
            date_default_timezone_set('America/Bogota');
            $this->fechahora = date("Y-m-d H:i:s");
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar tipos de documentos
    public function listTypeDocument()
    {
        try {
            $sql   = "SELECT * FROM tbl_tipo_documento ORDER BY tipo_documento_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar tipos de documentos
    public function listSequential($documento)
    {
        try {
            session_start();
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT tipo_documento_id FROM tbl_tipo_documento WHERE tipo_documento_codigo = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($documento));
            $const = $query->fetch(PDO::FETCH_BOTH);
            // var_dump($const["tipo_documento_id"]);exit();
            if ( $const == true) {
                $sql   = "  SELECT *
                            FROM tbl_secuenciales 
                            WHERE secuencial_id_emisor = '68' AND  secuencial_id_tip_documento = '1' AND secuencial_activo <> 0 
                            ORDER BY secuencial_prefijo ASC";
                $query = $this->pdo->prepare($sql);
                $const = $query->execute(array($const["tipo_documento_id"],$id));
                $const = $query->fetchALL(PDO::FETCH_BOTH);
                // echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando
                // echo "<pre>"; var_dump($query->errorInfo());exit();//para ver el errores pdo
                return $const;
            }
                        
 
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }
    
    // Listar tipos de operacion
    public function listTypeOperation()
    {
        try {
            $sql   = "SELECT * FROM tbl_tipo_operacion_emision WHERE tipoope_emision_codigo = '10' ORDER BY tipoope_emision_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar metodo de pago
    public function listPaymentMethod()
    {
        try {
            $sql   = "SELECT * FROM tbl_metodo_pago  ORDER BY metodo_pago_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar medio de pago
    public function listPaymentMeans()
    {
        try {
            $sql   = "SELECT * FROM tbl_medio_pago ORDER BY medio_pago_codigo ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar tipos de operacion
    public function listClient()
    {
        try {
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT cliente_id, cliente_razon_social FROM tbl_clientes WHERE cliente_id_emisor = ? AND  cliente_estado_activo = 1 ORDER BY cliente_razon_social ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar tipos de operacion
    public function seeClients($client)
    {
        try {
            session_start();
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT cliente_numero_identificacion, cliente_digito_verificacion, cliente_email FROM tbl_clientes WHERE cliente_id_emisor = ? AND cliente_id = ? ";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id,$client));
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    
    // Listar los conceptos
    public function listConcept()
    {
        try {
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT  producto_id, producto_codigo, producto_descripcion FROM tbl_producto WHERE producto_id_emisor = ? ORDER BY producto_codigo ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    // Listar los tipos de descuentos
    public function listTypeDiscount()
    {
        try {
            $sql   = "SELECT * FROM tbl_tipo_descuento ORDER BY tipo_descuento_nombre ASC";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute();
            $const = $query->fetchALL(PDO::FETCH_BOTH);
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

     //Consultar productos con relacion
     public function infoConcept($data)
     {
         try {
            session_start();
             $id = $_SESSION["emisor_id"];
             $sql   = "SELECT  p.producto_id,
             p.producto_codigo, 
             p.producto_precio_unitario, 
             p.producto_descripcion,
             u.unimed_nombre, 
             p.producto_id_unidad_medida,
             p.producto_id_tipo_impuesto,
             p.producto_id_porc_impuesto,
             i.porimp_nombre_entero,
             t.tipproducto_nombre
             FROM tbl_producto AS p
             INNER JOIN tbl_unidades_medida_dian AS u ON u.unimed_codigo = p.producto_id_unidad_medida
             INNER JOIN tbl_porcentaje_impuesto AS i ON i.porimp_codigo = p.producto_id_porc_impuesto
             INNER JOIN tbl_tipo_producto AS t ON t.tipproducto_codigo = p.producto_id_tipo_producto
             WHERE producto_id = ? AND producto_id_emisor = ?";
             $query = $this->pdo->prepare($sql);
             $const = $query->execute(array($data,$id));
             $const = $query->fetch();
             return array($const);
             $this->disconnect;
         } catch (\Exception $e) {
             die($e->getMessage());
         }
     }


    // Guardar comprobante
    public function saveDocument($comprobante,$cliente,$items,$totales,$otros,$consecutivo)
    {
        try {
      
            $id = $_SESSION["emisor_id"];
        //  echo "<pre>"; var_dump($comprobante);exit();
            //guardo los totales en json
            $datos = json_encode($comprobante);
            //guardo los totales en json
            $total = json_encode($totales);
            //guardo los medios de pago en json
            $otro = json_encode($otros);
            $sql   = "INSERT INTO tbl_comprobante (comprobante_id_tipo_documento, comprobante_datos, comprobante_fecha_emision, comprobante_no_documento, comprobante_id_tipo_operacion, comprobante_id_cliente, comprobante_monto_total, comprobante_totales, comprobante_producto, comprobante_otros, comprobante_emisor_id)  VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            $query = $this->pdo->prepare($sql);
            $result = $query->execute(array($comprobante[0], $datos, $this->fechahora, $consecutivo, $comprobante[5], $cliente[0], $totales[3], $total, $items, $otro, $id));
            
            // echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando

            // echo "<pre>"; var_dump($result);exit();
            if ($result == true) {//si guardo el comprobante
                $sql   = "UPDATE tbl_secuenciales  SET secuencial_consecutivo = (secuencial_consecutivo +1) WHERE secuencial_id = ? AND secuencial_id_emisor = ?";
                $query = $this->pdo->prepare($sql);
                $result = $query->execute(array($comprobante[2],$id));
                if ($result == true ) {
                    return  array('answer' => 'save');
                }else{
                    return  array('answer' => 'error');
                }
            }else{
                return  array('answer' => 'error');
            }

            return $result;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

       // Listar secuenciales
       public function listSecuncial($secuencial)
       {
           try {
               // session_start();
               $id = $_SESSION["emisor_id"];
               $sql   = "SELECT secuencial_prefijo, secuencial_rango_inicial, secuencial_consecutivo FROM tbl_secuenciales WHERE secuencial_id = ? AND secuencial_id_emisor = ?";
               $query = $this->pdo->prepare($sql);
               $const = $query->execute(array($secuencial, $id));
               $const = $query->fetch();
            //    echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando
               return $const;
           } catch (\Exception $e) {
               die($e->getMessage());
           }
       }
   

    // Listar todo los datos del cliente
    public function listTokens()
    {
        try {
            // session_start();
            $id = $_SESSION["emisor_id"];
            $sql   = "SELECT emisor_token_empresa, emisor_token_password FROM tbl_emisores WHERE emisor_id = ?";
            $query = $this->pdo->prepare($sql);
            $const = $query->execute(array($id));
            $const = $query->fetch();
            return $const;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

     // Listar todo los datos del cliente
     public function listAllClients($cliente)
     {
         try {
            //  session_start();
             $id = $_SESSION["emisor_id"];
             $sql   = "SELECT  	t.tipoper_codigo, 
             t.tipoper_nombre, 
             c.cliente_razon_social, 
             c.cliente_nombre_comercial, 
             i.tipoiden_codigo, 
             i.tipoiden_nombre, 
             c.cliente_numero_identificacion,
             c.cliente_digito_verificacion,
             r.regfiscal_codigo,
             r.regfiscal_nombre,
             c.cliente_email,
             c.cliente_telefono,
             tf.tributofiscal_codigo,
             tf.tributofiscal_nombre,
             rf.responsfiscal_codigo,
             rf.responsfiscal_nombre,
             a.actividad_codigo,
             a.actividad_nombre,
             p.pais_codigo,
             p.pais_nombre,
             d.departamento_codigo,
             d.departamento_nombre,
             cc.ciudad_codigo,
             cc.ciudad_nombre,
             c.cliente_direccion,
             c.cliente_codigo_postal,
             c.cliente_id_lenguaje
             FROM tbl_clientes AS c 
             INNER JOIN tbl_tipo_persona AS t ON t.tipoper_id = c.cliente_id_tipo_persona
             INNER JOIN tbl_tipo_identificacion AS i ON i.tipoiden_id = c.cliente_tipoiden_id
             INNER JOIN tbl_regimen_fiscal AS r ON r.regfiscal_id = c.cliente_id_regimen_fiscal
             INNER JOIN tbl_tributo_fiscal AS tf ON tf.tributofiscal_id = c.cliente_id_detalle_tributario
             INNER JOIN tbl_responsabilidad_fiscal AS rf ON rf.responsfiscal_id = c.cliente_id_respons_fiscal
             INNER JOIN tbl_actividad_economica AS a ON a.actividad_id = c.cliente_actividad_economica
             INNER JOIN tbl_pais AS p ON p.pais_id = c.cliente_id_pais
             INNER JOIN tbl_departamento AS d ON d.departamento_id = c.cliente_id_departamento
             INNER JOIN tbl_ciudad AS cc ON cc.ciudad_id = c.cliente_id_ciudad
             WHERE c.cliente_id_emisor = ? AND cliente_id = ?";
             $query = $this->pdo->prepare($sql);
             $const = $query->execute(array($id,$cliente[0]));
             $const = $query->fetch();
             return $const;
         } catch (\Exception $e) {
             die($e->getMessage());
         }
     }


     
      // Listar documentos
      public function listDucuments($start, $length, $search, $orderColumn, $orderDir)
      {
          try {
              session_start();
              $id = $_SESSION["emisor_id"];
              $orderColumns = $orderColumn + 1;
              $searchs      = "";
              if ($search) {
                  $searchs = " ( tipo_documento_nombre LIKE '%" . $search . "%'
                                  OR comprobante_fecha_emision LIKE '%" . $search . "%'
                                  OR comprobante_no_documento LIKE '%" . $search . "%'
                                  OR cliente_razon_social LIKE '%" . $search . "%'
                                  OR cliente_numero_identificacion LIKE '%" . $search . "%'
                                  OR comprobante_monto_total LIKE '%" . $search . "%' ) AND";
              }
              $sql = "SELECT COUNT(1) FROM tbl_comprobante 
              WHERE comprobante_emisor_id = $id";
              $query = $this->pdo->prepare($sql);
              $count = $query->execute();
              $count = $query->fetch();
   
           //    var_dump($sql);exit();
   
              $sql = "SELECT t.tipo_documento_nombre,
              c.comprobante_fecha_emision,
              c.comprobante_no_documento,
              cc.cliente_razon_social,
              cc.cliente_numero_identificacion,
              cc.cliente_digito_verificacion,
              c.comprobante_monto_total
              FROM tbl_comprobante AS c
              INNER JOIN tbl_tipo_documento AS t ON  t.tipo_documento_codigo = c.comprobante_id_tipo_documento
              INNER JOIN tbl_clientes AS cc ON  cc.cliente_id = c.comprobante_id_cliente
              WHERE  $searchs  comprobante_emisor_id = $id 
              ORDER BY  $orderColumns $orderDir
              LIMIT $start, $length";
              $query = $this->pdo->prepare($sql);
              $const = $query->execute();
              $const = $query->fetchALL(PDO::FETCH_BOTH);
              $rows  = $query->rowCount();
            //   echo "<pre>"; var_dump($query->debugDumpParams());exit();//Para ver que se esta ejecutando
           //    var_dump($sql);exit();
   
              $datos = array($const, $rows, $count);
              return $datos;
              $this->disconnect;
          } catch (\Exception $e) {
              die($e->getMessage());
          }
      }

    
    
    
}