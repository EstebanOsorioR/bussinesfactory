$(document).ready(function() {

    const url = window.location;
    let baseurl = '',
        answerlogin = "#answerlogin",
        cssInvalidUser = {
            "text-align": "center",
            "color": "red",
            "bottom": "-10px",
            "position": "relative"
        };

    if (url.hostname == 'localhost') {
        baseurl = url.origin + "/" + url.pathname.split('/')[1] + "/";
    } else {
        baseurl = url.origin + "/";
    }

    function alertswal(msn, inf, type) {
        swal({
            title: msn,
            text: inf,
            type: type,
            confirmButtonColor: "#3c8dbc",
            showConfirmButton: true,
        });
    }

    function alertswalurl(msn, inf, type, url) {
        swal({
                title: msn,
                text: inf,
                type: type,
                confirmButtonColor: "#3c8dbc",
                showConfirmButton: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    location.href = url;
                }
            }
        );
    }

    $(document).on('submit', 'form[name="frmlogin[]"]', function(e) {
        e.preventDefault();

        $.post(baseurl + "home/loginIssuing", $(this).serialize(), function(data) {
            answer = data.answer;
            // console.log(answer)
            if (answer == "success_auth") {
                location.href = baseurl + "home/index";
            }
            // else if (answer == "success_pass") {
            //     location.href = baseurl + "myaccount/password";
            // } else if (answer == "success_tax") {
            //     location.href = baseurl + "myaccount/frmaccount";
            // } 
            else if (answer == "error_auth") {
                $(answerlogin).show().html("<p>Usuario o contraseña son inválidos.</p>").css(cssInvalidUser);
                setTimeout(function() {
                    $(answerlogin).fadeOut(1500);
                }, 6000);
            } else if (answer == "error_active") {
                $(answerlogin).show().html("<p>Usuario o contraseña son inválidos, favor comunicarse con el administrador del sistema.</p>").css(cssInvalidUser);
                setTimeout(function() {
                    $(answerlogin).fadeOut(1500);
                }, 6000);
            } else if (answer == "emptycaptcha") {
                alertswal("OOPS!!", "Debe validar el Captcha para continuar.", "info");
            } else if (answer == "error") {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            } else if (answer == "confirm") {
                alertswal("Cuenta no confirmada", "por favor valide su correo para continuar.", "info");
            } else {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            }
        }, "json").fail(function() {
            alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
        })

    })

    $(document).on('submit', 'form[name="frmregister[]"]', function(e) {
        // $.blockUI({
        //     message: '<h3>Espere un momento por favor...</h3>',
        //     css: {
        //         border: 'none',
        //         padding: '15px',
        //         backgroundColor: '#000',
        //         '-webkit-border-radius': '10px',
        //         '-moz-border-radius': '10px',
        //         opacity: .5,
        //         color: '#fff'
        //     }
        // });
        e.preventDefault();
        $.post(baseurl + "home/registerIssuing", $(this).serialize(), function(data) {
            answer = data.answer;
            // console.log(answer);
            if (answer == "send") {
                alertswalurl("Se ha enviado", "Un correo de confirmación a su cuenta", "success", baseurl + "home/login");
            } else if (answer == 'previous_register') {
                alertswal("OOPS!! Hemos detectado", "Un registro previo de este correo el cual aun no ha sido confirmado, por favor ingrese en el email indicado y confirme su solicitud.", "info");
            } else if (answer == "emptycaptcha") {
                alertswal("OOPS!!", "Debe validar el Captcha para continuar.", "info");
            } else if (answer == "exists") {
                alertswal("OOPS!!", "El correo electrónico ya ha sido registrado.", "info");
            } else if (answer == "error") {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            } else {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            }
        }, "json").fail(function() {
            alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
        })

    })

    $(document).on('submit', 'form[name="frmrestoreemail[]"]', function(e) {
        e.preventDefault();
        $.post(baseurl + "home/restoreEmailLogin", $(this).serialize(), function(data) {
            answer = data.answer;
            // console.log(answer);
            if (answer == "send") {
                alertswalurl("Te hemos enviado por correo ", "El enlace para restablecer tu contraseña", "success", baseurl + "home/login");
            } else if (answer == 'no_exists') {
                alertswal("OOPS!!", "No podemos encontrar ningún usuario con ese correo electrónico.", "info");
            } else if (answer == "emptycaptcha") {
                alertswal("OOPS!!", "Debe validar el Captcha para continuar.", "info");
            } else if (answer == "error") {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            } else {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            }
        }, "json").fail(function() {
            alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
        })

    })

});