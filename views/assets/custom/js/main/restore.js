$(document).ready(function() {

    const url = window.location;
    let baseurl = '',
        answerlogin = "#answerlogin",
        cssInvalidUser = {
            "text-align": "center",
            "color": "red",
            "bottom": "-10px",
            "position": "relative"
        };

    if (url.hostname == 'localhost') {
        baseurl = url.origin + "/" + url.pathname.split('/')[1] + "/";
    } else {
        baseurl = url.origin + "/";
    }

    function alertswal(msn, inf, type) {
        swal({
            title: msn,
            text: inf,
            type: type,
            confirmButtonColor: "#3c8dbc",
            showConfirmButton: true,
        });
    }

    function alertswalurl(msn, inf, type, url) {
        swal({
                title: msn,
                text: inf,
                type: type,
                confirmButtonColor: "#3c8dbc",
                showConfirmButton: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    location.href = url;
                }
            }
        );
    }

    $(document).on('submit', 'form[name="frmpass[]"]', function(e) {
        e.preventDefault();
        $.post(baseurl + "home/restorePass", $(this).serialize(), function(data) {
            answer = data.answer;
            // console.log(answer);
            if (answer == "success") {
                alertswalurl("Su contraseña ha sido restablecida", "Por favor ingrese nuevamente.", "success", baseurl + "home/login");
            } else if (answer == 'same') {
                alertswal("OOPS!!", "Las contraseñas no son iguales", "error");
            } else if (answer == "emptycaptcha") {
                alertswal("OOPS!!", "Debe validar el Captcha para continuar.", "info");
            } else if (answer == "error") {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            } else {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            }
        }, "json").fail(function() {
            alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
        })

    })

});