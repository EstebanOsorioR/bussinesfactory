$(document).ready(function() {

    const url = window.location;
    let baseurl = '',
        seleccionados = [];;
    if (url.hostname == 'localhost') {
        baseurl = url.origin + "/" + url.pathname.split('/')[1] + "/";
    } else {
        baseurl = url.origin + "/";
    }

    function alertswal(msn, inf, type) {
        swal({
            title: msn,
            text: inf,
            type: type,
            confirmButtonColor: "#3c8dbc",
            showConfirmButton: true,
        });
    }

    let table = $('#tableproducts').DataTable({
        "autoWidth": false,
        "paging": true,
        "info": true,
        "filter": true,
        'stateSave': true,
        "processing": true,
        "serverSide": true,
        "order": [
            [1, "asc"]
        ],
        "ajax": {
            "url": baseurl + "catalog/seeProducts",
            "type": "POST"
        },
        "columns": [{
            "data": "checkbox"
        }, {
            "data": "typeproduct"
        }, {
            "data": "code"
        }, {
            "data": "description",
        }, {
            "data": "unit",
        }, {
            "data": "price",
        }, {
            "data": "tax",
        }, {
            "data": "typetax",
        }, {
            "data": "actions",
        }],
        "columnDefs": [{
            "targets": -9,
            "orderable": false,
            "searchable": false,
            "className": "text-center"
        }],
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    table.on('draw', function() {
        $.unblockUI();
        $(".chk_agregar").each(function(index, element) {
            let object = $(this);
            $.each(seleccionados, function(index, value) {
                if (object.val() == value) {
                    object.prop('checked', true);
                }
            });
            if (object.prop('checked') != true) {
                todos = false;
            }
        });
        verifChkTodo();
    });

    function verifChkTodo() {
        let valChecked = true;
        $(".chk_agregar").each(function(index, element) {
            if ($(this).prop('checked') != true) {
                valChecked = false;
            }
        });
        if ($(".chk_agregar").length > 0) {
            $("#chk_todo").prop('checked', valChecked);
        } else {
            $("#chk_todo").prop('checked', false);
        }
        $("#span_selecc").html(seleccionados.length);
    }

    table.on('page', function() {
        $.blockUI({
            message: null
        });
    });

    $("#chk_todo").click(function() {
        let seleccion = $(this).prop('checked');
        $(".chk_agregar").each(function(index, element) {
            if ($(this).prop('checked') != seleccion) {
                $(this).click();
            }
        });
    });

    $("#a_eliminar").click(function() {
        // console.log(seleccionados);
        if (seleccionados.length > 0) {
            swal({
                title: "¿Estás seguro?",
                text: "Confirme si realmente desea Eliminar los productos seleccionados",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E74C3C",
                confirmButtonText: "Si, Eliminar!",
                closeOnConfirm: false
            }, function(isConfirm) {
                if (!isConfirm) return;
                $.post(baseurl + "catalog/deleteMassivelyProducts", { data: seleccionados }, function(data) {
                    answer = data.answer;
                    // console.log(answer);
                    if (answer == true) {
                        swal({
                                title: "La información",
                                text: "Se ha eliminado con éxito",
                                type: "success",
                                confirmButtonColor: "#337AB7",
                                showConfirmButton: true
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    location.href = baseurl + "catalog/listproducts";
                                }
                            }
                        );
                    } else if (answer == false) {
                        alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
                    }
                }, "json").fail(function() {
                    alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
                });
            });
        } else {
            alertswal("OOPS!!", "Debes de seleccionar al menos un producto", "info");
        }
    });


    $(document).on("click", "input[class='chk_agregar']", function() {
        if ($(this).prop('checked')) {
            seleccionados.push($(this).val());
        } else {
            var i = seleccionados.findIndex(function(element) {
                return element == this;
            }, $(this).val())
            seleccionados.splice(i, 1);
        }
        verifChkTodo();
    });

    $("#tableproducts tbody").on("click", "#a_remover", function() {
        var data = $(this).attr("data-delete");
        seleccionados.push(data);
        if (seleccionados.length > 0) {
            swal({
                title: "¿Estás seguro?",
                text: "Confirme si realmente desea Eliminar los productos seleccionados",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E74C3C",
                confirmButtonText: "Si, Eliminar!",
                closeOnConfirm: false
            }, function(isConfirm) {
                if (!isConfirm) return;
                $.post(baseurl + "catalog/deleteMassivelyProducts", { data: seleccionados }, function(data) {
                    answer = data.answer;
                    // console.log(answer);
                    if (answer == true) {
                        swal({
                                title: "La información",
                                text: "Se ha eliminado con éxito",
                                type: "success",
                                confirmButtonColor: "#337AB7",
                                showConfirmButton: true
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    location.href = baseurl + "catalog/listproducts";
                                }
                            }
                        );
                    } else if (answer == false) {
                        alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
                    }
                }, "json").fail(function() {
                    alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
                });
            });
        } else {
            alertswal("OOPS!!", "Debes de seleccionar al menos un producto", "error");
        }
    });

});