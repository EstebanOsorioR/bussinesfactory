$(document).ready(function() {

    const url = window.location;
    let baseurl = '',
        seleccionados = [];;
    if (url.hostname == 'localhost') {
        baseurl = url.origin + "/" + url.pathname.split('/')[1] + "/";
    } else {
        baseurl = url.origin + "/";
    }

    function alertswal(msn, inf, type) {
        swal({
            title: msn,
            text: inf,
            type: type,
            confirmButtonColor: "#3c8dbc",
            showConfirmButton: true,
        });
    }

    let table = $('#tabledocuments').DataTable({
        "autoWidth": false,
        "paging": true,
        "info": true,
        "filter": true,
        'stateSave': true,
        "processing": true,
        "serverSide": true,
        "order": [
            [1, "asc"]
        ],
        "ajax": {
            "url": baseurl + "issue/seeDucuments",
            "type": "POST"
        },
        "columns": [{
            "data": "document"
        }, {
            "data": "date"
        }, {
            "data": "voucher"
        }, {
            "data": "clinet",
        }, {
            "data": "nit",
        }, {
            "data": "total",
        }, {
            "data": "actions",
        }, {
            "data": "email",
        }],
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });



});