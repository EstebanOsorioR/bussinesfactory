$(document).ready(function() {

    const url = window.location;
    let baseurl = '',
        answer = '';
    if (url.hostname == 'localhost') {
        baseurl = url.origin + "/" + url.pathname.split('/')[1] + "/";
    } else {
        baseurl = url.origin + "/";
    }

    function alertswal(msn, inf, type) {
        swal({
            title: msn,
            text: inf,
            type: type,
            confirmButtonColor: "#3c8dbc",
            showConfirmButton: true,
        });
    }

    function alertswalurl(msn, inf, type, url) {
        swal({
                title: msn,
                text: inf,
                type: type,
                confirmButtonColor: "#3c8dbc",
                showConfirmButton: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    location.href = url;
                }
            }
        );
    }



    $("#tipodocumento").change(function() {
        let tipodocumento = $("#tipodocumento").val();

        $.ajax({
            type: 'POST',
            data: { dato: tipodocumento },
            url: baseurl + "issue/selectSequential",
            success: function(data) {
                // console.log(data == "");
                if (data != "") {
                    $("#secuencial").html('<option value="">Seleccione</option>' + data);
                } else {
                    $("#secuencial").html('<option value="">Seleccione</option>');
                }

            },
            error: function() {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            }
        })
    });
    $("#tipodocumento").change();


    $("#fechavencimiento").daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
        "locale": {
            "format": "DD-MM-YYYY",
            "separator": " - ",
            "applyLabel": "Guardar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "Hasta",
            "customRangeLabel": "Personalizar",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
        // "startDate": "2011-12-31",
        // "endDate": "2016-01-07",
        // "opens": "center"
    });

    $("#cliente").change(function() {
        let cliente = $("#cliente").val();

        $.ajax({
            type: 'POST',
            data: { dato: cliente },
            url: baseurl + "issue/seeClient",
            success: function(data) {
                // console.log(data == "");
                if (data != "") {
                    $("#clientedata").html(data);
                } else {
                    $("#clientedata").html('');
                }

            },
            error: function() {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            }
        })
    });
    $("#cliente").change();

    var porcentaje;
    $("#concepto").change(function() {
        let concepto = $("#concepto").val();
        $.ajax({
            type: 'POST',
            data: { dato: concepto },
            url: baseurl + "issue/seeConcept",
            success: function(data) {
                // let txt = data;
                if (data && data != "") {
                    let obj = JSON.parse(data);
                    // console.log(data);
                    $("#codigo").val(obj.code);
                    $("#tipo_producto").val(obj.producttype);
                    $("#descripcion").val(obj.description);
                    $("#unidad").val(obj.unit);
                    $("#codigo_unidad").val(obj.codeunit);
                    $("#cantidad").val("1");
                    $("#precio").val(obj.price);
                    $("#impuesto option[value='" + obj.tax + "']").prop("selected", true);
                    $("#tipoiva option[value='" + obj.taxtype + "']").prop("selected", true);
                    $("#descuento").val("0");
                    $('#agregar_concepto').attr("disabled", false);
                    $('#agregar_seriales').attr("disabled", false);
                    if (obj.taxtype == "05") {
                        porcentaje = 19;
                    } else if (obj.taxtype == "02") {
                        porcentaje = 16;
                    } else if (obj.taxtype == "03") {
                        porcentaje = 5;
                    } else {
                        porcentaje = 0;
                    }
                    $("#porcentaje_iva").val(porcentaje);
                } else {
                    $("#concepto  option:selected").removeAttr("selected");
                    $("#codigo").val("");
                    $("#tipo_producto").val("");
                    $("#descripcion").val("");
                    $("#unidad").val("");
                    $("#codigo_unidad").val("");
                    $("#cantidad").val("");
                    $("#precio").val("");
                    $("#impuesto  option:selected").removeAttr("selected");
                    $("#tipoiva option:selected").removeAttr("selected");
                    $("#descuento").val("0");
                    $('#agregar_concepto').attr("disabled", true);
                    $('#agregar_seriales').attr("disabled", true);
                    $("#porcentaje_iva").val("");

                }
            },
            error: function() {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            }
        })
    });
    $("#concepto").change();
    $("#tipoiva").change(function() {
        let tipoiva = $("#tipoiva").val();

        if (tipoiva == "05") {
            porcentaje = 19;
        } else if (tipoiva == "02") {
            porcentaje = 16;
        } else if (tipoiva == "03") {
            porcentaje = 5;
        } else {
            porcentaje = 0;
        }
        $("#porcentaje_iva").val(porcentaje);
    });





    // console.log(pocentaje);

    const tableitem = $("#detalle-item"),
        btnAddOption = $("#agregar_concepto"),
        btnUpdateOption = $("#editar_concepto"),
        concepto = $("#concepto"),
        product_id = $("#product_id"),
        codigo = $("#codigo"),
        unidad = $("#unidad"),
        unidad_codigo = $("#codigo_unidad"),
        tipo_producto = $("#tipo_producto"),
        descripcion = $("#descripcion"),
        cantidad = $("#cantidad"),
        precio = $("#precio"),
        impuesto = $("#impuesto"),
        tipoiva = $("#tipoiva"),
        porcentaje_iva = $("#porcentaje_iva"),
        descuento = $("#descuento"),
        agregar_concepto = $('#agregar_concepto'),
        agregar_seriales = $('#agregar_seriales');



    let PRODUCTS = []

    const initProduct = (id) => ({
        id,
        concepto: concepto.val(),
        codigo: codigo.val(),
        unidad: unidad.val(),
        unidad_codigo: unidad_codigo.val(),
        tipo_producto: tipo_producto.val(),
        descripcion: descripcion.val(),
        cantidad: cantidad.val(),
        precio: precio.val(),
        impuesto: impuesto.val(),
        tipoiva: tipoiva.val(),
        porcentaje_iva: porcentaje_iva.val(),
        descuento: descuento.val()
    })

    const add = (product) => {
        PRODUCTS = [...PRODUCTS, product]
    }

    const update = (id, product) => {
        const indice = PRODUCTS.findIndex(pro => pro.id === id)
        if (indice !== -1) {
            PRODUCTS[indice] = product
        }
    }
    const remove = (id) => {
        PRODUCTS = PRODUCTS.filter(product => product.id !== id)
    }

    const getProduct = id => {
        const product = PRODUCTS.find(product => product.id === id)
        return product || {}
    }

    const getTotal = (cantidad, valor, descuento, porcentaje_iva) => {
        const subtotal = cantidad * valor
        const total = subtotal - descuento
        const iva = getIva(cantidad, valor, porcentaje_iva)
        return total + iva
    }

    const getCantidadValor = (cantidad, valor) => {
        const subtotal = cantidad * valor
        return subtotal
    }

    const getIva = (cantidad, valor, porcentaje_iva) => {
        const subtotal = cantidad * valor
        const iva = subtotal * (porcentaje_iva / 100)
        return iva
    }

    const getIvasDescriminado19 = (cantidad, valor, porcentaje_iva) => {
        const subtotal = cantidad * valor
        if (porcentaje_iva == 19) {
            const impuesto19 = subtotal * (porcentaje_iva / 100)
            return impuesto19
        } else {
            return 0
        }

    }

    const getIvasDescriminado16 = (cantidad, valor, porcentaje_iva) => {
        const subtotal = cantidad * valor
        if (porcentaje_iva == 16) {
            const impuesto16 = subtotal * (porcentaje_iva / 100)
            return impuesto16
        } else {
            return 0
        }

    }

    const getIvasDescriminado5 = (cantidad, valor, porcentaje_iva) => {
        const subtotal = cantidad * valor
        if (porcentaje_iva == 5) {
            const impuesto5 = subtotal * (porcentaje_iva / 100)
            return impuesto5
        } else {
            return 0
        }

    }

    const getIvasDescriminado0 = (cantidad, valor, porcentaje_iva) => {
        const subtotal = cantidad * valor
        if (porcentaje_iva == 0) {
            const impuesto0 = subtotal * (porcentaje_iva / 100)
            return impuesto0
        } else {
            return 0
        }

    }



    const getValues = () => {
        const { subtotal = 0, total = 0, descuentos = 0, impuestos = 0, impuesto19 = 0, impuesto16 = 0, impuesto5 = 0, impuesto0 = 0 } = PRODUCTS.reduce((acc, current) => {
            const { subtotal = 0, total = 0, descuentos = 0, impuestos = 0, impuesto19 = 0, impuesto16 = 0, impuesto5 = 0, impuesto0 = 0 } = acc
            const { cantidad, precio, descuento, porcentaje_iva } = current
            const sub = (cantidad * precio) - descuento

            return {
                subtotal: subtotal + sub,
                total: total + getTotal(cantidad, precio, descuento, porcentaje_iva),
                descuentos: descuentos + Number(descuento),
                impuestos: impuestos + getIva(cantidad, precio, porcentaje_iva),
                impuesto19: impuesto19 + getIvasDescriminado19(cantidad, precio, porcentaje_iva),
                impuesto16: impuesto16 + getIvasDescriminado16(cantidad, precio, porcentaje_iva),
                impuesto5: impuesto5 + getIvasDescriminado5(cantidad, precio, porcentaje_iva),
                impuesto0: impuesto0 + getIvasDescriminado0(cantidad, precio, porcentaje_iva)
            }
        }, {})

        // por si tengo que descrimnar los ivas
        // let
        //     subtotal = 0,
        //     total = 0,
        //     descuentos = 0,
        //     impuesto19 = 0,
        //     impuesto16 = 0


        // for (let i = 0; PRODUCTS.length < i; i++) {
        //     const { cantidad, precio, descuento, porcentaje_iva } = PRODUCTS[i]

        //     if (porcentaje_iva === 19) {
        //         impuesto19 = impuestos + getIva(cantidad, precio, porcentaje_iva)
        //     }

        //     // if(porcentaje_iva === 16){}

        //     // subtotal = subtotal + sub,
        //     // total= total + getTotal(cantidad, precio, descuento, porcentaje_iva),
        //     // descuentos =descuentos + Number(descuento),
        //     // impuestos = impuestos + getIva(cantidad, precio, porcentaje_iva)
        // }




        $("#subtotal").html(`$ ${subtotal}`)
        $("#input_subtotal").val(subtotal)
        $("#descuento_total").html(`$ ${descuentos}`)
        $("#input_descuento_total").val(descuentos)
        $("#ivas").html(`$ ${impuestos}`)
        $("#input_ivas").val(impuestos)
        $("#total_total").html(`$ ${total}`)
        $("#input_total_total").val(total)


        $("#input_porcentaje_19").val(impuesto19)
        $("#input_porcentaje_16").val(impuesto16)
        $("#input_porcentaje_5").val(impuesto5)
        $("#input_porcentaje_excluido").val(impuesto0)
        $("#input_subtotal_gobal").val(subtotal)
        $("#input_ivas_totales").val(impuestos)


    }

    const render = () =>
        PRODUCTS.map(({
            id,
            codigo,
            cantidad,
            unidad,
            unidad_codigo,
            tipo_producto,
            descripcion,
            precio,
            porcentaje_iva,
            // impuesto,
            descuento
        }, index) => (`<tr class="fila">
        <th>${index}</th>
        <th>${codigo}</th>
        <input type="hidden" name="frmitems[]" value="${codigo}">
        <th>${cantidad}</th>
        <input type="hidden" name="frmitems[]" value="${cantidad}">
        <th>${unidad}</th>
        <input type="hidden" name="frmitems[]" value="${unidad_codigo}">
        <th> ${tipo_producto} - ${descripcion}</th>
        <input type="hidden" name="frmitems[]" value="${tipo_producto} - ${descripcion}">
        <th>${precio}</th>
        <input type="hidden" name="frmitems[]" value="${precio}">
        <input type="hidden" name="frmitems[]" value="${getCantidadValor(cantidad, precio)}">
        <th>${porcentaje_iva}</th>
        <input type="hidden" name="frmitems[]" value="${porcentaje_iva}">
        <th>${getIva(cantidad, precio, porcentaje_iva)}</th>
        <input type="hidden" name="frmitems[]" value="${getIva (cantidad, precio, porcentaje_iva)}">
        <th>${descuento}</th>
        <input type="hidden" name="frmitems[]" value="${descuento}">
        <th>${getTotal(cantidad, precio, descuento, porcentaje_iva)}</th>
        <input type="hidden" name="frmitems[]" value="${getTotal(cantidad, precio, descuento, porcentaje_iva)}">
        <th style="text-align: right;">
        <button type="button" class="btn btn-success edit-product" data-indice=${id}> <i class="fa fa-pencil"></i></button>
        <button type="button" class="btn btn-danger remove-product"  data-indice=${id}> <i class="fa fa-times"></i></button>
        </th>
       </tr> `))





    //Aqui agregar todos los ID del formulario

    const clearForm = () => {
        concepto.val('');
        codigo.val('');
        unidad.val('');
        unidad_codigo.val('');
        tipo_producto.val('');
        descripcion.val('');
        cantidad.val('');
        precio.val('');
        // limpiar el resto de campos
        // impuesto.prop('selected', false);
        // tipoiva.prop("selected", false);
        $("#impuesto  option:selected").removeAttr("selected");
        $("#tipoiva option:selected").removeAttr("selected");

        porcentaje_iva.val('');
        descuento.val('');
        agregar_concepto.attr("disabled", true);
        agregar_seriales.attr("disabled", true);

    }

    btnAddOption.click(() => {
        const product = initProduct(PRODUCTS.length)
        clearForm()
        add(product);
        const rows = render();
        tableitem.html(rows);
        getValues()
    });


    $(document).on('click', '.remove-product', function() {
        const id = Number($(this).data('indice'))
        remove(id)
        const rows = render();
        tableitem.html(rows);
        getValues()
    })


    $(document).on('click', '.edit-product', function() {
        const id = $(this).data('indice')
        const product = getProduct(id)
        concepto.val(product.concepto);
        codigo.val(product.codigo);
        unidad.val(product.unidad);
        unidad_codigo.val(product.unidad_codigo);
        tipo_producto.val(product.tipo_producto);
        descripcion.val(product.descripcion);
        cantidad.val(product.cantidad)
        precio.val(product.precio)
        product_id.val(product.id)
            //setear el resto de capos 
            // impuesto.val(product.impuesto)
            // tipoiva.val(product.tipoiva)
        $("#impuesto option[value='" + (product.impuesto) + "']").prop("selected", true);
        $("#tipoiva option[value='" + (product.tipoiva) + "']").prop("selected", true);
        porcentaje_iva.val(product.porcentaje_iva)
        descuento.val(product.descuento)
        btnAddOption.css('display', 'none')
        btnUpdateOption.css('display', 'block')
        console.log(product.impuesto);
    })

    btnUpdateOption.click(() => {
        const id = Number(product_id.val())
        const product = initProduct(id)
        update(id, product)
        clearForm()
        const rows = render();
        tableitem.html(rows);
        btnAddOption.css('display', 'block')
        btnUpdateOption.css('display', 'none')
        getValues()
    })

    // console.log(iiii);

    // btnAddOption.click();
    // generateItem();


    $(document).on('submit', 'form[name="frmvoucher[]"]', function(e) {
        e.preventDefault();
        $.post(baseurl + "issue/generateDocument", $(this).serialize(), function(data) {
            if (data && data != "") {
                let codigo = data.codigo,
                    mensaje = data.mensaje,
                    mensajesValidacion = data.mensajesValidacion;
                if (codigo == 200) {
                    alertswalurl("Se generó", mensaje, "success", baseurl + "issue/listvoucher");
                } else {
                    alertswalurl("OOPS!!", mensaje + "-" + mensajesValidacion, "error", baseurl + "issue/listvoucher");
                }
            } else {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            }
        }, "json").fail(function() {
            alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
        });
    });

});