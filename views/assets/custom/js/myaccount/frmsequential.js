$(document).ready(function() {

    const url = window.location;
    let baseurl = '',
        answer = '';
    if (url.hostname == 'localhost') {
        baseurl = url.origin + "/" + url.pathname.split('/')[1] + "/";
    } else {
        baseurl = url.origin + "/";
    }

    function alertswal(msn, inf, type) {
        swal({
            title: msn,
            text: inf,
            type: type,
            confirmButtonColor: "#3c8dbc",
            showConfirmButton: true,
        });
    }

    function alertswalurl(msn, inf, type, url) {
        swal({
                title: msn,
                text: inf,
                type: type,
                confirmButtonColor: "#3c8dbc",
                showConfirmButton: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    location.href = url;
                }
            }
        );
    }


    $("#fechainicio").daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
        "locale": {
            "format": "DD-MM-YYYY",
            "separator": " - ",
            "applyLabel": "Guardar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "Hasta",
            "customRangeLabel": "Personalizar",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
        // "startDate": "2021-05-01",
        // "endDate": "2016-01-07",
        // "opens": "center"

    });


    $("#fechafin").daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
        "locale": {
            "format": "DD-MM-YYYY",
            "separator": " - ",
            "applyLabel": "Guardar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "Hasta",
            "customRangeLabel": "Personalizar",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
        // "startDate": "2011-12-31",
        // "endDate": "2016-01-07",
        // "opens": "center"
    });


    //  Funcion para guardar y editar sedes
    $(document).on('submit', 'form[name="frmsequential[]"]', function(e) {
        e.preventDefault();
        $.post(baseurl + "myaccount/saveEditSequential", $(this).serialize(), function(data) {

            answer = data.answer;
            console.log(answer);
            if (answer == "success_save") {
                alertswalurl("La información", "Se guardó satisfactoriamente", "success", baseurl + "myaccount/account");
            } else if (answer == "success_edit") {
                alertswalurl("Los cambios", "Se guardó satisfactoriamente", "success", baseurl + "myaccount/account");
            } else if (answer == "exists") {
                $("#notify").html('<div class="col-md-6"><div class="alert alert-warning alert-dismissible fade in text-center" role="alert">' + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>' + '</button>' + '<strong>Este secuencial </strong>ya ha sido creada anteriormente.' + '</div>' + '</div>');
            } else if (answer == "forced") {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            } else if (answer == "error") {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            } else {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            }
        }, "json").fail(function() {
            alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
        });
    });




});