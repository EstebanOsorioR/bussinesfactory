$(document).ready(function() {

    const url = window.location;
    let baseurl = '',
        answerlogin = "#answerlogin",
        cssInvalidUser = {
            "text-align": "center",
            "color": "red",
            "bottom": "-10px",
            "position": "relative"
        };

    if (url.hostname == 'localhost') {
        baseurl = url.origin + "/" + url.pathname.split('/')[1] + "/";
    } else {
        baseurl = url.origin + "/";
    }

    function alertswal(msn, inf, type) {
        swal({
            title: msn,
            text: inf,
            type: type,
            confirmButtonColor: "#3c8dbc",
            showConfirmButton: true,
        });
    }

    function alertswalurl(msn, inf, type, url) {
        swal({
                title: msn,
                text: inf,
                type: type,
                confirmButtonColor: "#3c8dbc",
                showConfirmButton: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    location.href = url;
                }
            }
        );
    }

    $.post(baseurl + "myaccount/checkStatusAlert", function(data) {
        // console.log(typeof(data));
        if (typeof(data)) {
            answer = data.answer;
            if (answer == "alertpass") {
                alertswal("Importante", "Debe Actualizar su contraseña por medidas de seguridad.", "info");
            }
        }
    }, "json").fail(function() {
        // alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
    })


    //has uppercase
    window.Parsley.addValidator('uppercase', {
        requirementType: 'number',
        validateString: function(value, requirement) {
            var uppercases = value.match(/[A-Z]/g) || [];
            return uppercases.length >= requirement;
        },
        messages: {
            es: 'Su contraseña debe contener al menos (%s) letra mayúscula.',
            en: 'Your password must contain at least (%s) uppercase letter.'
        }
    });

    //has lowercase
    window.Parsley.addValidator('lowercase', {
        requirementType: 'number',
        validateString: function(value, requirement) {
            var lowecases = value.match(/[a-z]/g) || [];
            return lowecases.length >= requirement;
        },
        messages: {
            es: 'Su contraseña debe contener al menos (%s) letra minúscula.',
            en: 'Your password must contain at least (%s) lowercase letter.'
        }
    });

    //has number
    window.Parsley.addValidator('number', {
        requirementType: 'number',
        validateString: function(value, requirement) {
            var numbers = value.match(/[0-9]/g) || [];
            return numbers.length >= requirement;
        },
        messages: {
            es: 'Su contraseña debe contener al menos (%s) número.',
            en: 'Your password must contain at least (%s) number.'
        }
    });

    //has special char
    window.Parsley.addValidator('special', {
        requirementType: 'number',
        validateString: function(value, requirement) {
            var specials = value.match(/[^a-zA-Z0-9]/g) || [];
            return specials.length >= requirement;
        },
        messages: {
            es: 'Su contraseña debe contener al menos (%s) caracteres especiales.',
            en: 'Your password must contain at least (%s) special characters.'
        }
    });

    $(document).on('submit', 'form[name="frmpassword[]"]', function(e) {
        e.preventDefault();
        $.post(baseurl + "myaccount/changePass", $(this).serialize(), function(data) {
            answer = data.answer;
            console.log(answer);
            if (answer == "success") {
                alertswalurl("Su contraseña", "Ha sido actualizada de manera exitosa.", "success", baseurl + "home/index");
            } else if (answer == "success_edit") {
                alertswalurl("Su contraseña ha sido actualizada de manera exitosa", "Por medidas de Seguridad, debe iniciar sesion nuevamente.", "success", baseurl + "home/logout");
            } else if (answer == 'same') {
                alertswal("OOPS!!", "La contraseña no puede ser igual a la contraseña anterior", "info");
            } else if (answer == "samenew") {
                alertswal("OOPS!!", "La contraseña nueva debe ser igual a la contreseña confimada.", "info");
            } else if (answer == "no_agree") {
                alertswal("OOPS!!", "La contraseña actual es incorrecta. Favor de verificar sus datos.", "info");
            } else if (answer == "error") {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            } else {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            }
        }, "json").fail(function() {
            alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
        })

    })

});