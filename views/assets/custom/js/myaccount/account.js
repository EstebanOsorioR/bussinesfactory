$(document).ready(function() {

    const url = window.location;
    let baseurl = '',
        seleccionados = [];
    if (url.hostname == 'localhost') {
        baseurl = url.origin + "/" + url.pathname.split('/')[1] + "/";
    } else {
        baseurl = url.origin + "/";
    }

    function alertswal(msn, inf, type) {
        swal({
            title: msn,
            text: inf,
            type: type,
            confirmButtonColor: "#3c8dbc",
            showConfirmButton: true,
        });
    }

    let tablefolios = $('#tablefolios').DataTable({
        "autoWidth": false,
        "paging": true,
        "info": true,
        "filter": true,
        "ordering": false,
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    let table = $('#tablesequential').DataTable({
        "autoWidth": false,
        "paging": true,
        "info": true,
        "filter": true,
        'stateSave': true,
        "processing": true,
        "serverSide": true,
        // "ordering": false,
        "order": [
            [1, "asc"]
        ],
        "ajax": {
            "url": baseurl + "myaccount/seeSequential",
            "type": "POST"
        },
        "columns": [{
            "data": "document"
        }, {
            "data": "establishment"
        }, {
            "data": "prefix"
        }, {
            "data": "startdate",
        }, {
            "data": "enddate",
        }, {
            "data": "active",
        }, {
            "data": "environment",
        }, {
            "data": "actions",
        }],
        "columnDefs": [{
            "targets": 7,
            "orderable": false,
            "searchable": false,
            "className": "text-center"
        }],
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $("#tablesequential tbody").on("click", "#a_remover", function() {
        var data = $(this).attr("data-delete");
        seleccionados.push(data);
        if (seleccionados.length > 0) {
            swal({
                title: "¿Estás seguro?",
                text: "Confirme si realmente desea Eliminar los clientes seleccionados",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E74C3C",
                confirmButtonText: "Si, Eliminar!",
                closeOnConfirm: false
            }, function(isConfirm) {
                if (!isConfirm) return;
                $.post(baseurl + "myaccount/inactivateSequential", { data: seleccionados }, function(data) {
                    answer = data.answer;
                    console.log(answer);
                    if (answer == true) {
                        swal({
                                title: "La información",
                                text: "Se ha eliminado con éxito",
                                type: "success",
                                confirmButtonColor: "#337AB7",
                                showConfirmButton: true
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    location.href = baseurl + "myaccount/account";
                                }
                            }
                        );
                    } else if (answer == false) {
                        alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
                    }
                }, "json").fail(function() {
                    alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
                });
            });
        } else {
            alertswal("OOPS!!", "Debes de seleccionar al menos un cliente", "error");
        }
    });


});