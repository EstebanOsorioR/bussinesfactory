$(document).ready(function() {

    const url = window.location;
    let baseurl = '',
        answer = '';
    if (url.hostname == 'localhost') {
        baseurl = url.origin + "/" + url.pathname.split('/')[1] + "/";
    } else {
        baseurl = url.origin + "/";
    }

    function alertswal(msn, inf, type) {
        swal({
            title: msn,
            text: inf,
            type: type,
            confirmButtonColor: "#3c8dbc",
            showConfirmButton: true,
        });
    }

    function alertswalurl(msn, inf, type, url) {
        swal({
                title: msn,
                text: inf,
                type: type,
                confirmButtonColor: "#3c8dbc",
                showConfirmButton: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    location.href = url;
                }
            }
        );
    }



    function calcularDigitoVerificacion(myNit) {
        var vpri,
            x,
            y,
            z;

        // Se limpia el Nit
        myNit = myNit.replace(/\s/g, ""); // Espacios
        myNit = myNit.replace(/,/g, ""); // Comas
        myNit = myNit.replace(/\./g, ""); // Puntos
        myNit = myNit.replace(/-/g, ""); // Guiones

        // Se valida el nit
        if (isNaN(myNit)) {
            console.log("El nit/cédula '" + myNit + "' no es válido(a).");
            return "";
        };

        // Procedimiento
        vpri = new Array(16);
        z = myNit.length;

        vpri[1] = 3;
        vpri[2] = 7;
        vpri[3] = 13;
        vpri[4] = 17;
        vpri[5] = 19;
        vpri[6] = 23;
        vpri[7] = 29;
        vpri[8] = 37;
        vpri[9] = 41;
        vpri[10] = 43;
        vpri[11] = 47;
        vpri[12] = 53;
        vpri[13] = 59;
        vpri[14] = 67;
        vpri[15] = 71;

        x = 0;
        y = 0;
        for (var i = 0; i < z; i++) {
            y = (myNit.substr(i, 1));
            // console.log ( y + "x" + vpri[z-i] + ":" ) ;

            x += (y * vpri[z - i]);
            // console.log ( x ) ;    
        }

        y = x % 11;
        // console.log ( y ) ;

        return (y > 1) ? 11 - y : y;
    }

    // Calcular
    function calcular() {

        // Verificar que haya un numero
        let nit = document.getElementById("nit").value;
        let isNitValid = nit >>> 0 === parseFloat(nit) ? true : false; // Validate a positive integer

        // Si es un número se calcula el Dígito de Verificación
        if (isNitValid) {
            let inputDigVerificacion = document.getElementById("digitoVerificacion");
            inputDigVerificacion.value = calcularDigitoVerificacion(nit);
        }
    }

    $("#nit").change(function() {
        calcular();
    });
    $("#nit").change();

    $("#departament").change(function() {
        let departament = $("#departament").val();

        $.ajax({
            type: 'POST',
            data: { dato: departament },
            url: baseurl + "myaccount/selectCity",
            success: function(data) {
                // console.log(data == "");
                if (data != "") {
                    $("#city").html('<option value="">Seleccione</option>' + data);
                } else {
                    $("#city").html('<option value="">Seleccione</option>');
                }

            },
            error: function() {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            }
        })
    });
    $("#departament").change();

    $.post(baseurl + "myaccount/checkStatusAlert", function(data) {
        // console.log(typeof(data));
        if (typeof(data)) {
            answer = data.answer;
            if (answer == "alertaccount") {
                alertswal("Para poder continuar!", "Debe completar todos los datos requeridos de su empresa.", "info");
            }
        }
    }, "json").fail(function() {
        // alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
    })



    // Funcion para guardar y editar cuenta
    $(document).on('submit', 'form[name="frmmyaccount[]"]', function(e) {
        e.preventDefault();
        $.post(baseurl + "myaccount/saveEditAccount", $(this).serialize(), function(data) {
            answer = data.answer;
            console.log(answer);
            if (answer == "success_edit") {
                alertswalurl("Los cambios", "Se guardó satisfactoriamente", "success", baseurl + "home/index");
            } else if (answer == "error") {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            } else {
                alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
            }
        }, "json").fail(function() {
            alertswal("OOPS!!", "Error 500 - Error de servidor interno.", "error");
        });
    });

    // $('select').selectpicker({
    //     showTick: true,
    // });

});