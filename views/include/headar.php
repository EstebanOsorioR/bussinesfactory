<?php
// session_start(); // Activa la variable de sesion
if (!$_SESSION["validar"]) {
    header("location:" . SERVERURL);
    exit();
}
  $razon =$_SESSION["emisor_razon_social"];
  $nit = $_SESSION["emisor_numero_identificacion"];   
  $codver = $_SESSION["emisor_digito_verificacion"] ;

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Website description-->
    <meta name="description" content = "Business Factory"/>
    <meta name="author" content ="Edison Monsalve Y Estaban Osorio">
    <meta name="keywords" content ="Aplicación Web Facturación Electrónica">

    <title>Business Factory <?php echo $title; ?></title>

    <!-- App Icon -->
    <link rel="icon" href="<?php echo SERVERURL; ?>views/assets/images/LogoOriginalIncompleto.png">
    <!-- Bootstrap -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo SERVERURL; ?>views/assets/build/css/custom.css?<?php echo time(); ?>" rel="stylesheet">
    <?php
      $list =  substr($_GET["a"],0,4);
      if ($list == "list" || $_GET["a"] == "frmmeasureunit" ||  $_GET["a"] == "account") {
        echo '<!-- Datatables -->';
        echo '<link href="' . SERVERURL . 'views/assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">';
        echo '<link href="' . SERVERURL . 'views/assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">';
      }
    ?>
    <!-- iCheck -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Sweetalert -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/sweetalert/dist/sweetalert.css" rel="stylesheet">    
    <?php
      if ($_GET["a"] == "frmsequential" || $_GET["a"] == "voucher" || $_GET["a"] == "index") {
        echo '<!-- bootstrap-daterangepicker -->';
        echo'<link href="' . SERVERURL . 'views/assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">';
    
      }
    ?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title" id="site_title"><img src="<?php echo SERVERURL; ?>views/assets/images/LogoOriginal.png" alt="" width="170" height="50"></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div>
            <div class="profile clearfix busines">
              <div class="profile_pic">
                <img src="<?php echo SERVERURL; ?>views/assets/images/user.png" alt="..." class="img-circle profile_img">
              </div>
            </div>
            <div class="profile_info" id="profile_info">
              <?php
              $see = new HomeController();
              $see->seeDataIssuing();
              // var_dump($see);
              ?>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />
            <br />
            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                <li><a href="<?php echo SERVERURL; ?>home/index"><i class="fa fa-home"></i> Inicio </a></li>
                  <li><a><i class="fa fa-file-text"></i> Catálogo <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo SERVERURL; ?>catalog/listclients">Clientes</a></li>
                      <li><a href="<?php echo SERVERURL; ?>catalog/listmeasureunit">Unidades de Medida</a></li>
                      <li><a href="<?php echo SERVERURL; ?>catalog/listproducts">Productos</a></li>
                      <li><a href="#">Establecimientos</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-sign-out"></i> Facturación <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo SERVERURL; ?>issue/voucher">Emitir comprobante</a></li>
                      <li><a href="<?php echo SERVERURL; ?>issue/listvoucher">Comprobantes emitidos</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-cogs"></i> Configuración <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo SERVERURL; ?>myaccount/account">Datos fiscales</a></li>
                      <li><a href="#">Personalización</a></li>
                      <li><a href="<?php echo SERVERURL; ?>myaccount/frmpassword">Cambio contraseña</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> Soporte <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#">Manual de usuario</a></li>
                      <li><a href="#">Administración</a></li>
                    </ul>
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Configuración">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Pantalla completa">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Cerrar con llave">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Cerrar sesión" href="<?php echo SERVERURL; ?>home/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo SERVERURL; ?>views/assets/images/user.png" alt=""><?php echo $razon;?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Configuración</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Ayuda</a></li>
                    <li><a href="<?php echo SERVERURL; ?>home/logout"><i class="fa fa-sign-out pull-right"></i> Cerrar sesión</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
      