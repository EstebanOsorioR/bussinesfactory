<div class="clearfix"></div>
   
</div>
      <!-- /page content -->
  <!-- footer content -->
  <footer>
    <div class="pull-right">
      <a href="#">Business Factory</a> - Sistemas de facturación electrónica
    </div>
    <div class="clearfix"></div>
  </footer>
  <!-- /footer content -->
  </div>
  </div>

  <!-- jQuery -->
  <script src="<?php echo SERVERURL; ?>views/assets/vendors/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="<?php echo SERVERURL; ?>views/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="<?php echo SERVERURL; ?>views/assets/vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="<?php echo SERVERURL; ?>views/assets/vendors/nprogress/nprogress.js"></script>


  <?php
  $list =  substr($_GET["a"],0,4);
  if ($list == "list" || $_GET["a"] == "frmmeasureunit" ||  $_GET["a"] == "account") {
    echo '<!-- Datatables -->';
    echo '<script src="' . SERVERURL . 'views/assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>';
    echo '<script src="' . SERVERURL . 'views/assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>';
    echo '<script src="' . SERVERURL . 'views/assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>';
    echo '<script src="' . SERVERURL . 'views/assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>';
    echo '<script src="' . SERVERURL . 'views/assets/vendors/jquery.blockUI.js"></script>';

  }


  $frm =  substr($_GET["a"],0,3);
  if ($frm == "frm" ||  $_GET["a"] == "voucher") {
    echo '<!-- Parsley -->';
    echo '<script src="' . SERVERURL . 'views/assets/vendors/parsley/dist/parsley.min.js"></script>';
    echo '<script src="' . SERVERURL . 'views/assets/vendors/parsley/dist/i18n/es.js"></script>';
  }
  ?>



  <!-- iCheck -->
  <script src="<?php echo SERVERURL; ?>views/assets/vendors/iCheck/icheck.min.js"></script>

  <?php 
        if ($_GET["a"] == "frmsequential" || $_GET["a"] == "voucher" || $_GET["a"] == "index") {
          echo '<!-- bootstrap-daterangepicker -->';
          echo '<script src="' . SERVERURL . 'views/assets/vendors/moment/min/moment.min.js"></script>';
          echo '<script src="' . SERVERURL . 'views/assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>';      
        }
  ?>
  <?php
       if ($_GET["a"] == "index") {
        echo '<!-- jQuery Sparklines -->';
        echo '<script src="' . SERVERURL . 'views/assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>';
        echo '<!-- Flot -->';
        echo '<script src="' . SERVERURL . 'views/assets/vendors/Flot/jquery.flot.js"></script>';
        echo '<script src="' . SERVERURL . 'views/assets/vendors/Flot/jquery.flot.pie.js"></script>';      
        echo '<script src="' . SERVERURL . 'views/assets/vendors/Flot/jquery.flot.time.js"></script>';  
        echo '<script src="' . SERVERURL . 'views/assets/vendors/Flot/jquery.flot.stack.js"></script>';  
        echo '<script src="' . SERVERURL . 'views/assets/vendors/Flot/jquery.flot.resize.js"></script>'; 
        echo '<!-- Flot plugins -->';
        echo '<script src="' . SERVERURL . 'views/assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>';
        echo '<script src="' . SERVERURL . 'views/assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>';
        echo '<script src="' . SERVERURL . 'views/assets/vendors/flot.curvedlines/curvedLines.js"></script>';
        echo '<!-- DateJS -->';
        echo '<script src="' . SERVERURL . 'views/assets/vendors/DateJS/build/date.js"></script>';

      }
?>
  


  <!-- Sweetalert -->
  <script src="<?php echo SERVERURL; ?>views/assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
  <!-- Custom Theme Scripts -->
  <script src="<?php echo SERVERURL; ?>views/assets/build/js/custom.js?<?php echo time(); ?>"></script>
  <!-- Custom Scripts -->
  <?php
      switch ($_GET["a"]) {
        // Mi Cuenta Emisor
        case "account":
          echo '<script src="' . SERVERURL . 'views/assets/custom/js/myaccount/account.js?' . time() . '"></script>';
          break;
        case "frmaccount":
          echo '<script src="' . SERVERURL . 'views/assets/custom/js/myaccount/frm.js?' . time() . '"></script>';
          break;
          case "frmsequential":
            echo '<script src="' . SERVERURL . 'views/assets/custom/js/myaccount/frmsequential.js?' . time() . '"></script>';
            break;
        case "frmpassword":
          echo '<script src="' . SERVERURL . 'views/assets/custom/js/myaccount/password.js?' . time() . '"></script>';
          break;
        //Emisión  
        case "voucher":
          echo '<script src="' . SERVERURL . 'views/assets/custom/js/issue/voucher.js?' . time() . '"></script>';
          break;
        case "listvoucher":
          echo '<script src="' . SERVERURL . 'views/assets/custom/js/issue/list.js?' . time() . '"></script>';
          break;
        // Catálogo
        case "frmclient": 
          echo '<script src="' . SERVERURL . 'views/assets/custom/js/client/frm.js?' . time() . '"></script>';
          break;
        case "listclients": 
            echo '<script src="' . SERVERURL . 'views/assets/custom/js/client/list.js?' . time() . '"></script>';
            break;
        case "listmeasureunit":
            echo '<script src="' . SERVERURL . 'views/assets/custom/js/measureunit/list.js?' . time() . '"></script>';
            break;
        case "frmmeasureunit": 
            echo '<script src="' . SERVERURL . 'views/assets/custom/js/measureunit/frm.js?' . time() . '"></script>';
            break;
        case "listproducts":
            echo '<script src="' . SERVERURL . 'views/assets/custom/js/products/list.js?' . time() . '"></script>';
            break;
        case "frmproducts":
            echo '<script src="' . SERVERURL . 'views/assets/custom/js/products/frm.js?' . time() . '"></script>';
            break;
      }
  ?>

</body>
</html>