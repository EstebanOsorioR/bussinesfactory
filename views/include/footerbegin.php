<!-- jQuery -->
<script src="<?php echo SERVERURL; ?>views/assets/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo SERVERURL; ?>views/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- NProgress -->
<script src="<?php echo SERVERURL; ?>views/assets/vendors/nprogress/nprogress.js"></script>
<!-- Parsley -->
<script src="<?php echo SERVERURL; ?>views/assets/vendors/parsley/dist/parsley.min.js"></script>
<script src="<?php echo SERVERURL; ?>views/assets/vendors/parsley/dist/i18n/es.js"></script>
<!-- Sweetalert -->
<script src="<?php echo SERVERURL; ?>views/assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
<!-- blockUI -->
<script src="<?php echo SERVERURL; ?>views/assets/vendors/jquery.blockUI.js"></script>
<!-- API Recapcha -->
<script src="https://www.google.com/recaptcha/api.js?hl=es" async defer></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo SERVERURL; ?>views/assets/build/js/custom.js"></script>
<!-- Custom Scripts -->
<?php 
if (isset($_GET["a"]) && $_GET["a"] == "restorePassword") {
    echo '<script src=" '.SERVERURL.'views/assets/custom/js/main/restore.js?'.time().'"></script>';
}else {
    echo '<script src=" '.SERVERURL.'views/assets/custom/js/main/login.js?'.time().'"></script>';
}
?>

</body>
</html>