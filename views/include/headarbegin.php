<!DOCTYPE html>
<html lang="es">
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Website description-->
    <meta name="description" content = "Business Factory"/>
    <meta name="author" content ="Edison Monsalve Y Estaban Osorio">
    <meta name="keywords" content ="Aplicación Web Facturación Electrónica">  

    <title>Business Factory</title>

    <!-- App Icon -->
    <link rel="icon" href="<?php echo SERVERURL; ?>views/assets/images/LogoOriginalIncompleto.png">
    <!-- Bootstrap -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/animate.css/animate.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo SERVERURL; ?>views/assets/build/css/custom.css" rel="stylesheet">
    <!-- Sweetalert -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/sweetalert/dist/sweetalert.css" rel="stylesheet">

  </head>
