<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Catálogo <small>Clientes </small>
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <a class="btn btn-primary" href="<?php echo SERVERURL; ?>catalog/frmclient"><i class="fa fa-plus-circle"></i>  &nbsp; Agregar Cliente</a>
                    <a class="btn btn-primary" id="a_eliminar"><i class="fa fa-minus-circle"></i>  &nbsp; Eliminar</a>

                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a></li>
                                <li><a href="#">Settings 2</a></li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    <table id="tableclients" class="table table-bordered table-hover dt-responsive">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 28px;"><input type="checkbox" id="chk_todo"></th>
                                <th>Tipo de cliente</th>
                                <th>Tipo de Identificación</th>
                                <th>Número de Identificación</th>
                                <th>Razón social/Nombre</th>
                                <th>Correo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                    </table>
                    <div><span id="span_selecc"></span> Registro(s) seleccionado(s)</div>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
</div>