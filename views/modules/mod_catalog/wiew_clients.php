<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Detalle <small>cliente: <?php echo $data["cliente_razon_social"];?> </small>
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <a class="btn btn-primary" href="<?php echo SERVERURL; ?>catalog/listclients"> <i class="fa fa-arrow-left"></i>  &nbsp; Regresar</a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <!-- content starts here -->
                <table class="table table-bordered">
                    <tbody>
                        <?php
                        // var_dump($data);exit();
                        if (!empty($data)) {

                            // var_dump($data);exit();
                           
                                // var_dump($value);exit();
                        echo '  <tr>
                                    <th colspan="2" style="text-align:center">Datos del cliente</th>
                                </tr>
                                 <tr>
                                    <th scope="row">Tipo de cliente	</th>
                                    <td>'.$data["tipoper_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Razón social/Nombre	</th>
                                    <td>'.$data["cliente_razon_social"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Tipo de Identificación</th>
                                    <td>'.$data["tipoiden_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Número de Identificación</th>
                                    <td>'.$data["cliente_numero_identificacion"]."-".$data["cliente_digito_verificacion"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Régimen Fiscal</th>
                                    <td>'.$data["regfiscal_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Correo</th>
                                    <td>'.$data["cliente_email"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Teléfonos</th>
                                    <td>'.$data["cliente_telefono"].'</td>
                                </tr> 
                                <tr>
                                    <th scope="row">Referencia</th>
                                    <td>'.$data["cliente_nombre_contacto"].'</td>
                                </tr>
                                <tr>
                                    <th colspan="2" style="text-align:center">Información adicional</th>
                                </tr>
                                <tr>
                                    <th scope="row">Detalle Tributario</th>
                                    <td>'.$data["tributofiscal_codigo"]."-".$data["tributofiscal_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Responsabilidades Fiscales	</th>
                                    <td>'.$data["responsfiscal_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Actividad Económica	</th>
                                    <td>'.$data["actividad_codigo"]."-".$data["actividad_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th colspan="2" style="text-align:center">Dirección de domicilio</th>
                                </tr>
                                <tr>
                                    <th scope="row">País</th>
                                    <td>'.$data["pais_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Departamento</th>
                                    <td>'.ucfirst(strtolower($data["departamento_nombre"])).'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Municipio</th>
                                    <td>'.ucfirst(strtolower($data["ciudad_nombre"])).'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Dirección</th>
                                    <td>'.$data["cliente_direccion"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Código Postal</th>
                                    <td>'.$data["cliente_codigo_postal"].'</td>
                                </tr>
                                ';
                        }
                        ?>
                    </tbody>
                </table>
                <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
</div>
