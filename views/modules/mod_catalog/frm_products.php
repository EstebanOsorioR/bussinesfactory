<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Agregar <small>producto </small>
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <a class="btn btn-primary" href="<?php echo SERVERURL; ?>catalog/listproducts"> <i class="fa fa-arrow-left"></i>  &nbsp; Regresar</a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    <form  class="form-horizontal" id="form" data-parsley-validate method="post" name="frmproducts[]">
                    <?php
                        if (empty($data)) {//si $data viene vacia
                            echo '<input type="hidden" name="frmproducts[]" value="0">';
                        } else {
                            echo '<input type="hidden" name="frmproducts[]" value="1">';
                        }
                    ?>
                        <!-- Descripción -->
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Descripción</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                            

                                    <div class="form-group">   
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label>Tipo: <span class="required">*</span></label>
                                            <select class="select2_single form-control" tabindex="-1" name="frmproducts[]" required="required">
                                                <option value="">Seleccione</option>
                                                <?php
                                                $select = new CatalogController();
                                                if (!empty($data)) {
                                                    $select->selectTypeProduct($data['producto_id_tipo_producto']);
                                                } else {
                                                    $select->selectTypeProduct();
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label>Código: <span class="required">*</span></label>
                                            <input type="text" class="form-control mayus" name="frmproducts[]" required="required" <?php if (!empty($data)) {echo 'value="' . $data["producto_codigo"] . '" readonly="readonly"';}?>>
                                        </div>
                                        
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label>Descripción: <span class="required">*</span></label>
                                            <input type="text" class="form-control mayus" name="frmproducts[]" required="required"<?php if (!empty($data)) {echo 'value="' . $data["producto_descripcion"] . '"';}?>>
                                        </div>                        
                                    </div>
                                
                                    <div class="form-group">   
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label>Unidad de medida: <span class="required">*</span></label>
                                            <select class="select2_single form-control" tabindex="-1" name="frmproducts[]" required="required">
                                                <option value="">Seleccione</option>
                                                <?php                                                
                                                if (!empty($data)) {
                                                    $select->selectUnit($data['producto_id_unidad_medida']);
                                                } else {
                                                    $select->selectUnit();
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label>Precio unitario: <span class="required">*</span></label>
                                            <input type="number" class="form-control" name="frmproducts[]" required="required" <?php if (!empty($data)) {echo 'value="' . $data["producto_precio_unitario"] . '"';}?>>
                                        </div>
                                    </div>
                            </div>
                        </div>

                        <!-- Impuestos -->
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Impuestos</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>

                            <div class="x_content">
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>Tipo de Impuesto: <span class="required">*</span></label>
                                        <select class="select2_single form-control" tabindex="-1" name="frmproducts[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php
                                                if (!empty($data)) {
                                                    $select->selectTax($data['producto_id_tipo_impuesto']);
                                                } else {
                                                    $select->selectTax();
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>Tipo de IVA: <span class="required">*</span></label>
                                        <select class="select2_single form-control" tabindex="-1" name="frmproducts[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php
                                                if (!empty($data)) {
                                                    $select->selectTypeTax($data['producto_id_porc_impuesto']);
                                                } else {
                                                    $select->selectTypeTax();
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        if (!empty($data)) {
                            echo ' <input type="hidden" name="frmproducts[]" value="' . $data["producto_id"] . '">';
                        }
                        ?>
                     
                        <div class="box-center" id="notify"></div>    
                        
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <a class="btn btn-danger" href="<?php echo SERVERURL; ?>catalog/listproducts"> Cancelar</a>
                            <?php 
                            if (empty($data)) {
                               echo '<button type="reset" class="btn btn-primary">Reiniciar</button>';
                            }?>
                            <button type="submit" class="btn btn-success">Guardar</button>
                            </div>
                        </div>

                    </form>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
</div>
