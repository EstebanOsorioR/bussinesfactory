<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Detalle <small>producto: <?php echo $data["producto_descripcion"];?> </small>
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <a class="btn btn-primary" href="<?php echo SERVERURL; ?>catalog/listproducts"> <i class="fa fa-arrow-left"></i>  &nbsp; Regresar</a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <!-- content starts here -->
                <table class="table table-bordered">
                    <tbody>
                        <?php
                        // var_dump($data);exit();
                        if (!empty($data)) {

                            // var_dump($data);exit();
                           
                                // var_dump($value);exit();
                        echo '  <tr>
                                    <th scope="row">Tipo</th>
                                    <td>'.$data["tipproducto_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Código</th>
                                    <td>'.$data["producto_codigo"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Descripción</th>
                                    <td>'.$data["producto_descripcion"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Unidad</th>
                                    <td>'.$data["unimed_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Precio unitario	</th>
                                    <td>'.$data["producto_precio_unitario"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Tipo de impuesto</th>
                                    <td>'.$data["impuesto_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Porcentaje de Impuesto</th>
                                    <td>'.$data["porimp_nombre"].'</td>
                                </tr>';
                            
                        }
                        ?>
                    </tbody>
                </table>
                <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
</div>
