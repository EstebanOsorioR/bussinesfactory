<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Generar <small>documento </small></h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <!-- content starts here -->
                    <form class="form-horizontal" id="form" data-parsley-validate method="post" name="frmvoucher[]">
                        <!-- DATOS DE COMPROBANTE -->
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>DATOS DE COMPROBANTE</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>Tipo de Documento<span class="required">*</span></label>
                                        <select class="select2_single form-control" id="tipodocumento" tabindex="-1" name="frmvoucherdata[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php
                                                $select = new IssueController();                                                
                                                $select->selectTypeDocument(); ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>Establecimiento <span class="required">*</span></label>
                                        <select class="select2_single form-control" tabindex="-1" name="frmvoucherdata[]" required="required">
                                            <option value="">Seleccione</option>
                                            <option value="1">PRINCIPAL</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>Secuencial <span class="required">*</span></label>
                                        <select class="select2_single form-control" id="secuencial"  name="frmvoucherdata[]" tabindex="-1" required="required"> </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label>Fecha Vencimiento <span class="required">*</span></label>

                                        <div class="col-md-12 form-group">
                                            <input type="text" class="form-control has-feedback-right" id="fechavencimiento" name="frmvoucherdata[]" placeholder="First Name" aria-describedby="inputSuccess2Status3" />
                                            <span class="fa fa-calendar-o form-control-feedback right" aria-hidden="true"></span>
                                            <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>Cantidad Decimal <span class="required">*</span></label>
                                        <select class="select2_single form-control" tabindex="-1" name="frmvoucherdata[]" required="required">
                                            <option value="">Seleccione</option>
                                            <option selected value="2">2</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>Tipo de Oparación<span class="required">*</span></label>
                                        <select class="select2_single form-control" tabindex="-1" name="frmvoucherdata[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php                                             
                                                $select->selectTypeOperation(); ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- CLIENTE -->
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>CLIENTE</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>Cliente<span class="required">*</span></label>
                                        <select class="select2_single form-control" id="cliente" tabindex="-1" name="frmclient[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php                                                                                       
                                            $select->selectClient(); ?>
                                        </select>
                                    </div>
                                    <div id="clientedata"></div>
                                </div>
                            </div>
                        </div>
                        <!-- CONCEPTOS -->
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>CONCEPTOS</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>Concepto</label>
                                        <select class="select2_single form-control" id="concepto" tabindex="-1">
                                            <option value="">Seleccione</option>
                                            <?php                                             
                                                $select->selectConcept(); ?>
                                        </select>
                                    </div>
                                    <input type="hidden" id="tipo_producto">
                                    <input type="hidden" id="descripcion">
                                    <input type="hidden" id="product_id">

                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label>Código</label>
                                        <input type="text" class="form-control" id="codigo" readonly="readonly"/>
                                    </div>

                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <label>Unidad de medida</label>
                                        <input type="text" class="form-control" id="unidad" readonly="readonly"/>
                                    </div>
                                    <input type="hidden" id="codigo_unidad">
                                    <div class="col-md-3 col-sm-3 col-xs-12 pull-right">
                                        <label> &nbsp; </label>
                                        <br />
                                        <button type="button" class="btn btn-block btn-primary agregar_seriales" id="agregar_seriales" disabled>Agregar Seriales</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-1 col-sm-1 col-xs-12">
                                        <label>Cantidad <span class="required"></span></label>
                                        <input type="text" class="form-control" id="cantidad" />
                                    </div>

                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label>Precio Unitario <span class="required"></span></label>
                                        <input type="text" class="form-control" id="precio"  />
                                    </div>

                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label>Impuesto</label>
                                        <select class="select2_single form-control" id="impuesto" tabindex="-1" >
                                            <option value="">Seleccione</option>
                                            <?php 
                                         $select->catalog->selectTax(); ?>
                                        </select>
                                    </div>

                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label>IVA</label>
                                        <select class="select2_single form-control" id="tipoiva" tabindex="-1" >
                                            <option value="">Seleccione</option>
                                            <?php
                                          $select->catalog->selectTypeTax(); ?>
                                        </select>
                                    </div>
                                    <input type="hidden" id="porcentaje_iva" value="0">

                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label>Tipo de Descuento</label>
                                        <select class="select2_single form-control" tabindex="-1" id="tipo_descuento" >
                                            <option value="">Seleccione</option>
                                            <?php
                                           $select->selectTypeDiscount(); ?>
                                        </select>
                                    </div>

                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label>Descuento</label>
                                        <input type="text" id="descuento" class="form-control" />
                                    </div>

                                    <div class="col-md-1 col-sm-1 col-xs-12 pull-right">
                                        <label> &nbsp; </label>
                                        <br />
                                        <button type="button" class="btn btn-block btn-primary agregar_concepto" id="agregar_concepto" disabled><i class="fa fa-plus" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-block btn-success editar_concepto" style="display:none;" id="editar_concepto" ><i class="fa fa-arrow-down" aria-hidden="true"></i></button>
                                        <!-- <a class="btn btn-primary" id="agregar_concepto"> <i class="fa fa-plus"></i></a> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="table-responsive table-data">
                                        <table id="itemsconceptos" width="100%" class="table table-striped bt_style" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th width="3%">#</th>
                                                    <th width="5%">Código</th>
                                                    <th width="5%">Cant.</th>
                                                    <th width="6%">Medida</th>
                                                    <th width="16%">Descripción</th>
                                                    <th width="10%">Valor Unitario</th>
                                                    <th width="9%">% Impuesto</th>
                                                    <th width="9%">IVA</th>
                                                    <th width="9%">Descuento</th>
                                                    <th width="10%">Valor Total</th>
                                                    <th width="12%" style="text-align: center;">Acciones</th>
                                                </tr>
                                            </thead>

                                            <tbody id="detalle-item"></tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class=" pull-right">
                                            <span > <b> Subtotal:  &nbsp;</b> <span class="badge badge-success" id="subtotal">$ 0.00</span> &nbsp;</span>
                                            <input type="hidden" id="input_subtotal" name="frmtotales[]">
                                            <span > <b> Descuento:  &nbsp;</b> <span class="badge badge-success" id="descuento_total">$ 0.00</span> &nbsp;</span>
                                            <input type="hidden" id="input_descuento_total" name="frmtotales[]">
                                            <span > <b> Impuesto:  &nbsp;</b> <span class="badge badge-success" id="ivas">$ 0.00</span> &nbsp;</span>
                                            <input type="hidden" id="input_ivas" name="frmtotales[]">
                                            <span > <b> Total:</b>  &nbsp;<span class="badge badge-success" id="total_total">$ 0.00</span> &nbsp;</span>
                                            <input type="hidden" id="input_total_total" name="frmtotales[]">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class=" pull-right">
                                            <!-- <span > <b> IVA 19%:  &nbsp;</b> <span class="badge badge-success" id="porcentaje_19">$ 0.00</span> &nbsp;</span> -->
                                            <input type="hidden" value="19" name="frmivas[]">
                                            <input type="hidden" id="input_porcentaje_19" name="frmivas[]">
                                            <!-- <span > <b> IVA 16%:  &nbsp;</b> <span class="badge badge-success" id="porcentaje_16">$ 0.00</span> &nbsp;</span> -->
                                            <input type="hidden" value="16" name="frmivas[]">
                                            <input type="hidden" id="input_porcentaje_16" name="frmivas[]">
                                            <!-- <span > <b> IVA 5%:  &nbsp;</b> <span class="badge badge-success" id="porcentaje_5">$ 0.00</span> &nbsp;</span> -->
                                            <input type="hidden" value="5" name="frmivas[]">
                                            <input type="hidden" id="input_porcentaje_5" name="frmivas[]">
                                            <input type="hidden" value="0" name="frmivas[]">
                                            <!-- <span > <b> Excluido :  &nbsp;</b> <span class="badge badge-success" id="porcentaje_excluido">$ 0.00</span> &nbsp;</span> -->
                                            <input type="hidden" id="input_porcentaje_excluido" name="frmivas[]">
                                            <input type="hidden" id="input_subtotal_gobal" name="frmivas[]">
                                            <input type="hidden" id="input_ivas_totales" name="frmivas[]">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- MEDIO DE PAGOS -->
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>MEDIO DE PAGOS</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>Metodo de Pagos <span class="required">*</span></label>
                                        <select class="select2_single form-control" tabindex="-1" name="frmothers[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php                                                                                       
                                            $select->selectPaymentMethod(); ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>Medio de Pagos <span class="required">*</span></label>
                                        <select class="select2_single form-control" tabindex="-1" name="frmothers[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php                                                                                       
                                            $select->selectPaymentMeans(); ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>Referencia del pago <span class="required">*</span></label>
                                        <input type="text" class="form-control" name="frmothers[]" required="required" />                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- INFORMACIÓN ADICIONAL -->
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>INFORMACIÓN ADICIONAL</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea name="frmothers[]" rows="8" style="min-width: 100%" ></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
        
                        <div class="box-center" id="notify"></div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                <a class="btn btn-danger" href="<?php echo SERVERURL; ?>issue/listvoucher"> Cancelar</a>
                                <button type="submit" class="btn btn-success">Guardar</button>
                            </div>
                        </div>
                    </form>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
</div>
