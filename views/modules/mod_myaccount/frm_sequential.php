<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Agregar <small>numeración secuencial </small></h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <a class="btn btn-primary" href="<?php echo SERVERURL; ?>myaccount/account"> <i class="fa fa-arrow-left"></i> &nbsp; Regresar</a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal" id="form" data-parsley-validate method="post" name="frmsequential[]">
                    <?php
                        if (empty($const)) {//si $data viene vacia
                            echo '<input type="hidden" name="frmsequential[]" value="0">';
                        } else {
                            echo '<input type="hidden" name="frmsequential[]" value="1">';
                        }
                    ?>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label>Tipo de Ambiente <span class="required">*</span></label>
                                <select class="select2_single form-control" tabindex="-1" name="frmsequential[]" required="required">
                                    <option value="">Seleccione</option>
                                    <?php
                                        $select = new MyAccountController();                                                
                                        if (!empty($const)) {
                                            $select->selectTypeEnvironment($const['secuencial_id_ambiente']); 
                                        } else {
                                            $select->selectTypeEnvironment(); 
                                        } 
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label>Tipo de documento <span class="required">*</span></label>
                                <select class="select2_single form-control" tabindex="-1" name="frmsequential[]" required="required">
                                    <option value="">Seleccione</option>
                                    <?php                                                                             
                                        if (!empty($const)) {
                                            $select->selectTypeDocument($const['secuencial_id_tip_documento']); 
                                        } else {
                                            $select->selectTypeDocument(); 
                                        } 
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label>Tipo de Operación: <span class="required">*</span></label>
                                <select class="select2_single form-control" tabindex="-1" name="frmsequential[]" required="required">
                                    <option value="">Seleccione</option>
                                    <?php                                                                             
                                        if (!empty($const)) {
                                            $select->selectTypeOperation($const['secuencial_id_tip_operacion']); 
                                        } else {
                                            $select->selectTypeOperation(); 
                                        } 
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label>Modalidad de uso para secuencial: <span class="required">*</span></label>
                                <select class="select2_single form-control" tabindex="-1" name="frmsequential[]" required="required">
                                    <option value="">Seleccione</option>
                                    <?php                                                                             
                                        if (!empty($const)) {
                                            $select->selectModality($const['secuencial_id_modalidad']); 
                                        } else {
                                            $select->selectModality(); 
                                        } 
                                    ?>
                                </select>
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label>Establecimiento: <span class="required">*</span></label>
                                <select class="select2_single form-control" tabindex="-1" name="frmsequential[]" required="required">
                                <option value="">Seleccione</option>    
                                <option selected value="1">PRINCIPAL</option>
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label>Número de resolución <span class="required">*</span></label>
                                <input type="number" class="form-control" data-parsley-length="[10, 12]" name="frmsequential[]" required="required"
                                <?php if (!empty($const)) {echo 'value="' . $const["secuencial_numero_resolucion"] . '"';}?> />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label>Prefijo <span class="required">*</span></label>
                                <input type="text" class="form-control mayus" data-parsley-length="[1, 4]" name="frmsequential[]" required="required" 
                                <?php if (!empty($const)) {echo 'value="' . $const["secuencial_prefijo"] . '"';}?> />
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label>Valor del rango desde <span class="required">*</span></label>
                                <input type="number" class="form-control" data-parsley-length="[1, 9]" name="frmsequential[]" required="required"
                                <?php if (!empty($const)) {echo 'value="' . $const["secuencial_rango_desde"] . '"';}?> />
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label>Valor del rango hasta <span class="required">*</span></label>
                                <input type="number" class="form-control" data-parsley-length="[1, 9]" name="frmsequential[]" required="required" 
                                <?php if (!empty($const)) {echo 'value="' . $const["secuencial_rango_hasta"] . '"';}?> />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label>Valor del rango inicio <span class="required">*</span></label>
                                <input type="number" class="form-control" data-parsley-length="[1, 9] " name="frmsequential[]" required="required"
                                <?php if (!empty($const)) {echo 'value="' . $const["secuencial_rango_inicial"] . '"';}?> />
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label>Fecha inicio <span class="required">*</span></label>
                               
                                    <div class="col-md-12  form-group">
                                        <input type="text" class="form-control has-feedback-right" id="fechainicio" name="frmsequential[]" placeholder="First Name" aria-describedby="inputSuccess2Status3"
                                        <?php 
                                        if (!empty($const)) {
                                            $fechainicio = new DateTime( $const["secuencial_fecha_inicio"]); 
                                            $fechainicio =date_format($fechainicio, 'd-m-Y');
                                            echo 'value="' .  $fechainicio . '"';
                                        }
                                        ?> />
                                        <span class="fa fa-calendar-o form-control-feedback right" aria-hidden="true"></span>
                                        <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                                    </div>
                            
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label>Fecha fin <span class="required">*</span></label>
                                <div class="controls">
                                    <div class="col-md-12 form-group">
                                        <input type="text" class="form-control has-feedback-right" id="fechafin" name="frmsequential[]" placeholder="First Name" aria-describedby="inputSuccess2Status3"
                                        <?php 
                                        if (!empty($const)) {
                                            $fechainicio = new DateTime( $const["secuencial_fecha_fin"]); 
                                            $fechainicio =date_format($fechainicio, 'd-m-Y');
                                            echo 'value="' .  $fechainicio . '"';
                                        }
                                        ?>
                                         />
                                        <span class="fa fa-calendar-o form-control-feedback right" aria-hidden="true"></span>
                                        <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label>Clave técnica <span class="required">*</span></label>
                                <input type="text" class="form-control" name="frmsequential[]" required="required"
                                <?php if (!empty($const)) {echo 'value="' . $const["secuencial_clavedian"] . '"';}?> />
                            </div>
                        </div>
                        <?php
                        if (!empty($const)) {
                            echo ' <input type="hidden" name="frmsequential[]" value="' . $const["secuencial_id"] . '">';
                        }
                        ?>

                        <div class="box-center" id="notify"></div>    
                        
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <a class="btn btn-danger" href="<?php echo SERVERURL; ?>myaccount/account"> Cancelar</a>
                            <?php 
                            if (empty($const)) {
                               echo '<button type="reset" class="btn btn-primary">Reiniciar</button>';
                            }?>
                            <button type="submit" class="btn btn-success">Guardar</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
