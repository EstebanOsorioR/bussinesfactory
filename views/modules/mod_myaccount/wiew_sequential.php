<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Detalle <small>numeración secuencial: <?php echo $data["secuencial_prefijo"].' '. $data["secuencial_rango_desde"].'-'.$data["secuencial_rango_hasta"];?> </small>
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <a class="btn btn-primary" href="<?php echo SERVERURL; ?>myaccount/account"> <i class="fa fa-arrow-left"></i>  &nbsp; Regresar</a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <!-- content starts here -->
                <table class="table table-bordered">
                    <tbody>
                        <?php
                        // var_dump($data);exit();
                        if (!empty($data)) {

                            // var_dump($data);exit();
                           
                                // var_dump($value);exit();
                        echo '  <tr>
                                    <th scope="row">Tipo de documento</th>
                                    <td>'.$data["tipo_documento_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Número de resolución</th>
                                    <td>'.$data["secuencial_numero_resolucion"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Prefijo</th>
                                    <td>'.$data["secuencial_prefijo"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Clave técnica</th>
                                    <td>'.$data["secuencial_clavedian"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Rango de Secuenciales</th>
                                    <td>'.$data["secuencial_rango_desde"].'-'.$data["secuencial_rango_hasta"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Valor del rango inicial</th>
                                    <td>'.$data["secuencial_rango_inicial"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Fecha de vigencia</th>
                                    <td>'.$data["secuencial_fecha_inicio"].' / '.$data["secuencial_fecha_inicio"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Modalidad de uso para secuencial</th>
                                    <td>'.$data["modalidad_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Establecimiento</th>
                                    <td>'.$data["establecimiento_nombre"].'</td>
                                </tr>
                                <tr>
                                    <th scope="row">Ambiente</th>
                                    <td>'.$data["ambiente_nombre"].'</td>
                                </tr>';
                            
                        }
                        ?>
                    </tbody>
                </table>
                <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
</div>
