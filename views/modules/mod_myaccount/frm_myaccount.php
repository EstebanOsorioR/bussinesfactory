<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Actualizar <small>datos fiscales </small></h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <?php
                if (!empty($const)) {
                    echo '  <div class="x_title">
                                <a class="btn btn-primary" href=" '.SERVERURL.'myaccount/account"> <i class="fa fa-arrow-left"></i> &nbsp; Regresar</a>
                                <div class="clearfix"></div>
                            </div>';
                }
                ?>
                <div class="x_content">
                    <!-- content starts here -->
                    <form class="form-horizontal" id="form" data-parsley-validate method="post" name="frmmyaccount[]">
                        <!-- DATOS DEL CONTRIBUYENTE -->
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>DATOS DEL CONTRIBUYENTE</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <label>Régimen de contribuyente <span class="required">*</span></label>
                                        <select class="select2_single form-control" tabindex="-1" name="frmtaxpayer[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php
                                                $select = new MyAccountController();                                                
                                                if (!empty($const)) {
                                                    $select->selecttaxpayerRegime($const['emisor_id_tipo_persona']);
                                                }else{ 
                                                    $select->selecttaxpayerRegime(); 
                                                 } 
                                                ?>
                                        </select>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <label>Tipo de régimen <span class="required">*</span></label>
                                        <select class="select2_single form-control" tabindex="-1" name="frmtaxpayer[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php        
                                                if (!empty($const)) {
                                                    $select->selectRegimeType($const['emisor_id_regimen_fiscal']); 
                                                } else { 
                                                    $select->selectRegimeType(); 
                                                } 
                                                ?>
                                        </select>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <label>Tipo de Identificación <span class="required">*</span></label>
                                        <select class="select2_single form-control" tabindex="-1" name="frmtaxpayer[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php
                                                if (!empty($const)) {
                                                    $select->selectIdentificationType($const['emisor_tipoiden_id']); 
                                                } else { 
                                                    $select->selectIdentificationType(); 
                                                } 
                                            ?>
                                        </select>
                                    </div>

                                    <div class="col-md-2 col-sm-11 col-xs-11">
                                        <label>Nro. de Identificación <span class="required">*</span></label>
                                        <input type="number" id="nit" class="form-control" name="frmtaxpayer[]" required="required"
                                        <?php if (!empty($const)) {echo 'value="' . $const["emisor_numero_identificacion"] . '"';}?>>
                                    </div>

                                    <div class="col-md-1 col-sm-1 col-xs-1">
                                        <label>D.V.<span class="required"></span></label>
                                        <input type="number" id="digitoVerificacion" class="form-control" name="frmtaxpayer[]" readonly = "readonly" required="required"
                                        <?php if (!empty($const)) {echo 'value="' . $const["emisor_digito_verificacion"] . '"';}?>>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <label>Razón social <span class="required">*</span></label>
                                        <input type="text" class="form-control" name="frmtaxpayer[]" required="required"
                                        <?php if (!empty($const)) {echo 'value="' . $const["emisor_razon_social"] . '"';}?>>
                                    </div>

                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <label>Nombre comercial <span class="required">*</span></label>
                                        <input type="text" class="form-control" name="frmtaxpayer[]" required="required"
                                        <?php if (!empty($const)) {echo 'value="' . $const["emisor_nombre_comercial"] . '"';}?>>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- DOMICILIO FISCAL -->
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>DOMICILIO FISCAL</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>

                            <div class="x_content">
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>País <span class="required">*</span></label>
                                        <select class="select2_single form-control" tabindex="-1" name="frmaddress[]" required="required">
                                            <?php 
                                            if (!empty($const)) {
                                                $select->selectCountry($const['emisor_id_pais']); 
                                            }else { 
                                                $select->selectCountry(); 
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>Departamento <span class="required">*</span></label>
                                        <select class="select2_single form-control" id="departament" tabindex="-1" name="frmaddress[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php 
                                            if (!empty($const)) {
                                                $select->selectDepartment($const['emisor_id_departamento']); 
                                            }else { 
                                                $select->selectDepartment(); 
                                            } 
                                            ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label>Ciudad <span class="required">*</span></label>
                                        <select class="select2_single form-control" id="city" tabindex="-1" name="frmaddress[]" required="required">
                                            <option value="">Seleccione</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-9 col-sm-12 col-xs-12">
                                        <label>Dirección <span class="required">*</span></label>
                                        <input type="text" class="form-control mayus" name="frmaddress[]" required="required"
                                        <?php if (!empty($const)) {echo 'value="' . $const["emisor_direccion"] . '"';}?>>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <label>Zona postal <span class="required">*</span></label>
                                        <input type="number" class="form-control" name="frmaddress[]" required="required" data-parsley-minlength="6" data-parsley-maxlength="6"
                                        <?php if (!empty($const)) {echo 'value="' . $const["emisor_codigo_postal"] . '"';}?>>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- INFORMACIÓN ADICIONAL -->
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>INFORMACIÓN ADICIONAL</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>

                            <div class="x_content">
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label>Actividad Económica <span class="required">*</span></label>
                                        <select class="form-control" tabindex="-1" name="frmadditional[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php 
                                            if (!empty($const)) {
                                                $select->selectEconomicActivity($const['emisor_actividad_economica']); 
                                            } else { 
                                                $select->selectEconomicActivity(); 
                                            } 
                                            ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label>Responsabilidades <span class="required">*</span></label>
                                        <select class="form-control" tabindex="-1" name="frmadditional[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php 
                                            if (!empty($const)) {
                                                $select->selectResponsibility($const['emisor_id_respons_fiscal']); 
                                            } else { 
                                                $select->selectResponsibility(); 
                                            } 
                                                ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label>Impuestos: <span class="required">*</span></label>
                                        <select class="select2_single form-control" tabindex="-1" name="frmadditional[]" required="required">
                                            <option value="">Seleccione</option>
                                            <?php 
                                            if (!empty($const)) {
                                                $select->selectTaxesDIAN($const['emisor_id_detalle_tributario']); 
                                            } else { 
                                                $select->selectTaxesDIAN();
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- INFORMACIÓN DE CONTACTO -->
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>INFORMACIÓN DE CONTACTO</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>

                            <div class="x_content">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <label>Teléfono <span class="required">*</span></label>
                                        <input type="number" class="form-control" name="frmcontact[]" data-parsley-minlength="8" data-parsley-maxlength="8"
                                        <?php if (!empty($const)) {echo 'value="' . $const["emisor_telefono"] . '"';}?>>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <label>Email <span class="required">*</span></label>
                                        <input type="email" class="form-control" name="frmcontact[]"
                                        <?php if (!empty($const)) {echo 'value="' . $const["emisor_email_recepcion"] . '"';}?>>
                                    </div>

                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <label>Nota <span class="required">*</span></label>
                                        <input type="text" class="form-control" name="frmcontact[]"
                                        <?php if (!empty($const)) {echo 'value="' . $const["emisor_nota"] . '"';}?>>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-center" id="notify"></div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                <?php
                                    if (!empty($const)&&$const["emisor_numero_identificacion"] != 0) {
                                    echo  '<a class="btn btn-danger" href="'. SERVERURL.'myaccount/account"> Cancelar</a>';
                                }
                                ?>
                                <button type="submit" class="btn btn-success">Guardar</button>
                            </div>
                        </div>
                    </form>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
</div>
