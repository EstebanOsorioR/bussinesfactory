<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
            Cambiar  <small>Contraseña </small>
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <!-- content starts here -->
                    <div class="box-center">
                      <div class="col-md-6 col-sm-10 col-xs-12 ">
                        <form class="form-horizontal" id="form" data-parsley-validate method="post" name="frmpassword[]">
                          <div class="item form-group">
                          <label>Contraseña actual: <span class="required">*</span></label>
                            <div>
                              <input class="form-control"  name="frmpassword[]" required="required" type="password"   data-parsley-required-message="Ingrese su contraseña actual."> 
                            </div>
                          </div>
                          <div class="item form-group">
                          <label>Nueva contraseña: <span class="required">*</span></label>
                            <div>
                              <input class="form-control" id="pass" name="frmpassword[]" required="required" type="password" 
                                data-parsley-length="[8, 20]"
                                data-parsley-pattern="^[a-zA-Z0-9+*/%*$-_@]+$"
                                data-parsley-required-message="Ingrese su nueva contraseña."
                                data-parsley-uppercase="1"
                                data-parsley-lowercase="1"
                                data-parsley-number="1"    
                                data-parsley-special="1">
                            </div>
                          </div>
                          <div class="item form-group">
                          <label>Confirmar nueva contraseña: <span class="required">*</span></label>
                            <div>
                              <input class="form-control" data-parsley-equalto="#pass"  name="frmpassword[]" required="required" type="password"
                              data-parsley-length="[8, 20]"
                              data-parsley-required-message="Vuelva a ingresar su nueva contraseña."
                              data-parsley-equalto="#pass"
                              data-parsley-equalto-message="Las contraseñas no coninciden.">
                            </div>
                          </div>
             
                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-6 col-md-offset-3">
                            <button id="send" type="submit" class="btn btn-primary">Cambiar contraseña</button>
                          </div>
                          </div>
                    </form>
                    </div>
                    </div>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
</div>
