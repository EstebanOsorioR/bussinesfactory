<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Datos <small>Fiscales </small></h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <!-- start tabpanel -->
                <div class="x_content">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Fiscales</a></li>
                            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Folios</a></li>
                            <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Secuenciales</a></li>
                            <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Certificado</a></li>
                            <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Logotipo</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <a class="btn btn-primary" href="<?php echo SERVERURL; ?>myaccount/frmaccount"><i class="fa fa-pencil"></i> &nbsp; Editar datos fiscales</a>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <div>
                                          <h4>DATOS DEL CONTRIBUYENTE</h4>

                                          <div class="x_content">
                                              <table class="table table-hover">
                                                  <tbody>
                                                      <tr>
                                                          <th scope="row">Régimen de contribuyente</th>
                                                          <td><?php if (!empty($const)&&!empty($const[0]["tipoper_nombre"])) {echo $const[0]["tipoper_nombre"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                      <tr>
                                                          <th scope="row">Tipo de régimen</th>
                                                          <td><?php if (!empty($const)&&!empty($const[0]["regfiscal_nombre"])) {echo $const[0]["regfiscal_nombre"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                      <tr>
                                                          <th scope="row">Tipo de Identificación</th>
                                                          <td><?php if (!empty($const&&!empty($const[0]["tipoiden_nombre"]))) {echo $const[0]["tipoiden_nombre"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                      <tr>
                                                          <th scope="row">Número de Identificación</th>
                                                          <td><?php if (!empty($const)&&!empty($const[0]["emisor_numero_identificacion"])) {echo $const[0]["emisor_numero_identificacion"]."-".$const[0]["emisor_digito_verificacion"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                      <tr>
                                                          <th scope="row">Razón social / Nombre Completo</th>
                                                          <td><?php if (!empty($const)&&!empty($const[0]["emisor_razon_social"])) {echo $const[0]["emisor_razon_social"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                      <tr>
                                                          <th scope="row">Nombre comercial</th>
                                                          <td><?php if (!empty($const)&&!empty($const[0]["emisor_nombre_comercial"])) {echo $const[0]["emisor_nombre_comercial"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                      <tr>
                                                          <th scope="row">Token Empresa</th>
                                                          <td><?php if (!empty($const)&&!empty($const[0]["emisor_token_empresa"])) {echo $const[0]["emisor_token_empresa"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                      <tr>
                                                          <th scope="row">Token Contraseña</th>
                                                          <td><?php if (!empty($const)&&!empty($const[0]["emisor_token_password"])) {echo $const[0]["emisor_token_password"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <div>
                                          <h4>DOMICILIO FISCAL</h4>

                                          <div class="x_content">
                                              <table class="table table-hover">
                                                  <tbody>
                                                      <tr>
                                                          <th scope="row">País</th>
                                                          <td><?php if (!empty($const)&&!empty($const[0]["pais_nombre"])) {echo $const[0]["pais_nombre"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                      <tr>
                                                          <th scope="row">Departamento</th>
                                                          <td><?php if (!empty($const)&&!empty($const[0]["departamento_nombre"])) {echo ucfirst(strtolower($const[0]["departamento_nombre"]));}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                      <tr>
                                                          <th scope="row">Ciudad</th>
                                                          <td><?php if (!empty($const)&&!empty($const[0]["ciudad_nombre"])) {echo ucfirst(strtolower($const[0]["ciudad_nombre"]));}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                      <tr>
                                                          <th scope="row">Dirección</th>
                                                          <td><?php if (!empty($const)&&!empty($const[0]["emisor_direccion"])) {echo $const[0]["emisor_direccion"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                      <tr>
                                                          <th scope="row">Código Postal</th>
                                                          <td><?php if (!empty($const)&&!empty($const[0]["emisor_codigo_postal"])) {echo $const[0]["emisor_codigo_postal"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                      </tr>
                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                       
                              
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div>
                                              <h4>INFORMACIÓN ADICIONAL</h4>

                                              <div class="x_content">
                                                  <table class="table table-hover">
                                                      <tbody>
                                                          <tr>
                                                              <th scope="row">Actividad Económica</th>
                                                              <td>  
                                                              <ul>
                                                                    <li><?php if (!empty($const)&&!empty($const[0]["actividad_codigo"])) {echo $const[0]["actividad_codigo"]." | ".$const[0]["actividad_nombre"];}else{echo '<i>- Sin Asignar -</i>';}?></li>
                                                                </ul>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <th scope="row">Responsabilidades Fiscales</th>
                                                              <td>  <ul>
                                                                    <li><?php if (!empty($const)&&!empty($const[0]["responsfiscal_codigo"])) {echo $const[0]["responsfiscal_codigo"]." | ".$const[0]["responsfiscal_nombre"];}else{echo '<i>- Sin Asignar -</i>';}?></li>
                                                                </ul></td>
                                                          </tr>
                                                          <tr>
                                                              <th scope="row">Impuestos</th>
                                                              <td>  <ul>
                                                                    <li><?php if (!empty($const)&&!empty($const[0]["tributofiscal_nombre"])) {echo $const[0]["tributofiscal_nombre"];}else{echo '<i>- Sin Asignar -</i>';}?></li>
                                                                </ul></td>
                                                          </tr>
                                                      </tbody>
                                                  </table>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <div>
                                              <h4>INFORMACIÓN DE CONTACTO</h4>

                                              <div class="x_content">
                                                  <table class="table table-hover">
                                                      <tbody>
                                                          <tr>
                                                              <th scope="row">Teléfono</th>
                                                              <td><?php if (!empty($const)&&!empty($const[0]["emisor_telefono"])) {echo $const[0]["emisor_telefono"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                          </tr>
                                                          <tr>
                                                              <th scope="row">Email</th>
                                                              <td><?php if (!empty($const)&&!empty($const[0]["emisor_email_recepcion"])) {echo $const[0]["emisor_email_recepcion"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                          </tr>
                                                          <tr>
                                                              <th scope="row">Nota</th>
                                                              <td><?php if (!empty($const)&&!empty($const[0]["emisor_nota"])) {echo $const[0]["emisor_nota"];}else{echo '<i>- Sin Asignar -</i>';}?></td>
                                                          </tr>
                                                      </tbody>
                                                  </table>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                          
                              </div>
                            
                            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                    <a class="btn btn-primary" href="<?php echo SERVERURL; ?>myaccount/account"><i class="fa fa-arrow-left"></i> &nbsp; Regresar</a>
                                </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <table id="tablefolios" class="table table-bordered table-hover dt-responsive" cellspacing="0" width="100%">
                                  <thead>
                                      <tr>
                                          <th>Fecha de creación</th>
                                          <th>Cantidad de folios</th>
                                          <th>Estatus</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  <tr>
                                          <th>05/06/2021</th>
                                          <th>1000</th>
                                          <th><span class="label label-primary">Activo</span></th>
                                      </tr>
                                  </tbody>
                              </table>
                            </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                    <a class="btn btn-primary" href="<?php echo SERVERURL; ?>myaccount/account"><i class="fa fa-arrow-left"></i> &nbsp; Regresar</a>
                                    <a class="btn btn-primary" href="<?php echo SERVERURL; ?>myaccount/frmsequential"><i class="fa fa-plus-circle"></i> &nbsp; Agregar secuancial</a>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <table id="tablesequential" class="table table-bordered table-hover dt-responsive" cellspacing="0" width="100%">
                                  <thead>
                                      <tr>
                                          <th>Documentos</th>
                                          <th>Establecimientos</th>
                                          <th>Prefijo (Rango)</th>
                                          <th>Fecha desde</th>
                                          <th>Fecha hasta</th>
                                          <th>Estatus</th>
                                          <th>Ambiente</th>
                                          <th>Acciones</th>
                                          
                                      </tr>
                                  </thead>
                              </table>
                            </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <a class="btn btn-primary" href="<?php echo SERVERURL; ?>myaccount/account"><i class="fa fa-arrow-left"></i> &nbsp; Regresar</a>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="box-center">
                                  <div class="thumbnail" style="width: 400px; height: 150px;">
                                      <div style="text-align: center; line-height: 30px;">
                                          <b>Número de identificación: </b> <?php if (!empty($const&&!empty($const[0]["emisor_numero_identificacion"]))) {echo $const[0]["emisor_numero_identificacion"];}else{echo '<i>- Sin Asignar -</i>';}?> <br />
                                          <b>No. de Serie:</b> 933032770290023987 <br />
                                          <b>Válido desde:</b> 25-04-2021 12:49:00 <br />
                                          <b>Válido hasta:</b> 24-04-2022 12:49:00 <br />
                                      </div>
                                  </div>
                              </div>
                          </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                      <a class="btn btn-primary" href="<?php echo SERVERURL; ?>myaccount/account"><i class="fa fa-arrow-left"></i> &nbsp; Regresar</a>
                              </div>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="box-center">
                                  <div>
                                      <div style="text-align: center; line-height: 30px;">
                                          <div class="thumbnail" style="width: 400px; height: 150px;">
                                            <h1>Logotipo</h1>
                                          </div>
                                          <div>
                                            <a class="btn btn-default" href="#"><i class="fa fa-search"></i> </a>
                                            <a class="btn btn-primary" href="#"><i class="fa fa-picture-o"></i> &nbsp; Selecionar logo</a>
                                            <a class="btn btn-default" href="#"><i class="fa fa-floppy-o"></i> &nbsp; Guardar logo</a>

                                          </div>
                                          <b>Formatos permitidos:</b> JPG,PNG. <br />
                                          <b>Tamaño máximo:</b> 600KB <br />
                                          <b>Máximo de ancho de la imagen:</b> 800px <br />
                                      </div>
                                  </div>
                              </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end tabpanel -->
            </div>
        </div>
    </div>
</div>
