<body class="login">
    <div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="post" name="frmpass[]">
              <h1>Restablecer Contraseña</h1>
              <div>
              <input type="hidden" name="frmpass[]" <?php echo 'value="'.base64_decode($data[0]."=").'"';  ?> required="">
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Contraseña" name="frmpass[]" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Confirmar contraseña" name="frmpass[]" required="" />
              </div>
              <div class= "g-recaptcha-center">
                <?php if ($_SERVER['HTTP_HOST'] == "localhost") {
                  echo "<!-- Recaptcha Local -->";
                  echo '<div class="g-recaptcha" data-sitekey="6Lcxq3MaAAAAAHlQp2OnQex0pSCmKbV4UUK-T-Om"></div>';
                }elseif($_SERVER['HTTP_HOST'] == "businesfactory.co"){
                  echo "<!-- Recaptcha Web -->";
                  echo '<div class="g-recaptcha" data-sitekey="6LfEoXMaAAAAAEs0quYCO-6v6w0nCNWtaps93U_L"></div>';

                }                
                ?>
                <div id="answerlogin"></div>
                
              </div>
              <div>
                <button class="btn btn-default" type="submit">Restablecer Contraseña</button>
                
              </div>

              <div class="clearfix"></div>

              <div class="separator">
              <a class="to_reset" href="<?php echo SERVERURL; ?>home/index">Ingresar al sisetema</a>
                <div class="clearfix"></div>
                <br />

                <div>
                  <img src="<?php echo SERVERURL; ?>views/assets/images/LogoOriginal.png" alt="" width="260" height="80">
                  <p>©2021 Todos los derechos reservados. Business Factory! es una plantilla de Bootstrap 3. Privacidad y condiciones</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
