<?php require_once "../../../config.php"?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Website description-->
    <meta name="description" content = "Business Factory"/>
    <meta name="author" content ="Edison Monsalve Y Estaban Osorio">
    <meta name="keywords" content ="Aplicación Web Facturación Electrónica">

    <title>Business Factory | Error404</title>

    <!-- App Icon -->
    <link rel="icon" href="<?php echo SERVERURL; ?>views/assets/images/LogoOriginalIncompleto.png">
    <!-- Bootstrap -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo SERVERURL; ?>views/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo SERVERURL; ?>views/assets/build/css/custom.min.css" rel="stylesheet">
  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
              <h1 class="error-number">404</h1>
              <h2>Lo sentimos pero no pudimos encontrar esta página</h2>
              <p>Esta página que está buscando no existe. <a href="#">¿ Informar esto?</a>
              </p>
              <div class="mid_center">
                <h3>Buscar</h3>
                <form>
                  <div class="col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Buscar...">
                      <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                          </span>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
    <!-- jQuery -->
    <script src="<?php echo SERVERURL; ?>views/vendors/assets/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo SERVERURL; ?>views/vendors/assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- NProgress -->
    <script src="<?php echo SERVERURL; ?>views/vendors/assets/nprogress/nprogress.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo SERVERURL; ?>views/assets/build/js/custom.min.js"></script>
    </body>
    </html>
