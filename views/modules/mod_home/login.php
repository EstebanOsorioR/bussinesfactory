<body class="login">
    <div>
      <a class="hiddenanchor" id="reset"></a>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="post" name="frmlogin[]">
              <h1>Inicio de sesión</h1>
              <div>
                <input type="email" class="form-control minus" placeholder="Correo Electrónico" name="frmlogin[]" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Contraseña" name="frmlogin[]" required="" />
              </div>
              <div class= "g-recaptcha-center">
                <?php if ($_SERVER['HTTP_HOST'] == "localhost") {
                  echo "<!-- Recaptcha Local -->";
                  echo '<div class="g-recaptcha" data-sitekey="6Lcxq3MaAAAAAHlQp2OnQex0pSCmKbV4UUK-T-Om"></div>';
                }elseif($_SERVER['HTTP_HOST'] == "businesfactory.co"){
                  echo "<!-- Recaptcha Web -->";
                  echo '<div class="g-recaptcha" data-sitekey="6LfEoXMaAAAAAEs0quYCO-6v6w0nCNWtaps93U_L"></div>';

                }                
                ?>
                <div id="answerlogin"></div>
                
              </div>
              <div>
                <button class="btn btn-default" type="submit">Ingresar</button>
                <a class="to_reset" href="#reset">¿Perdiste tu contraseña?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">¿Nuevo en el sitio? 
                  <a href="#signup" class="to_register"> Crear una cuenta </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <img src="<?php echo SERVERURL; ?>views/assets/images/LogoOriginal.png" alt="" width="260" height="80">
                  <p>©2021 Todos los derechos reservados. Business Factory! es una plantilla de Bootstrap 3. Privacidad y condiciones</p>
                </div>
              </div>
            </form>
          </section>
        </div>
        <?php
       
        ?>
        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form method="post" name="frmregister[]">
              <h1>Crear una cuenta</h1>
              <div>
                <input type="text" class="form-control mayus" placeholder="Razón social o Nombre" name="frmregister[]" required="" />
              </div>
              <div>
                <input type="email" class="form-control minus" placeholder="Correo Electrónico" name="frmregister[]" required="" />
              </div>
              <div>
              <div class= "g-recaptcha-center">
              <?php if ($_SERVER['HTTP_HOST'] == "localhost") {
                  echo "<!-- Recaptcha Local -->";
                  echo '<div class="g-recaptcha" data-sitekey="6Lcxq3MaAAAAAHlQp2OnQex0pSCmKbV4UUK-T-Om"></div>';
                }elseif($_SERVER['HTTP_HOST'] == "businesfactory.co"){
                  echo "<!-- Recaptcha Web -->";
                  echo '<div class="g-recaptcha" data-sitekey="6LfEoXMaAAAAAEs0quYCO-6v6w0nCNWtaps93U_L"></div>';
                }                
                ?>
              </div>
                <button class="btn btn-default" type="submit">Crear cuenta</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Ya eres usuario ?
                  <a href="#signin" class="to_register"> Ingresar </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <img src="<?php echo SERVERURL; ?>views/assets/images/LogoOriginal.png" alt="" width="260" height="80">
                  <p>©2021 Todos los derechos reservados. Business Factory! es una plantilla de Bootstrap 3. Privacidad y condiciones</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="reset" class="animate form reset_pass_form">
          <section class="login_content">
            <form method="post" name="frmrestoreemail[]">
              <h1>Restablecer Contraseña</h1>
              <div>
                <input type="email" class="form-control minus" placeholder="Correo Electrónico" name="frmrestoreemail[]" required="" />
              </div>
              <div>
              <div class= "g-recaptcha-center">
                <div class="g-recaptcha" data-sitekey="6Lcxq3MaAAAAAHlQp2OnQex0pSCmKbV4UUK-T-Om"></div>
              </div>
                <button class="btn btn-default" type="submit">Restablecer</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Ya eres usuario ?
                  <a href="#signin" class="to_resert"> Ingresar </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <img src="<?php echo SERVERURL; ?>views/assets/images/LogoOriginal.png" alt="" width="260" height="80">
                  <p>©2021 Todos los derechos reservados. Business Factory! es una plantilla de Bootstrap 3. Privacidad y condiciones</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
